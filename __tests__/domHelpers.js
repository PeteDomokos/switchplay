import React from 'react'
import ReactDOM from 'react-dom'
import { act } from 'react-dom/test-utils';

export const createContainer = () => {
  const container = document.createElement('div')
  const form = id => 
    container.querySelector(`form[id="${id}"] `)
  const field = (formId, fieldName) => 
    form(formId).elements[fieldName]
    
  return {
    render: component =>
        act(() => {
        	ReactDOM.render(component, container)
        }),
    renderAndWait: async component =>
      	await act(async () =>
      	 	ReactDOM.render(component, container)),
    container,
    form,
    field
  }
}