import React, {Component} from 'react'
import PropTypes from 'prop-types'
import { Link } from 'react-router-dom'
//material-ui
import {ListItem,ListItemText} from '@material-ui/core'
import Divider from '@material-ui/core/Divider'
//styles
import {withStyles} from '@material-ui/core/styles'
//helpers
import { renderActionButton } from '../Buttons'

export const ProfileActions = ({actions}) =>{
  return(
  <div style={{display:'flex', justifyContent:'center'}}>
    {actions.map((action,i) =>
      <div key={''+(action.id ? action.id : '')+i}>
        {renderActionButton(action,i)}
      </div>)}
  </div>
  )
}
ProfileActions.defaultProps = {
  buttonActions:[]
}

export const AdditionalItems = ({item}) =>
  <React.Fragment>
    <Divider/>
    <ListItem>
      {item.created &&
        <ListItemText 
          primary={"Joined: " + (new Date(item.created)).toDateString()}/>}
    </ListItem>
  </React.Fragment>

