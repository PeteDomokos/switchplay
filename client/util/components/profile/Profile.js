import React, {Component} from 'react'
import PropTypes from 'prop-types'
import {Redirect, Link} from 'react-router-dom'
//material-ui
import Paper from '@material-ui/core/Paper'
import {List, ListItem, ListItemAvatar, ListItemSecondaryAction, 
    ListItemText} from '@material-ui/core'
import Avatar from '@material-ui/core/Avatar'
import IconButton from '@material-ui/core/IconButton'
import Button from '@material-ui/core/Button'
import Typography from '@material-ui/core/Typography'
import Divider from '@material-ui/core/Divider'
//icons
import Edit from '@material-ui/icons/Edit'
import Person from '@material-ui/icons/Person'
//styles
import {withStyles} from '@material-ui/core/styles'
//child components
import DeleteGroup from '../../../groups/DeleteGroup'
import DeleteUser from '../../../user/DeleteUser'
//helpers


const styles = theme => ({
  //conditional for 'card' and !card
  root: theme.mixins.gutters({
    //maxWidth: 600,
    width:80,
    //margin: 'auto',
    //padding: theme.spacing(3),
    //marginTop: theme.spacing(0)
  }),
  title: {
    //margin: `${theme.spacing(3)}px 0 ${theme.spacing(2)}px`,
    //color: theme.palette.protectedTitle
  }
})

class Profile extends Component {
  constructor({match}) {
    super()
    this.state = {
      redirectToSignin: false
    }
  }
  //init - check user is authenticated - if so, set state to that user


  render() {
    const { classes, entityType, entity, authToUpdate, shortVersion } = this.props
    const { name, firstName, surname, desc, _id, created } = entity

    const photoUrl = 
    	entityType &&_id ? `/api/${entityType}/photo/${_id}?${new Date().getTime()}` : ''

    const fullName = name ? name : firstName + ' '+surname

    if(this.state.redirectToSignin)
      return <Redirect to='/signin'/>

    return (
        <Paper className={classes.root +' '+entityType} elevation={4}>
          <List dense>
            <ListItem>
              <ListItemAvatar>
                <Avatar src={photoUrl} className={classes.bigAvatar}/>
              </ListItemAvatar>
              <ListItemText 
                primary={fullName} secondary={desc}/> 
              {authToUpdate  && 
                <UpdateActions id={_id}/>}
            </ListItem>
            {!shortVersion && 
              <AdditionalItems created={created}/>}
          </List>
        </Paper>
    )
  }
}
Profile.propTypes = {
  classes: PropTypes.object.isRequired,
  entity: PropTypes.object.isRequired
}
Profile.defaultProps = {
}

const UpdateActions = ({id, entityType}) =>
  <ListItemSecondaryAction>
    <Link to={"/"+entityType+"/edit/"+id}>
      <IconButton aria-label="Edit" color="primary">
        <Edit/>
      </IconButton>
    </Link>
    {entityType === 'user' &&
    	<DeleteUser userId={id}/>}
    {entityType === 'group' &&
    	<DeleteGroup groupId={id}/>}
  </ListItemSecondaryAction>


const AdditionalItems = ({created}) =>
  <React.Fragment>
    <Divider/>
    <ListItem>
      <ListItemText 
        primary={"Created: " +(new Date(created)).toDateString()}/>
    </ListItem>
  </React.Fragment>

export default withStyles(styles)(Profile)