import React, { useEffect }  from 'react'
import GroupsSelector from './GroupsSelector'
import PlayersSelector from './PlayersSelector'
import DatasetsSelector from './DatasetsSelector'
/**
*
**/

//todo - add a buffer mechanism for fetching groups and players, and a way of determining 
//if more need to be fetched, as store.otherGroups and otherPlayers will not be ''
export const GroupsSelectorLoader = 
	({usersGroups, otherGroups, requiredNrGroups, preselections, history, onSave, onLoad, loading, active, onActivate}) => {
	useEffect(() => {
		//note userGroups already available if user logged in.
		//if user not logged in, then there are no userGroups 
		if(!otherGroups && !loading){
			console.log("GroupSelectorLoader loading groups...")
			onLoad()
		}
		//todo - deal with preselections - may be array of groupIds or maybe array of groups, 
		//attempt should be made to load them
		//and may be coming through as undefined - alert user, and then render as blank
	}, [])
	return (
		<GroupsSelector usersGroups={usersGroups} otherGroups={otherGroups} 
			history={history} requiredNrGroups={requiredNrGroups} preselections={preselections}
			onSave={onSave} loading={loading} active={active} onActivate={onActivate}/>)
}


export const PlayersSelectorLoader = 
	({usersPlayers, otherPlayers, requiredNrPlayers, preselections, history, onSave, onLoad, loading, active, onActivate}) => {
	useEffect(() => {
		//note userPlayers already available if user logged in.
		//if user not logged in, then there are no userPlayers 
		if(!otherPlayers && !loading){
			console.log("PlayerSelectorLoader loading players...")
			onLoad()
		}
		//deal with preselected (see groups above)...
	}, [])
	return (
		<PlayersSelector usersPlayers={usersPlayers} otherPlayers={otherPlayers} 
			preselections={preselections} history={history} onSave={onSave} 
			requiredNrPlayers={requiredNrPlayers} loading={loading} active={active} onActivate={onActivate} />)
}

export const DatasetsSelectorLoader = 
	({dashboard, groups, parents, path, onSave, onLoad, loading}) => {
		console.log("DatasetsSelectorLoader groups", groups)
	useEffect(() => {
		//if players and if any groups not loaded, load them from server
		const unloadedGroups = groups.filter(g => !g.datasets)
		const unloadedParents = parents.filter(g => !g.datasets)
		const unloaded = [...unloadedGroups, ...unloadedParents]
		if(unloaded.length != 0 && !loading){
			console.log("PlayerSelectorLoader loading players...")
			onLoad(unloaded.map(g => g._id))
		}
	}, [])
	return (
		<DatasetsSelector 
			dashboard={dashboard} groups={groups} parents={parents} path={path} 
			onSave={onSave} loading={loading} />)
}

