import React, {Component} from 'react'
import PropTypes from 'prop-types'
import { Link } from 'react-router-dom'
//material-ui
import Paper from '@material-ui/core/Paper'
import { List, ListItem, ListItemAvatar, ListItemSecondaryAction, 
  ListItemText } from '@material-ui/core'
import Avatar from '@material-ui/core/Avatar'
import IconButton from '@material-ui/core/IconButton'
import Button from '@material-ui/core/Button'
import Typography from '@material-ui/core/Typography'
import Divider from '@material-ui/core/Divider'
//icons
import Edit from '@material-ui/icons/Edit'
import Person from '@material-ui/icons/Person'
import ArrowForwardIcon from '@material-ui/icons/ArrowForward'
import AddCircleIcon from '@material-ui/icons/AddCircle'
import DeleteIcon from '@material-ui/icons/Delete'
import AddBoxIcon from '@material-ui/icons/AddBox';
import GroupAddIcon from '@material-ui/icons/GroupAdd'
import AssessmentIcon from '@material-ui/icons/Assessment'

export const renderIcon = (icon, options) =>{
  const opts = options ? options : {}
  switch(icon){
    case 'data':
      return(<AssessmentIcon/>)
    case 'arrow-for':
      return(<ArrowForwardIcon/>)
    case 'add':
      return(<AddCircleIcon/>)
    case 'add-group':
      return(<GroupAddIcon/>)
     case 'add-box':
      return(<AddBoxIcon fontSize={opts.fontSize}/>)
    case 'delete':
      return(<DeleteIcon/>)
    case 'edit':
      return(<Edit/>)
  }
}

export const renderActionButton = (buttonAction, i) =>{
  const { icon, label, link, onClick, style, options} = buttonAction
  const extraStyle = style ? style : {}
  if(link)
    return(
      <Link to={link} style={{textDecoration:'none'}} key={'link-button'+link+(i ? i :'')}>
        {icon ?
          <IconButton color="primary" style={extraStyle}>{renderIcon(icon, options)}</IconButton>
          :
          <Button color="primary" variant="contained" style={extraStyle}>
            {label}</Button>}
      </Link>
    )
  else
    return(
      <React.Fragment key={'nonlink-button'+(i ? i :'')}>
      {icon ?
        <IconButton color="primary" style={extraStyle}
          onClick={() => buttonAction.onClick(buttonAction)}>
          {renderIcon(icon, options)}
        </IconButton>
        :
        <Button color="primary" variant="contained" style={extraStyle}
           onClick={() => onClick(buttonAction)}>
          {label}</Button>}
      </React.Fragment>
    )
}