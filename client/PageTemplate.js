import React from "react";
import { render } from "react-dom"
import { withRouter} from 'react-router-dom'
import ReactPlayer from 'react-player'
import { Link } from 'react-router-dom'
import { slide as ElasticMenu } from 'react-burger-menu'

import { MainMenuManager } from './core/Menus'
import { MainMenu, SubMenu } from './core/Menus'
import logo from './assets/images/logo.png'
import auth from './auth/auth-helper'

//todo - pass in section as a param instead of using this method
/**
* Desc ...
* @param {string} 
* @return {any} props value
**/
let sectionName = (history) =>{
	if(history.location.pathname == '/')
		return 'home'
	if(history.location.pathname.startsWith('/about'))
		return 'about'
	if(history.location.pathname.startsWith('/education'))
		return 'education'
	if(history.location.pathname.startsWith('/dashboard'))
		return 'dashboard'
	if(history.location.pathname.startsWith('/contact'))
		return 'contact'
	if(history.location.pathname.startsWith('/signin'))
		return 'signin'
	if(history.location.pathname.startsWith('/signup'))
		return 'signup'
	return ''
}
/**
* Desc ...
* @param {string} 
* @return {any} props value
**/
let showHeader = (sectionName) => {
	if(["dashboard"].includes(sectionName))
		return false
	return true
}
/**
* Desc ...
* @param {string} 
* @return {any} props value
**/
let showLogo = (sectionName) => {
	if(["dashboard"].includes(sectionName))
		return false
	return true
}
/**
* Desc ...
* @param {string} 
* @return {any} props value
**/
let subMenuToShow = (sectionName) =>{
	if(["dashboard"].includes(sectionName))
		return 'dashboard'
	return undefined
}
/**
* Desc ...
* @param {string} 
* @param {Object}
* @param {Object[]}  
* @param {number} 
* @return {any} props value
**/
export const PageTemplate = withRouter(({children, history, match}) =>{
	return(
		/*todo - move menu into here instead of menu-manager, 
		so if mobile then menu is not part of header*/
	<section className={"page"}>
		<section className={'header '+(showHeader(sectionName(history)) ? 'full-height':'zero-height')}>
			<Link to="/">
				<div className={'not-ls logo-cont image-cont '
					+(showLogo(sectionName(history)) ? 'full-height':'zero-height')}>
            		<img src={logo} />
          		</div>
          		<div className='not-ss not-ms logo-cont image-cont full-height'>
            		<img src={logo} />
          		</div>
        	</Link>
	    	<div className='not-ss not-ms menus-cont'>
	      		<MainMenu/>
	      		{subMenuToShow(sectionName(history)) && 
	      			<SubMenu menuName={subMenuToShow(sectionName(history))}/>}
	    	</div>
		</section>
		<div className={'not-ls '+(history.location.pathname.includes('skills-and-fitness') ? 'dark':'light')}>
	        <div className='bm-burger-button'></div>
	        <ElasticMenu right width={150}>
	          <MainMenu elastic/>
	        </ElasticMenu>
	    </div>
		<div className={'page-background-cont '
			+(showHeader(sectionName(history)) ? 'with-header':'without-header')} 

			>
        </div>
        <section className='content'>
			{children}
		</section>
		<section className='footer'>footer</section>
	</section>
	)}
)


