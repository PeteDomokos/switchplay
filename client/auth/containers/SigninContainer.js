import{ Component } from "react";
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { withRouter, Redirect } from 'react-router'

import { fetchUser } from '../../actions/User'
import Signin from '../Signin'

const mapStateToProps = state => ({
})
const mapDispatchToProps = dispatch => ({
	fetchUser(id){
		dispatch(fetchUser(id))
	}
})

const SigninContainer = connect(
	mapStateToProps,
	mapDispatchToProps
	)(Signin)

export default withRouter(SigninContainer)