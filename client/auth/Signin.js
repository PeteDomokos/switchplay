import React, {Component} from 'react'
import {Redirect} from 'react-router-dom'
import PropTypes from 'prop-types'
//import {CardActions, CardContent} from 'material-ui/Card'
//import Card from '@material-ui/core/Card'
//material-ui
import {Card, CardActions, CardContent} from '@material-ui/core'
import Button from '@material-ui/core/Button'
import TextField from '@material-ui/core/TextField'
import Typography from '@material-ui/core/Typography'
import Icon from '@material-ui/core/Icon'
import { withStyles } from '@material-ui/core/styles'
//helpers
import auth from './../auth/auth-helper'
import {signin} from './api-auth.js'

const styles = theme => ({
  card: {
    maxWidth: '600px',
    width:'80vw',
    margin: 'auto',
    textAlign: 'center',
    marginTop: theme.spacing(5),
    marginBottom: theme.spacing(5),
    paddingBottom: theme.spacing(2)
  },
  error: {
    verticalAlign: 'middle'
  },
  title: {
    marginTop: theme.spacing(2),
    color: theme.palette.openTitle
  },
  textField: {
    marginLeft: theme.spacing(1),
    marginRight: theme.spacing(1),
    width: 300
  },
  submit: {
    marginBottom: theme.spacing(2)
  }
})

class Signin extends Component {
  state = {
      email: '',
      password: '',
      error: '',
      redirectToReferrer: false
  }
  processSignin = (user) =>{
    console.log("user", user)
    signin(user).then((data) => {
      if (data.error) {
        console.log("signing error. data: ", data)
        this.setState({error: data.error})
      } else {
        console.log("signed in at server. user: ", data)
        auth.authenticate(data, () => {
          //fetch fuller user object from server
          this.props.fetchUser(data.user._id)
          this.setState({redirectToReferrer: true})
        })
      }
    })
  }
  clickGuestSubmit = () =>{
    let user = {email: 'deo@y.z', password:'dddddd'}
    this.processSignin(user)
  }
  clickSubmit = () =>{
    let user = {
        email: this.state.email || undefined,
        password: this.state.password || undefined
    }
    this.processSignin(user)
  }
  handleChange = name => event => {
    this.setState({[name]: event.target.value})
  }

  render() {
    const {classes} = this.props
    const {from} = this.props.location.state || {
      from: {
        pathname: '/' //default to home for now as location undefined
      }
    }
    //TODO - find out why location undefined for redirect
    //console.log("location", this.props.location.state)
    const {redirectToReferrer} = this.state
    if (redirectToReferrer) {
      return (<Redirect to={from}/>)
    }

    return (
      <Card className={classes.card}>
        <CardContent>
          <Typography type="headline" component="h2" className={classes.title}>
            Sign In
          </Typography>
          <TextField id="email" type="email" label="Email" className={classes.textField} value={this.state.email} onChange={this.handleChange('email')} margin="normal"/><br/>
          <TextField id="password" type="password" label="Password" className={classes.textField} value={this.state.password} onChange={this.handleChange('password')} margin="normal"/>
          <br/> {
            this.state.error && (<Typography component="p" color="error">
              <Icon color="error" className={classes.error}>error</Icon>
              {this.state.error}
            </Typography>)
          }
        </CardContent>
        <CardActions style={{display:'flex', flexDirection:'column'}}>
          <Button style={{margin:10}} color="primary" variant="contained" 
            onClick={this.clickSubmit} className={classes.submit}>Submit</Button>
          <Button color="primary" variant="contained" onClick={this.clickGuestSubmit} 
            className={classes.submit}>Demo Sign-in</Button>
        </CardActions>
      </Card>
    )
  }
}

Signin.propTypes = {
  classes: PropTypes.object.isRequired
}

export default withStyles(styles)(Signin)
