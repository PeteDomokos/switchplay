import React, {Component} from 'react'
import {Redirect, Link} from 'react-router-dom'
import PropTypes from 'prop-types'
//material-ui
import { Card, CardActions, CardContent } from '@material-ui/core'
import Button from '@material-ui/core/Button'
import TextField from '@material-ui/core/TextField'
import Select from '@material-ui/core/Select'
import MenuItem from '@material-ui/core/MenuItem'
import Typography from '@material-ui/core/Typography' 
import Icon from '@material-ui/core/Icon'
import { Dialog, DialogActions, DialogContent, DialogContentText, 
  DialogTitle} from '@material-ui/core'
import FormControl from '@material-ui/core/FormControl'
import FormControlLabel from '@material-ui/core/FormControlLabel'
import FormLabel from '@material-ui/core/FormLabel'
import Radio from '@material-ui/core/Radio'
import RadioGroup from '@material-ui/core/RadioGroup'
//icons
import Publish from '@material-ui/icons/Publish'
//styles
import {withStyles} from '@material-ui/core/styles'

//helpers
import {createGroup} from './api-group.js'
import auth from './../auth/auth-helper'
//constants
import { groupTypes } from '../constants/UserAndGroupConstants'
import {  } from './GroupHelpers'

const styles = theme => ({
  card: {
    maxWidth: '70vw',
    margin: 'auto',
    textAlign: 'center',
    marginTop: theme.spacing(5),
    marginBottom: theme.spacing(5),
    paddingBottom: theme.spacing(2)
  },
  error: {
    verticalAlign: 'middle'
  },
  title: {
    marginTop: theme.spacing(2),
    color: theme.palette.openTitle
  },
  textField: {
    marginLeft: theme.spacing(0),
    marginRight: theme.spacing(0),
    width: '60vw'
  },
  submit: {
    margin: 'auto',
    marginBottom: theme.spacing(2)
  },
  selectBtns: {
    display:'flex',
    flexDirection: 'column'
  },
  selectBtn: {
    margin: 'auto',
    marginTop: theme.spacing(2),
  },
  additionalFields:{
  }
})

class CreateGroup extends Component {
  state = {
      name: '',
      desc:'',
      groupType:'',
      photo:'',
      groupId:'',
      parent:''
  }
  componentDidMount(){
    window.scrollTo(0, 0)
    this.groupData = new FormData()
    this.setState({
      //preselected parent - may be undefined
      parent:this.props.parent ? this.props.parent : ''
    })
  }
  handleChange = key => event => {
    const value = key === 'photo'
      ? event.target.files[0] : event.target.value

    this.groupData.set(key, value)
    this.setState({[key]: value})
  }
  clickSubmit = () => {
    console.log("submit")
    //admin - for now its just the user no-one else
    this.groupData.set('admin', [this.props.user._id])
    console.log("submitting")
    this.props.submit(this.groupData)
  }
  render() {
    const {classes, user, parent, creating, open, error} = this.props
    const availableParents = user.adminGroups

    return (<div>
      <Card className={classes.card}>
        <CardContent>

          <Typography type="headline" component="h2" className={classes.title}>
            Create Group
          </Typography>
          <SelectParentGroup parent={this.state.parent} availableParents={availableParents} 
            handleChange={this.handleChange}/>
          <SelectGroupType 
            groupType={this.state.groupType} groupTypes={groupTypes}
            handleChange={this.handleChange}/>
          <TextField id="name" label="Group Name" className={classes.textField} value={this.state.name} 
                    onChange={this.handleChange('name')} margin="normal"/><br/>
          <TextField id="desc" multiline rows="2" type="desc" label="Description" className={classes.textField} 
            value={this.state.desc} onChange={this.handleChange('desc')} margin="normal"/><br/>
          <input accept="image/*" onChange={this.handleChange('photo')} className={classes.input} 
            id="icon-button-file" type="file" style={{display:'none'}} />

          <label htmlFor="icon-button-file">
            <Button variant="contained" color="default" component="span">Upload Photo
              <Publish/>
            </Button>
          </label> 
          <span className={classes.filename}>{this.state.photo ? this.state.photo.name : ''}</span>

        </CardContent>
        <CardActions>
          <Button color="primary" variant="contained" onClick={this.clickSubmit} 
            className={classes.submit}>Create Group</Button>
        </CardActions>
      </Card>
    </div>)
  }
}

CreateGroup.propTypes = {
  classes: PropTypes.object.isRequired
}

const SelectGroupType  = ({selectedGroupType, groupTypes, handleChange}) => 
  <FormControl component="fieldset" >
    <FormLabel component="legend">Choose a word to describe this group...</FormLabel>
    <RadioGroup row onChange={handleChange('groupType')} style={{display:'flex'}}
      defaultValue="Club" aria-label="visibility" name="customized-radios">
      {groupTypes.map(type =>
         <FormControlLabel
          key={type} value={type} control={<Radio />} label={type} />)}
    </RadioGroup>
  </FormControl>

const SelectParentGroup = ({parent, availableParents, handleChange}) =>
  <div style={{margin:30}}>
    <h4 style={{margin:5}}>Parent Group (optional)</h4>
    <Select 
      value={parent}
      labelId="select-parent" id="select-parent" 
      onChange={handleChange('parent')}
      style={{minWidth:250}}>
      <MenuItem key={'no-parent'} value=''>No parent</MenuItem>
      {availableParents.map(group =>
        <MenuItem key={'select-parent-'+group._id} value={group._id}>{group.name}</MenuItem>
      )}
    </Select>
  </div>

export default withStyles(styles)(CreateGroup)
