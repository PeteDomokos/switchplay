import React, {Component} from 'react'
import PropTypes from 'prop-types'
import {Redirect, Link} from 'react-router-dom'
//material-ui
import Paper from '@material-ui/core/Paper'
import { List, ListItem, ListItemAvatar, ListItemSecondaryAction, 
  ListItemText} from '@material-ui/core'
import Avatar from '@material-ui/core/Avatar'
import IconButton from '@material-ui/core/IconButton'
import Button from '@material-ui/core/Button'
import Typography from '@material-ui/core/Typography'
import Divider from '@material-ui/core/Divider'
//icons
import Edit from '@material-ui/icons/Edit'
import Person from '@material-ui/icons/Person'
//styles
import {withStyles} from '@material-ui/core/styles'

//child components
import DeleteGroupContainer from './containers/DeleteGroupContainer'
import UserProfile from './../user/UserProfile'
//helpers
import auth from './../auth/auth-helper'
import { readGroup, addPlayer, removePlayer } from './api-group.js'

const styles = theme => ({
  root: theme.mixins.gutters({
    maxWidth: 600,
    margin: 'auto',
    padding: theme.spacing(3),
    marginTop: theme.spacing(0)
  }),
  title: {
    margin: `${theme.spacing(3)}px 0 ${theme.spacing(2)}px`,
    color: theme.palette.protectedTitle
  }
})

class GroupProfile extends Component {
  constructor({match}) {
    super()
    this.state = {
      group: {name:'', desc:'', _id:'', admin:[], players:[]},
      redirectToSignin: false
    }
  }
  //init - check user is authenticated - if so, set state to that user
  init = (groupId) => {
    //check any user is authenticated (not from params)
    const jwt = auth.isAuthenticated()
    //if not, then jwt will be a boolean false, and hence the following 
    //read method will return as an error. also error if jwt doesnt match the server one
    readGroup({
      id: groupId
    }, {t: jwt.token}).then((data) => {
      if(data.error){
        this.setState({redirectToSignin: true})
      }else{
        console.log("Group returned:", data)
        this.setState({group: data})
      }
    })
  }
  componentDidMount = () => {
    //todo - redux: pass this logic into group loader component and check store instead of props
    if(this.props.group)
      this.setState({group:this.props.group})
    else if(this.props.match)
      this.init(this.props.match.params.groupId)
  }

  render() {
    const { classes, group, authToUpdate, shortVersion } = this.props
    const photoUrl = group._id ? `/api/group/photo/${group._id}?${new Date().getTime()}` : ''

    if(this.state.redirectToSignin)
      return <Redirect to='/signin'/>

    return (
        <Paper className={classes.root +' group'} elevation={4}>
          <List dense>
            <ListItem>
              <ListItemAvatar>
                <Avatar src={photoUrl} className={classes.bigAvatar}/>
              </ListItemAvatar>
              <ListItemText primary={group.name} secondary={group.desc}/> 
              {authToUpdate  && 
                <UpdateActions groupId={group._id}/>}
            </ListItem>
            {!shortVersion && 
              <AdditionalItems group={group}/>}
          </List>
        </Paper>
    )
  }
}
GroupProfile.propTypes = {
  classes: PropTypes.object.isRequired,
  group: PropTypes.object.isRequired
}
GroupProfile.defaultProps = {
}

const UpdateActions = ({groupId}) =>
  <ListItemSecondaryAction>
    <Link to={"/group/edit/"+groupId}>
      <IconButton aria-label="Edit" color="primary">
        <Edit/>
      </IconButton>
    </Link>
    <DeleteGroupContainer id={groupId}/>
  </ListItemSecondaryAction>


const AdditionalItems = ({group}) =>
  <React.Fragment>
    <Divider/>
    <ListItem>
      <ListItemText 
        primary={"Created: " +(new Date(group.created)).toDateString()}/>
    </ListItem>
  </React.Fragment>

export default withStyles(styles)(GroupProfile)
