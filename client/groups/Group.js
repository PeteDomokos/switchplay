import React, {Component} from "react"
import PropTypes from "prop-types"
import { Route, Switch, Link }from 'react-router-dom'
//material-ui
import Button from '@material-ui/core/Button'
//children
import CreateGroup from "./CreateGroup"
import GroupSummaryContainer from "./containers/GroupSummaryContainer"
import EditGroupProfile from "./EditGroupProfile"
import EditGroupPlayersContainer from "./containers/EditGroupPlayersContainer"
import EditGroupCoachesContainer from "./containers/EditGroupCoachesContainer"
import DatasetsContainer from "../core/datasets/containers/DatasetsContainer"
import AddDatapointContainer from "../core/datasets/containers/AddDatapointContainer"
//todo - /group should redirect to "/groups"
const Group= ({group, loading}) =>{
    return(
      <section className='group'>
        <GroupMenu groupId={group._id} />
        <Switch>
          <Route exact path="/group/:groupId" component={GroupSummaryContainer}/>
          <Route path="/group/:groupId/edit" component={EditGroupProfile}/>
          <Route path="/group/:groupId/coaches/edit" component={EditGroupCoachesContainer}/>
          <Route path="/group/:groupId/players/edit" component={EditGroupPlayersContainer}/>
          <Route path="/group/:groupId/datasets" component={DatasetsContainer}/>
          <Route path="/group/:groupId/datapoints/new" component={AddDatapointContainer} />
        </Switch>
      </section>
      )
}

//todo - style the active link
const GroupMenu = ({groupId}) =>
  <div style={{display:'flex', justifyContent:'flex-end'}}>
    <Link to={"/group/"+groupId} style={{textDecoration:'none'}}>
      <Button color="primary" variant="contained" style={{margin:10}}>
        Group Home</Button>
    </Link>
    <Link to={"/dashboard"} style={{textDecoration:'none'}}>
      <Button color="primary" variant="contained" style={{margin:10}}>
        View Data</Button>
    </Link>
    <Link to={"/group/"+groupId +"/datasets/new"} style={{textDecoration:'none'}}>
      <Button color="primary" variant="contained" style={{margin:10}}>
        Add Dataset</Button>
    </Link>
    <Link to={"/group/"+groupId +"/datapoints/new"} style={{textDecoration:'none'}}>
      <Button color="primary" variant="contained" style={{margin:10}}>
        Add Datapoint</Button>
    </Link>
  </div> 

export default Group