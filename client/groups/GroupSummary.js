import React, {Component} from 'react'
import PropTypes from 'prop-types'
import {Redirect, Link} from 'react-router-dom'
//material-ui
import Paper from '@material-ui/core/Paper'
//styles
import {withStyles} from '@material-ui/core/styles'
//child components
import GroupProfile from './GroupProfile'
import Players from './Players'
import ProfileCollection from '../util/components/profile/ProfileCollection'
//helpers
import { datasetNamePair } from '../core/datasets/DatasetHelpers'
import auth from './../auth/auth-helper'
import { readGroup, addPlayer, removePlayer, updateGroupFields } from './api-group.js'
import { listPlayers } from './../user/api-user.js'

const styles = theme => ({
  root: theme.mixins.gutters({
    maxWidth: 600,
    margin: 'auto',
    padding: theme.spacing(3),
    marginTop: theme.spacing(2),
    marginBottom: 300// theme.spacing(10)
  }),
  title: {
    margin: `${theme.spacing(3)}px 0 ${theme.spacing(2)}px`,
    color: theme.palette.protectedTitle
  }
})

class GroupSummary extends Component {
  constructor({match}) {
    super()
    this.state = {
      //group: {name:'', desc:'', _id:'', admin:[], players:[]},
      //availablePlayers:[],
      redirectToSignin: false,
      Error:false
    }
  }
  handleEditPlayersClick = () => {
    this.setState({editingPlayers:!this.state.editingPlayers})
    //todo - if(group.parent) list only players from that group
    //else list allPlayers as below
    if(this.state.group.parent){
       readGroup({id: group.parent._id}, {t: jwt.token}).then(parent => {
          //data.name ensures case of {error:""} is not missed -todo - find out why this happening
          if(parent.error || !parent.name){
            console.log("Parent error", parent)
            alert("error retrieving available players from server")
          }else{
            console.log("Parent returned", parent)
            this.setState({availablePlayers: parent.players})
          }
        })
    }else{
      //no parent so get all players
      listPlayers().then(data => {
        if(data.error || !Array.isArray(data) ) {
          console.log("player list error", data)
          this.setState({playersListError: true})
        }else{
          console.log("players returned", data)
          this.setState({availablePlayers: data})
        }
      })
    }
  }

  sendUpdateToServer = (updatedPlayers) =>{
    console.log("group sendUpdateToServer players", updatedPlayers)
    const jwt = auth.isAuthenticated()
    updateGroupFields(
      {id: this.state.group._id}, 
      {t: jwt.token}, {players:updatedPlayers}).then((data) => {
      if (data.error) {
        console.log("group update error!", data)
        this.setState({error: data.error})
      } else {
        console.log("group updated", data)
      }
    })
  }
  render() {
    const {classes, group, loading, history} = this.props
    if(this.state.redirectToSignin)
      return <Redirect to='/signin'/>
    const jwt = auth.isAuthenticated()
    const authToUpdate = jwt && group.admin && 
      (group.admin.includes(jwt.user._id) || jwt.user.isSystemAdmin)

    const datasetsWithNamePairs = group.datasets ?
      group.datasets.map(d => {return {...d, namePair:datasetNamePair(d, group.datasets)} })
      : []
    //format users if they are only ids
    const admin = group.admin[0] && group.admin[0]._id ? 
      group.admin : group.admin.map(user => {return{_id:user}})
    const players = group.players[0] && group.players[0]._id ? 
      group.players : group.players.map(p => {return{_id:p}})
    const coaches = group.coaches[0] && group.coaches[0]._id ? 
      group.coaches : group.coaches.map(c => {return{_id:c}})

    return (
      <Paper className={classes.root +' group'} elevation={4}>
        <GroupProfile group={group} authToUpdate={authToUpdate} />
        <GroupAdmin adminUsers={admin}/>
        <CoachesSummary coaches={coaches} groupId={group._id}/>
        <PlayersSummary players={players} groupId={group._id}/>
        <DatasetsSummary datasets={datasetsWithNamePairs} groupId={group._id} history={history}/>
      </Paper>
      )
  }
}

GroupSummary.propTypes = {
  classes: PropTypes.object.isRequired,
  group: PropTypes.object.isRequired
}

GroupSummary.defaultProps = {
}

const GroupAdmin = ({adminUsers}) =>{
  const titleStyle = {marginTop:'2vh', marginBottom:'2vh'}
  const listStyle = {margin:'5vh', marginTop:'2vh', marginBottom:'2vh'}
  return(
    <div className='admin-users users'>
      <h4 style={titleStyle}>Admin</h4>
      <div style={listStyle}>
        {adminUsers.map(u =>
          <div className='user-list-item' key={'admin'+u._id}>{u.username}</div>)
        }
      </div>
    </div>
)}
GroupAdmin.defaultProps = {
  adminUsers:[]
}

const CoachesSummary = ({coaches, groupId}) =>
  <div style={{margin:30}}>
    <ProfileCollection format='grid' title='Coaches' itemType='user' items={coaches} 
      emptyMesg='No coaches in group yet' />
     <Link to={"/group/"+groupId+'/coaches/edit'}>
      <button className='btn btn-primary'>
       {coaches.length == 0 ? 'Add Coaches' : 'Edit Coaches'}</button>
    </Link>
  </div>

CoachesSummary.defaultProps = {
  coaches:[]
}

const PlayersSummary = ({title, players, groupId}) =>
  <div style={{margin:30}}>
    <ProfileCollection format='grid' title='Players' itemType='user' items={players} 
      emptyMesg='No players in group yet' />
    <Link to={"/group/"+groupId+'/players/edit'}>
      <button className='btn btn-primary'>
       {players.length == 0 ? 'Add Players' : 'Edit Players'}</button>
    </Link>
  </div>

PlayersSummary.defaultProps = {
  players:[]
}

/*
make it onClick -
opt 1: bypass the selectors, save the dataset in store
under dashboard.datasets, and go to dashboard/group/groupId
(group/dataset both loaded already as they are loaded at start of GroupSummaryLoader
with loadGroup
But if we are using DatasetsSummary from outside of teh group, then may not be.
It may be best to have a loadGroup gate in GroupDashboardLoader. 

BEST OPTION:
Alternatively, these links go to teh Selectors, but before that they save both the group and 
teh dataset in dashboard in store, but they dont mark isSelected and datasetsAreSelected 
as true, so it will load teh selectors but with those in it

downside - we cant link to the dashboard directly, unless we also make 
isSelected and datasetsAreSelected = true. In that case, we should have  aloadGroup gate 
in DashboardLoader - probably best to have it so we have the flexibility. Same for 
otehr dashbiards
*/
const DatasetsSummary = ({title, datasets, groupId, history}) =>{
  const actions = dataset => {
    return [
      {icon:'data', label:'view data',
        link:"/dashboard",
        id:dataset._id},
      {icon:'add', label:'add data', 
        link:"/group/"+groupId+'/datasets/'+dataset._id+"/datapoints/new",
        id:dataset._id},
      //link to DeleteDataset component
      {icon:'delete', label:'delete', link:"", id:dataset._id}
    ]
  }
  const onClickDataset = dataset => {
    history.push("/group/"+groupId+'/datasets/'+dataset._id)
  }
  return(
    <div style={{margin:30}}>
      <ProfileCollection format='list' title='Datasets' itemType='dataset' items={datasets} 
        onClickItem={onClickDataset} actions={actions} emptyMesg='No datasets added yet' />
      <Link to={"/group/"+groupId+'/datasets/new'}>
        <button className='btn btn-primary'>Add Dataset</button>
      </Link>
    </div>
    )
}

DatasetsSummary.defaultProps = {
  datasets:[]
}

export default withStyles(styles)(GroupSummary)