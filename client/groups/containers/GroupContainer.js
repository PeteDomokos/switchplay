import React, {Component} from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'

import { fetchGroup } from '../../actions/Groups'

import GroupLoader from '../GroupLoader'

const mapStateToProps = (state, ownProps) => {
	const { groupId } = ownProps.match.params
	return({
		group: state.user.groups.find(g => g._id === groupId),
		loading:state.asyncProcesses.loading.group
	})
}
const mapDispatchToProps = dispatch => ({
	onLoad(groupId){
		dispatch(fetchGroup(groupId))
	}
})

//wrap all 4 sections in the same container for now.
const GroupContainer = connect(
	mapStateToProps,
	mapDispatchToProps
	)(GroupLoader)

export default GroupContainer

