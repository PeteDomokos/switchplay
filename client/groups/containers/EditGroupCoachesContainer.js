import React, {Component} from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'

//todo - import { fetchEligibleCoaches } from '../../actions/PlayersAndCoaches'
import { fetchGroup, addPlayer, removePlayer } from '../../actions/Groups'

import EditGroupCoaches from '../EditGroupCoaches'


const mapStateToProps = state => {
	return({
		//uName: state.user.uName,
	})
}
const mapDispatchToProps = dispatch => ({
	/*onDemoReport(history){
		dispatch(attemptLoadReport(history))
	}*/
})

//wrap all 4 sections in the same container for now.
const EditGroupCoachesContainer = connect(
	mapStateToProps,
	mapDispatchToProps
	)(EditGroupCoaches)

export default EditGroupCoachesContainer

