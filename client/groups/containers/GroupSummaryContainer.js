import React, {Component} from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'

import { fetchGroup } from '../../actions/Groups'

import GroupSummary from '../GroupSummary'

const mapStateToProps = (state, ownProps) => {
	const { groupId } = ownProps.match.params
	return({
		group: state.user.groups.find(g => g._id === groupId),
		loading:state.asyncProcesses.loading.group,
		history:ownProps.history
	})
}
const mapDispatchToProps = dispatch => ({
})

//wrap all 4 sections in the same container for now.
const GroupSummaryContainer = connect(
	mapStateToProps,
	mapDispatchToProps
	)(GroupSummary)

export default GroupSummaryContainer

