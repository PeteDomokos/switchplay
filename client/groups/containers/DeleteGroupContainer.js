import React, {Component} from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'

import { openDeleteGroupDialog, deleteGroup, closeDeleteGroupDialog } from '../../actions/Groups'

import DeleteGroup from '../DeleteGroup'


const mapStateToProps = (state, ownProps) => {
	return{
		id:ownProps.id,
		deleting:state.asyncProcesses.deleting.group,
		open:state.dialogs.deleteGroup,
		error:''
	}
}
const mapDispatchToProps = dispatch => ({
	openDialog(){
		console.log("openDeleteGroupDialog called...")
		dispatch(openDeleteGroupDialog())
	},
	deleteGroup(groupId, history){
		dispatch(deleteGroup(groupId, history))
	},
	closeDialog(){
		dispatch(closeDeleteGroupDialog())
	}
})

//wrap all 4 sections in the same container for now.
const DeleteGroupContainer = connect(
	mapStateToProps,
	mapDispatchToProps
	)(DeleteGroup)

export default DeleteGroupContainer

