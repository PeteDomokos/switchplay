import React, {Component} from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'

import { fetchEligiblePlayers } from '../../actions/PlayersAndCoaches'
import { fetchGroup, addPlayer, removePlayer } from '../../actions/Groups'
import { playersFromGroupNotInOtherGroup } from '../GroupHelpers'

import EditGroupPlayersLoader from '../EditGroupPlayersLoader'

const mapStateToProps = (state, ownProps) => {
	let available
	const { groupId } = ownProps.match.params
	const group = state.user.groups.find(g => g._id === groupId)
	//available
	if(group.parent){
		//get parent players not selected yet
		const parentGroup = state.user.groups.find(g => g._id === parent)
		available = playersFromGroupNotInOtherGroup(parentGroup, group)
	}else{
		//get all players not selected yet
		available = playersFromGroupNotInOtherGroup({players:state.available.players}, group)
	}

	return({
		group:group,
		availablePlayers:available,
		loading:state.asyncProcesses.loading.players
	})
}
const mapDispatchToProps = dispatch => ({
	onLoadAvailable(groupId, parentId){
		if(parentId)
			dispatch(fetchGroup(parentId))
		else
			dispatch(fetchEligiblePlayers(groupId))
	},
	//note - a player is just a user we call player instead. The user contains playerInfo object. 
	handleAdd(player, groupId){
		console.log("adding player")
		dispatch(addPlayer(player, groupId))
	},
	handleRemove(player, groupId){
		console.log("removing player", player)
		dispatch(removePlayer(player, groupId))
	}
})

//wrap all 4 sections in the same container for now.
const EditGroupPlayersContainer = connect(
	mapStateToProps,
	mapDispatchToProps
	)(EditGroupPlayersLoader)

export default EditGroupPlayersContainer

