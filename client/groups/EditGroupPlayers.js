import React, {Component} from 'react'
import PropTypes from 'prop-types'
import {Redirect, Link} from 'react-router-dom'
//material-ui
import {withStyles} from '@material-ui/core/styles'
import Paper from '@material-ui/core/Paper'
//child components
import ProfileCollection from '../util/components/profile/ProfileCollection'
//helpers
import auth from './../auth/auth-helper'
import { readGroup, addPlayer, removePlayer, updateGroupFields } from './api-group.js'
import { listPlayers } from './../user/api-user.js'

const styles = theme => ({
  root: theme.mixins.gutters({
    maxWidth: 600,
    margin: 'auto',
    padding: theme.spacing(3),
    marginTop: theme.spacing(2),
    marginBottom: 300// theme.spacing(10)
  }),
  title: {
    margin: `${theme.spacing(3)}px 0 ${theme.spacing(2)}px`,
    color: theme.palette.protectedTitle
  }
})

class EditGroupPlayers extends Component {
  constructor({match}) {
    super()
    this.state = {
      redirectToSignin: false,
      error:false
    }
  }
  componentDidMount = () => {
    window.scrollTo(0, 0)
  }
  render() {
    const {classes, groupId, players, availablePlayers, 
        loading, handleAdd, handleRemove} = this.props

    const removeAction = (player) => {
      return [{icon:'delete', label:'Remove', onClick:handleRemove(player)}]
    }
    const addAction = (player) => {
      return [{icon:'add', label:'Add', onClick:handleAdd(player)}]
    }

    if(this.state.redirectToSignin)
      return <Redirect to='/signin'/>
    return (
      <Paper className={classes.root +' group'} elevation={4}>
        <ProfileCollection 
          format='grid' title='Players In Group' itemType='user' 
          items={players} actions={removeAction} singleRow
          emptyMesg='No players in group yet' />

        <ProfileCollection 
          format='grid' title='Available Players' itemType='user' 
          items={availablePlayers} actions={addAction} singleRow
          emptyMesg={loading ? 'Loading players...' : 'No players available'} />

        <Link to={"/group/"+groupId}>
          <button className='btn btn-primary add-players' onClick={this.handleEditPlayersClick}>
          Finished</button>
        </Link>
      </Paper>
    )
  }
}

EditGroupPlayers.propTypes = {
  classes: PropTypes.object.isRequired
}

EditGroupPlayers.defaultProps = {
}

export default withStyles(styles)(EditGroupPlayers)

