import React, { useEffect }  from 'react'
import EditGroupPlayers from './EditGroupPlayers'
/**
*
**/
const EditGroupPlayersLoader 
	=({onLoadAvailable, group, availablePlayers, loading, handleAdd, handleRemove}) => {
	useEffect(() => {
		//if no availablePlayers array, then need to either load parent to get parent players,
		//or load all available players from db
		if(!availablePlayers){
			console.log("EditGroupPlayersLoader loading available players...")
			onLoadAvailable(group._id, group.parent)
		}
	}, [])
	return (
		<EditGroupPlayers
			groupId={group._id}
			players={group.players} availablePlayers={availablePlayers} 
			loading={loading} 
			handleAdd={player => () => handleAdd(player, group._id)} 
			handleRemove={player => () => handleRemove(player, group._id)}/>
		)
}

export default EditGroupPlayersLoader