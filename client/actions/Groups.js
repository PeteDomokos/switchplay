import C from '../constants/ConstantsForStore'
import { filterUniqueByProperty } from '../util/helpers/ArrayManipulators'
import { status, parseResponse, logError, 
	fetchStart, fetchEnd, fetchThenDispatch} from './Common'

export const createGroup = group => dispatch => {
	console.log("actions.createGroup()")
	fetchThenDispatch(dispatch, 
		'creating.group',
		{
			url: '/api/groups/',
			method: 'POST',
			headers:{
	        	'Accept': 'application/json'
	      	},
			body:group, //not stringify as its a formidable object
			requireAuth:true,
			processor: data => {return {type:C.SAVE_NEW_GROUP, group:data, connection:'admin'}}
		})
}
export const openDeleteGroupDialog = () => {
	console.log("actions.openDeleteGroupDialog()")
	return {
		type:C.OPEN_DIALOG,
		path:'deleteGroup'
	}
}

export const deleteGroup = (groupId, history) => dispatch => {
	console.log("actions.deleteGroup() history")
	fetchThenDispatch(dispatch, 
		'deleting.group',
		{
			url: '/api/groups/'+groupId,
			method: 'DELETE',
			requireAuth:true,
			processor: data => {
				history.push("/")
				return {type:C.DELETE_GROUP, id:groupId}
			}
		})
}
//todo - impl deleteUser and genralise the openDialog and closeDialog methods into Common file
export const closeDeleteGroupDialog = () => {
	console.log("actions.closeDeleteGroupDialog()")
	return {
		type:C.CLOSE_DIALOG,
		path:'deleteGroup'
	}
}
/*
export const updateGroup = group => {
	fetchThenDispatch(dispatch, 
		'creating.group',
		{
			url: '/api/groups/',
			method: 'POST',
			headers:{
	        	'Accept': 'application/json'
	      	},
			body:group, //not stringify as its a formidable object
			requireAuth:true,
			processor: data => {return {type:C.UPDATE_GROUP, group:data, connection:'admin'}}
		})
}

*/

//to fetch a group in full
export const fetchGroup = id => dispatch => {
	fetchThenDispatch(dispatch, 
		'loading.group',
		{
			url: '/api/groups/' +id, 
			requireAuth:true,
			processor: data => {return {type:C.SAVE_GROUP, group:data}}
		})
}

export const addPlayer = (player, groupId) => dispatch => {
	//add to group in store first so UI not held up
	//warning - this will only save teh client side details of player, not th eplayer that is returned from server.
	//if later we need more details, we should re-save the player when returned from server too
	dispatch(
		{type:'ADD_PLAYER', groupId:groupId, player:player})
	//todo - error handler must warn and remove again if server error
	fetchThenDispatch(dispatch, 'updating.group.players', 
		{
			url: '/api/group/players/add',
			method:'PUT', 	
		 	requireAuth:true,
		 	body:JSON.stringify({playerId:player._id, groupId:groupId}),
		 	errorHandler:error =>{
				logError(error)
			 	alert("Server error: Player "+player._id+" was not added. Try again.")
			 	//remove player in store as it wasnt added at server
			 	dispatch({type:'REMOVE_PLAYER', groupId:groupId, player:player})
			}
		})
}

export const removePlayer = (player, groupId) => dispatch => {
	//remove from group in store first so UI not held up
	dispatch(
		{type:'REMOVE_PLAYER', groupId:groupId, player:player})
	//todo - error handler must warn and add again if server error
	fetchThenDispatch(dispatch, 'updating.group.players', 
		{
			url: '/api/group/players/remove',
		 	method:'PUT', 	
		 	requireAuth:true,
		 	body:JSON.stringify({playerId:player._id, groupId:groupId}),
			errorHandler:error =>{
				logError(error)
			 	alert("Server error: Player "+player._id+" was not removed. Try again.")
			 	//add player back in store as it wasnt removed at server
			 	dispatch({type:'ADD_PLAYER', groupId:groupId, player:player})
			}
		})
}

export const addDataset = (dataset, groupId) => dispatch => {
	//save to server first. if succ, then save to store
	//for now, whole group is returned and updated in store
	fetchThenDispatch(dispatch, 'updating.group.datasets', 
		{
			url: '/api/group/'+groupId+'/datasets/add',
		 	method:'PUT', 	
		 	requireAuth:true,
		 	processor: data => {
				return {
					type:C.SAVE_GROUP, group:data
				}
			},
		 	body:JSON.stringify(dataset),
			errorHandler:error =>{
				logError(error)
			}
		})
}
export const addDatapoint = (datapoint, groupId, datasetId) => dispatch => {
	//save to server first. if succ, then save to store
	fetchThenDispatch(dispatch, 'updating.dataset.datapoints', 
		{
			url: '/api/group/'+groupId+'/dataset/'+datasetId+'/datapoints/add',
		 	method:'PUT', 	
		 	requireAuth:true,
		 	processor: data => {
				return {
					type:C.SAVE_GROUP, group:data
				}
			},
		 	body:JSON.stringify(datapoint),
			errorHandler:error =>{
				logError(error)
			}
		})
}
export const resetStatus = path => (
	{
		type:C.RESET_STATUS,
		path:path
	}
)

//fetches all public group summaries

//todo - buffer these so only fetch a batch at at time. Also then will need a way of ensuring
//loader still requests more as store will have groups stored
export const fetchGroups = options => dispatch => {

	//todo - if(options && options.groupIds)
	//add query string onto url to get specific groups
	fetchThenDispatch(dispatch, 
		'loading.groups',
		{
			url: '/api/groups', 
			requireAuth:false,
			processor: data => {return {type:C.SAVE_GROUPS, groups:data}}
		})
}
