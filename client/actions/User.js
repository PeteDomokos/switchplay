import C from '../constants/ConstantsForStore'
import { filterUniqueByProperty } from '../util/helpers/ArrayManipulators'
import { status, parseResponse, logError, 
	fetchStart, fetchEnd, fetchThenDispatch} from './Common'

//todo - error handler must be passed through to remove user from session storge if not auth on server
export const fetchUser = id => dispatch => {
	console.log("fetchUser id", id)
	fetchThenDispatch(dispatch, 
		'loading.user',
		{
			url: '/api/user/'+id, 
			requireAuth:true,
			processor: data => {
				//gather all groups and players  into one array for client-side manipulation
				//Any temporary saving and retrieving of data on client side happens in here
				//with specific group and player arrays within user used as references only
				const groups = [
					...data.adminGroups, 
					...data.groupsFollowing,
					...data.groupsViewed, 
					...data.player.groups,
					...data.coach.groups]
				const filteredGroups = filterUniqueByProperty('_id', groups)
				const players = [
				//todo - add playersFollowing and playersViewed properties to user.model
					...data.adminGroups.map(g => g.players),   
					...data.player.groups.map(g => g.players), 
					...data.coach.groups.map(g => g.players)]
				const filteredPlayers = filterUniqueByProperty('_id', players)
				const dataWithGroupsAndPlayers = {...data, groups:filteredGroups, players:filteredPlayers}
				return {type:C.SAVE_USER, value:dataWithGroupsAndPlayers}
			}
		}) 
}

export const openDeleteUserDialog = () => {
	console.log("actions.openDeleteUserDialog()")
	return {
		type:C.OPEN_DIALOG,
		path:'deleteUser'
	}
}
export const deleteUser = (userId, history) => dispatch => {
	console.log("actions.deleteUser()")
	//deleting ok, but DELETE_USER action not impl in reducer
	//and also need to clear session storage 
	fetchThenDispatch(dispatch, 
		'deleting.user',
		{
			url: '/api/user/'+userId,
			method: 'DELETE',
			requireAuth:true,
			processor: data => {
				//clear session storage
				if (typeof window !== "undefined")
      				sessionStorage.removeItem('jwt')
				history.push("/")
				return {type:C.DELETE_USER, id:userId}
			}
		})
}
//todo - impl deleteUser and genralise the openDialog and closeDialog methods into Common file
export const closeDeleteUserDialog = () => {
	console.log("actions.closeDeleteUserDialog()")
	return {
		type:C.CLOSE_DIALOG,
		path:'deleteUser'
	}
}

/*
export const login = (user) => (
	{
		type: C.LOGIN,
		user
	}
)
export const demoLogin = () => (
	{
		type: C.DEMO_LOGIN
	}
)
export const saveUser = (user) => (
	{
		type: C.SAVE_USER,
		user
	}
)
export const removeUser = () => (
	{
		type: C.REMOVE_USER
	}
)
*/
