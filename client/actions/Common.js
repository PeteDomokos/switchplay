import C from '../constants/ConstantsForStore'
import auth from '../auth/auth-helper'

//HELPER METHODS
export const status = resp =>{
	if (resp.status != 200)
		return Promise.reject(new Error(resp.statusText))
	return Promise.resolve(resp)
}
export const parseResponse = resp => {
	return resp.json()
}
export const logError = err =>
	console.error("There is an action error!!!",err)
//higher-order actions
export const fetchStart = path => (
	{
		type: C.START,
		path:path
	}
)
export const fetchEnd = path => (
	{
		type: C.END,
		path:path
	}
)
//async
/**
*	fetches data from server and dispatched it to the store, 
*   Warning - server must return an action object.
*
*   Fetch Defaults: method GET, content-type json, no authentication
*/
export const fetchThenDispatch = (dispatch, asyncProcessesPath, options, responseProcessor) => {
	console.log("fetchThenDispatch")
	dispatch(fetchStart(asyncProcessesPath))

	const { method, url, headers, body, requireAuth, processor, errorHandler } = options
	const handleError = errorHandler ? errorHandler : logError
	const processResponse = processor ? processor : function(data){return data}

	const requiredHeaders = headers ? headers : {
		'Accept': 'application/json', 
     	'Content-Type': 'application/json'
	} 
	if(requireAuth){
		const jwt = auth.isAuthenticated()
		requiredHeaders['Authorization'] ='Bearer '+jwt.token
	}
	const settings = {
		method: method ? method : 'GET',
		headers: requiredHeaders,
		body:body //maybe undefined
	}
	
	//let settings = (method != 'GET' && body) ? {...generalSettings, body} : generalSettings
	//console.log("calling fetch url:", url)
	//console.log("and settings:", settings)
	fetch(url, settings)
	  .then(status)
	  .then(parseResponse)
	  .then(data =>{
	  	console.log("Response from server: ", data)
	  	return data
	  })
	  .then(processResponse)
	  .then(dispatch)
	  .then(data =>{
	  	dispatch(fetchEnd(asyncProcessesPath))
	  })
	  //todo - put an error action into the catch, or an optional one, inc dispatch fetch end
	  .catch(logError)
}
/*
export const openDialog = path => {
	console.log("actions.Dialog() path", path)
	return {
		type:C.OPEN_DIALOG,
		path:path
	}
}

//todo - impl deleteUser and genralise the openDialog and closeDialog methods into Common file
export const closeDialog = () => {
	console.log("actions.closeDialog() path", path)
	return {
		type:C.CLOSE_DIALOG,
		path:path
	}
}
*/