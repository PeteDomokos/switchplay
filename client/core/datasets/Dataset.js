import React, {Component} from 'react'
import { Route }from 'react-router-dom'
import PropTypes from 'prop-types'
//linked and child components
import AddDatapointContainer from './containers/AddDatapointContainer'
//helpers
import { readDataset } from './api-dataset'

class Dataset extends Component{
	render() {
		const { dataset, loadingDatapoints } = this.props
	    return (
	      <section className='dataset' style={{height:200, margin:20, backgroundColor:'white'}}>
	      		<h3 style={{margin:30, backgroundColor:'white'}}>Dataset</h3>
	      		<Datapoints/>
	      </section>
	    )
	}
}
Dataset.propTypes = {
}
Dataset.defaultProps = {
}

const Datapoints = ({datapoints}) => 
	<div style={{margin:30, backgroundColor:'white'}}>Datapoints</div>

export default Dataset
