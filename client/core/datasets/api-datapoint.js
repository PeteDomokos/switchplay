export const createDatapoint = (params, credentials, datapoint) => {
  console.log("client api-datapoint create() params", params)
  console.log("client api-datapoint create() datapoint", datapoint)
  return fetch('/api/datasets/bygroup/' +params.groupId +'/' +params.datasetId, {
    method: 'PUT',
    headers: {
      'Accept': 'application/json', 
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ' + credentials.t
    },
    body: JSON.stringify(datapoint)
  }).then((response) => {
    return response.json()
  }).catch((err) => console.log(err))



  /*
  return fetch('/api/datasets/bygroup/'+params.groupId+'/'+params.datasetId, {
      method: 'PUT',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + credentials.t
      },
      body: JSON.stringify(datapoint)
    })
    .then((response) => {
      return response.json()
    }).catch((err) => console.log(err))*/
}

