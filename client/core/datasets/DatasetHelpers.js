import { onlyUnique } from '../../util/helpers/ArrayManipulators'

export const categoryAndNameBothSet = dataset =>{
  return dataset.category && changed == 'name' || 
        dataset.name && changed == 'category'
}

export const customDatasetAlreadyDefined = (datasetToCheck, datasets) =>{
  return datasetAlreadyDefined(datasetToCheck, datasets.filter(d => !d.isStandard))
}

export const customDatasetsForGroup = (group) =>{
  if(!group || !group.datasets)
    return []
  return group.datasets.filter(d => !d.isStandard)
}

//validates and processes fields to return dataset
//todo - handle user creating a custom dataset with same identifiers as one they already have,
// custom or standard. Add (custom) and (standard) to names if req.
export const datasetFormProcessor = (userId, group, fields) =>{
  if(datasetAlreadyDefined(fields, group.datasets)){
    return{
      error: 'Duplicate test. Change the category, name or number of participants.'}
  }else
    return{
      category: fields.category,
      name: fields.name,
      nrParticipants:fields.nrParticipants,
      createdBy:userId,
      isStandard:fields.isStandard,
      visibleTo:fields.visibleTo,
      dataValueFormats:[
        //todo - impl options for these, and capitalise name and make units lower case
        {name:'Time', unit:'secs', dataType:'number'},
        {name:'Penalties', dataType:'number'}]
    }
}

export const datapointFormProcessor = (userId, fields) =>{
   const { players, dataValues, eventDate } = fields
   return {
      player: players.length == 1 ? players[0]._id : undefined,
      players:players.length > 1 ? players.map(p => p._id) : undefined,
      //in state, we store each value with a name too, but not needed in db
      dataValues:dataValues.map(dv => dv.value),
      eventDate:eventDate ? eventDate : undefined,
      createdBy:userId
    }
}


export const datasetAlreadyDefined = (datasetToCheck, datasets) =>{
  if(datasets.find(d =>
    d.category === datasetToCheck.category &&
    d.name === datasetToCheck.name &&
    d.nrParticipants === datasetToCheck.nrParticipants))
    return true
  return false
}

export const datasetNamePair = (dataset, datasets) =>{
  if(!datasets || !datasets.length)
     return [dataset.name,'']
  if(!dataset)
    return ['','']

  //check uniqueness
  const otherSets = datasets.filter(d => d._id != dataset._id)
  const categoryAndNameAreUnique = otherSets.find(d => 
      d.category === dataset.category && d.name === dataset.name) 
        ? false : true
  //create extra part of name in some cases
  let extraName = ''
  if(dataset.nrParticipants != 1)
    extraName = ' ('+dataset.nrParticipants +' players)'
  else if(!categoryAndNameAreUnique)
    extraName = ' (1 player)'
  return [dataset.name, extraName]
}


export const editPlayers = (currentPlayers, player) =>{
  if(currentPlayers.map(p => p._id).includes(player._id)){
    console.log("removing")
    return currentPlayers.filter(p => p._id !== player._id)
  }
  else{
     console.log("adding")
    return [...currentPlayers, player]
  }
}

export const findAvailableCategories = (selectedDataset, availableDatasets) =>{
  if(!Array.isArray(availableDatasets) || !selectedDataset)
    return []
  let { name, nrParticipants } = selectedDataset
  return availableDatasets
    .map(d => d.category)
    .filter(onlyUnique)
}
export const findAvailableNames = (selectedDataset, availableDatasets) =>{
  //console.log("findAvailableNames selectedDataset", selectedDataset)
  //console.log("findAvailableNames availableDatasets", availableDatasets)
    if(!Array.isArray(availableDatasets) || !selectedDataset)
      return []
  //if cat != '', filter to sets with that cat
  //if nrParts != '', filter remaining sets to sets with that NrParts
  let { category, nrParticipants } = selectedDataset
  return availableDatasets
    .filter(d => !category || d.category === category)
    .filter(d => !nrParticipants || d.nrParticipants === nrParticipants)
    .map(d => d.name)
    .filter(onlyUnique)
}

export const findAvailableNrParticipants = (dataset, availableDatasets) =>{
    if(!Array.isArray(availableDatasets) || !dataset)
    return []
  //if cat != '', filter to sets with that cat
  //if name != '', filter remaining sets to sets with that name
  let { isStandard, category, name } = dataset
  //return 
  let available = availableDatasets
    .filter(d => !category || d.category === category)
    .filter(d => !name || d.name === name)
    .map(d => d.nrParticipants)
    .filter(onlyUnique)
    //.sorted((a,b) => a - b)
  return available
}


//returns the dataset from datasets - this may be a deeper version as datset may only 
//have the 3 required fields
export const findDataset = (dataset, datasets) =>{
  return datasets.find(d => 
    d.category === dataset.category &&
    d.name === dataset.name &&
    d.nrParticipants === dataset.nrParticipants)
}

/**
*   filters out any standard datasets that are already in group.datasets
**/
export const findAvailableStandardDatasets = (group, datasets) =>{
  //console.log("findAvailableStandardDatasets() group:", group)
  if(!group || !group.datasets ||!Array.isArray(group.datasets) || !datasets)
    return []
  let standardGroupDatasets = group.datasets.filter(d => d.isStandard)
  //console.log("standardGroupDatasets", standardGroupDatasets)
  //console.log("datasets to compare to group", datasets)
  //return those standardDatasets not in group already 
  let available = datasets.filter(d => {
    let inGroup = standardGroupDatasets.find(grpDataset =>
      grpDataset.name === d.name && 
      grpDataset.category === d.category && 
      grpDataset.nrParticipants === d.nrParticipants)
    return !inGroup
  })
  //console.log("available:", available)
  return available
}

export const findUniqueCategories = (datasets) =>{
  if(!datasets)
    return []
  return datasets.map(set => set.category)
                 .filter(onlyUnique)
}

export const merge = datasets => {
  //todo - impl this.....................................
  return datasets
}
export const namePairsForCategory = (category, datasets) =>{
  if(!category || !datasets.length)
    return ['','']
  return datasets.filter(dataset => dataset.category === category)
                 .map(dataset => datasetNamePair(dataset, datasets))
}

export const standardDatasetAlreadyDefined = (datasetToCheck, datasets) =>{
  return datasetAlreadyDefined(datasetToCheck, datasets.filter(d => d.isStandard))
}

export const standardDatasetsForGroup = (group) =>{
  if(!group || !group.datasets)
    return []
  return group.datasets.filter(d => d.isStandard)
}

/**
*
**/
export const visibilityValue = (value) => {
  switch(value){
    case 'all-users':{
      return ['group-admin', 'group-coaches', 'group-members', 'all-users']
    }
    case 'group-members':{
      return ['group-admin', 'group-coaches', 'group-members']
    }
    case 'group-coaches':{
      return ['group-admin', 'group-coaches']
    }
    case 'group-admin':{
      return ['group-admin']
    }
  }
}