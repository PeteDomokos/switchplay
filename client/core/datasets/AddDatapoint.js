import React, {Component} from 'react'
import {Redirect} from 'react-router-dom'
import PropTypes from 'prop-types'
//material-ui
import {Card, CardActions, CardContent} from '@material-ui/core'
import Typography from '@material-ui/core/Typography'
import Button from '@material-ui/core/Button'
import Icon from '@material-ui/core/Icon'
//styles
import {withStyles} from '@material-ui/core/styles'
//child components
import { AddDatapointGateway } from '../../util/components/Gateways'
import { AddDatapointFields } from './AddDatapointFields'
import { SelectGroup } from './InputFields'
import { SelectDataset } from './SelectDataset'
import { DialogWrapper, LoadingAlert } from '../../util/components/Dialog'
//helpers
import auth from '../../auth/auth-helper'
import { readGroup } from '../../groups/api-group.js'
import { createDatapoint } from './api-datapoint.js'
import { datapointFormProcessor, findDataset, findAvailableNrParticipants, editPlayers, 
  standardDatasetAlreadyDefined, customDatasetAlreadyDefined } from './DatasetHelpers'

const styles = theme => ({
   card: {
    maxWidth: '70vw',
    margin: 'auto',
    textAlign: 'center',
    marginTop: theme.spacing(5),
    marginBottom: theme.spacing(5),
    paddingBottom: theme.spacing(2)
  },
  error: {
    verticalAlign: 'middle'
  },
  title: {
    marginTop: theme.spacing(2),
    color: theme.palette.openTitle
  },
  textField: {
    marginLeft: theme.spacing(1),
    marginRight: theme.spacing(1),
    width: '240px'
  },
  submit: {
    margin: 'auto',
    marginBottom: theme.spacing(2)
  },
  selectBtns: {
    display:'flex',
    flexDirection: 'column'
  },
  selectBtn: {
    margin: 'auto',
    marginTop: theme.spacing(2)
  },
  additionalFields:{
  }

})
//helper
const defaultNrParticipants = (dataset, datasets) =>{
  let availableNrParticipants = 
          findAvailableNrParticipants(dataset, datasets)
  if(availableNrParticipants.includes(1))
    return 1
  else
    return ''
}

class AddDatapoint extends Component {
  state = {
    dataset:{category:'', name:'', nrParticipants:''},
    fields:{players:[], dataValues:[], eventDate:Date.now()},
    error:''
  }

  handleDatasetChange = key => event =>{
    let updatedDataset, updatedFields 
    let updatedPlayers = this.state.fields.players

    let { dataset, fields } = this.state
    let { group } = this.props
    let { value } = event.target
    updatedDataset = {...dataset, [key]:value}
    //if category changed, reset all dataset fields
    if(key === 'category' && value !== dataset.category){
      updatedDataset.name = ''
      updatedDataset.nrParticipants = ''
    }
    //if cat and name set, we do other operations
    if(updatedDataset.category && updatedDataset.name){
      //both fields set so we can identify the dataset and merge with teh full datset from group, even with a default nrParticpants if necc.
      //at the end, we will merge with the selected dataset from group
      const updatedDatasetBeforeMerge = {...dataset, [key]:value}
      //set nrParticipants to 1 by default if 1 is available 
      //and if nrparticipants not set
      if(!dataset.nrParticipants && key !== 'nrParticipants'){
        updatedDatasetBeforeMerge.nrParticipants = 
          defaultNrParticipants(updatedDatasetBeforeMerge, group.datasets)
      }

      //todo - set default player
      //merge all fields here with the dataset from group
      var fullDatasetFromGroup
      if(standardDatasetAlreadyDefined(updatedDatasetBeforeMerge, group.datasets) && 
         customDatasetAlreadyDefined(updatedDatasetBeforeMerge, group.datasets)){
        //must ask user which one they want. for now, default to standard and warn user
        alert("you have a custom and standard dataset with this set up. The standard one is being returned")
        fullDatasetFromGroup = 
          findDataset(updatedDatasetBeforeMerge, group.datasets.filter(d => d.isStandard))
      }else
        fullDatasetFromGroup = findDataset(updatedDatasetBeforeMerge, group.datasets)
      //merge
      updatedDataset = { ...updatedDatasetBeforeMerge, ...fullDatasetFromGroup }
    }
    this.setState({
      dataset:updatedDataset, 
      fields:{...fields, players:updatedPlayers}
    })
  }
  handleDatapointChange = key => event => {
    //if only 1 participant, simply replace the 1 item array with this player
    if(key == 'player'){
      console.log("event value", event.target.value)
      //bug - had to use playerId as value as group from server returns a slightly different object 
      //for each player so in Select in shows up as out of range
      //This will be resolved anyway when I stop daving teh whole group 
      //just pass through the par of tge group to reducer thant needs to be saved

      //todo - generalise this so we pass in this.props.group[key]
      //todo------const player = findById(event.target.value, this.props.group.players)
      const player = this.props.group.players.find(p => p._id == event.target.value)
      console.log("player", player)
      if(this.state.dataset.nrParticipants == 1)
        this.setState(state =>{
          return {fields:{...state.fields, players:[player]}}
        })
      else{
        const updatedPlayers = editPlayers(this.state.fields.players, player)
        this.setState(state =>{
          return {fields:{...state.fields, players:updatedPlayers}}
        })
      }
    }
    else if(key.name){
      //target.value is a dataValue
      //merge the value with the format so we can identify each one

      const newValue = Number(event.target.value)
      if(isNaN(newValue))
        alert("Each value must be a number")
      else{
        const newDataValue = {...key, value:Number(event.target.value)}
        //console.log("newDataValue", newDataValue)
        const otherValues = this.state.fields.dataValues.filter(dv => dv.name !== key.name)
        //console.log("otherValues", otherValues)
        const updatedValues = [...otherValues, newDataValue]
        //console.log("setting state updatedValues", updatedValues)
        this.setState(state =>{
          return {fields:{...state.fields, dataValues:updatedValues}}
        })
      }
    }
    else if(key == 'date'){
      const { value } = event.target
      const date = new Date(value).getTime() 
      console.log("date", date)
      this.setState(state =>{
            return {fields:{...state.fields, eventDate:date}}
          })
    }
  }
  resetValues = () =>{
     this.setState(state => {
        return {
          fields:{...state.fields, dataValues:[]}
        }
      })
  }
  reset = () =>{
    //dont reset availableGroups
    this.setState({
      dataset:{category:'', name:'', nrParticipants:''},
      fields:{players:[], dataValues:[], eventDate:Date.now()},
      error:''
    })
  }

  clickSubmit = () => {

    const jwt = auth.isAuthenticated()
    const { dataset, fields } = this.state
    const { group } = this.props
    const datapoint = datapointFormProcessor(jwt.user._id, fields)
    if(datapoint.error)
      this.setState({error: datapoint.error})
    else
      this.props.onSubmit(datapoint, this.props.group._id, dataset._id)     
  }
  render(){
    const { dataset, fields } = this.state
    console.log("eventDate!!!!!!!!!!!!!!!!!!!!!!!!!!!", fields.eventDate)
    const { group, availableGroups, updating, classes, resetStatus } = this.props
    //updating has either...pending, complete, error
    const participantsSelected = 
      fields.players.length > 0 || fields.coach ? true : false
    const requiredValuesEntered = dataset.dataValueFormats &&
      dataset.dataValueFormats.length === fields.dataValues.length ? true : false
    const handlers={
      handleDatasetChange:this.handleDatasetChange,
      handleDatapointChange:this.handleDatapointChange
    }
     const dialogButtonActions = updating.error ? 
      [
        {label:'Try again', onClick:this.clickSubmit},
        {label:'Return Home', link:'/'}
      ]
      :
      [
        {label:'Add More Data', 
          onClick: () =>{
            this.resetValues()
            resetStatus()
          }
        },
        {label:'Return Home', link:'/'}
      ]
    const datasetSelected = dataset.category && dataset.name && dataset.nrParticipants
    return (
      <div>
        {!group ? 
          <AddDatapointGateway groups={availableGroups} withSubgroups/>
          :
          <Card className={classes.card}>
            <CardContent>
              <Typography 
                type="headline" component="h1" className={classes.title}>
                Add Test Datapoint
              </Typography>
              <Selections 
                group={group} dataset={dataset} fields={fields} 
                handlers={handlers} classes={classes}/>
              {this.state.error && 
                <Typography component="p" color="error">
                  <Icon color="error" className={classes.error}>error</Icon>
                  {this.state.error}
                </Typography>}
            </CardContent>

            <CardActions>
            {datasetSelected && requiredValuesEntered &&
              <Button color="primary" variant="contained" onClick={this.clickSubmit} 
                className={classes.submit}>Save</Button>}
            {datasetSelected &&
              <Button color="primary" variant="contained" onClick={this.reset} 
                className={classes.submit}>Start Again</Button>}
            </CardActions>
          </Card>
        }
        <DialogWrapper open={updating.complete || updating.error} title='New Data'
          mesg={updating.error ? updating.error : 'Data entry successfully created'} 
          buttons={dialogButtonActions} />
      </div>
      )
  }
}

AddDatapoint.propTypes = {
}
AddDatapoint.defaultProps = {
  availableGroups:[]
}

const Selections = ({group, dataset, fields, handlers, classes}) =>{
  const datasetSelected = dataset.category && dataset.name && dataset.nrParticipants
  return(
    <React.Fragment>
      <SelectDataset 
        selectedDataset={dataset} availableDatasets={group.datasets} 
        handleDatasetChange={handlers.handleDatasetChange} />
          
      {datasetSelected &&
        <AddDatapointFields group={group} classes={classes}
          dataset={dataset} fields={fields}  
          handleChange={handlers.handleDatapointChange}/>}
    </React.Fragment>
    )
}
  
export default withStyles(styles)(AddDatapoint)
