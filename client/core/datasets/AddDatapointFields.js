import React, {Component} from 'react'
import {Redirect} from 'react-router-dom'
import PropTypes from 'prop-types'
//material-ui
import {Card, CardActions, CardContent} from '@material-ui/core'
import Typography from '@material-ui/core/Typography'
import Button from '@material-ui/core/Button'
import Icon from '@material-ui/core/Icon'
import Select from '@material-ui/core/Select'
import MenuItem from '@material-ui/core/MenuItem'
import TextField from '@material-ui/core/TextField'
import { MuiPickersUtilsProvider } from '@material-ui/pickers'
//other third party
import MomentUtils from '@date-io/moment'
import * as moment from 'moment';
//child components
import { SelectGroup } from './InputFields'
import { SelectDataset } from './SelectDataset'
import { DialogWrapper, LoadingAlert } from '../../util/components/Dialog'
//helpers
import auth from '../../auth/auth-helper'
import { readGroup } from '../../groups/api-group.js'
import { findAvailableNrParticipants } from './DatasetHelpers'
  
export const AddDatapointFields = ({group, dataset, fields, handleChange, classes}) =>{
  //console.log("AddDatapointFields group", group)
  //console.log("AddDatapointFields dataset", dataset)
  //console.log("AddDatapointFields fields", fields)
  return(
    <React.Fragment>
      {dataset.nrParticipants == 1 ?
        <SelectPlayer 
          selectedPlayer={fields.players.length == 1 ? fields.players[0] : ''}
          availablePlayers={group.players} handleChange={handleChange}/>
        :
        <SelectPlayers selectedPlayers={fields.players} availablePlayers={group.players} 
          nrParticipants={dataset.nrParticipants} handleChange={handleChange}/>
      }
    <MuiPickersUtilsProvider utils={MomentUtils}>
      <SelectEventDate handleChange={handleChange} selectedDate={fields.eventDate}
        classes={classes}/>
    </MuiPickersUtilsProvider>
      <AddValues dataValues={fields.dataValues} 
        dataValueFormats={dataset.dataValueFormats} handleChange={handleChange}/>
    </React.Fragment>
    )
}
const SelectPlayer = ({title, selectedPlayer, availablePlayers, handleChange}) => {
return(
  <div>
    <h4 style={{margin:5}}>{title ? title : "Select a player"}</h4>
    <Select 
      value={selectedPlayer ? selectedPlayer._id : ''}
      labelId="label" id="select-player" 
      onChange={handleChange('player')}
      style={{minWidth:120}}>
      {availablePlayers.map(player =>
        <MenuItem key={'select-'+player.firstName} value={player._id}>{player.firstName} {player.surname}</MenuItem>
      )}
    </Select>
  </div>
  )}

SelectPlayer.defaultProps = {
  availablePlayers:[]
}

const SelectPlayers = ({selectedPlayers, availablePlayers, nrParticipants, handleChange}) => {
   const playerIndex = nrParticipants ? [...Array(nrParticipants).keys()] :[]
   return(
    <div>
      <h4 style={{margin:5}}>Select Players</h4>
      <div style={{display:'flex', justifyContent:'space-around'}}>
        {playerIndex.map(i =>{
          return(
            <SelectPlayer 
              title={'Player '+(playerIndex[i]+1)} selectedPlayer={selectedPlayers[i]}
              availablePlayers={availablePlayers} handleChange={handleChange} 
              key={'players-'+i}/>
            )})}
      </div>
    </div>
    )
}

SelectPlayers.defaultProps = {
  availablePlayers:[]
}

//todo - move wrapper to root of app
/*
2020 18:37:55 GMT+0000 (Greenwich Mean Time)" does not conform 
to the required format.  The format is "yyyy-MM-ddThh:mm" 
followed by optional ":ss" or ":ss.SSS".*/
const SelectEventDate = ({handleChange, classes}) =>
  <div style={{margin:30}}>
    <form className={classes.container} noValidate>
    <TextField
      id="datetime-local"
      label="Date and Time"
      type="datetime-local"
      onChange={handleChange('date')}
      defaultValue={moment().format("YYYY-MM-DDTHH:mm")}
      className={classes.textField}
      InputLabelProps={{
        shrink: true,
      }}/>
    </form>
  </div>

const valuesStyles={
  card:{},
  title:{fontSize:16},
  label:{margin:10, marginBottom:0},
  textField:{maxWidth:120},
  units:{}
}
//warning - two uses of the word value - 
//values of the datapoint, and the value property of the textfield
const AddValues = ({dataValueFormats, dataValues, handleChange}) => {
  //helper
  const textFieldValue = dataValueFormat =>{
    let dataValue = dataValues.find(dv => dv.name === dataValueFormat.name)
    if(dataValue == undefined)
      return  ''
    return dataValue.value
  } 
  return(
    <Card style={valuesStyles.card}>
      <CardContent>
        <Typography type="headline" component="h2" style={valuesStyles.title}>
          Values
        </Typography>
        {dataValueFormats.map((format,i) =>
          <div key={format.name}>
          <h3 style={valuesStyles.label}>{format.name}</h3>
          <TextField id={"value-"+i} label="value" style={valuesStyles.textField} 
            value={textFieldValue(format)} 
            onChange={handleChange(format)} 
            margin="normal"/><br/>
          </div>
        )}
      </CardContent>
    </Card>
    )
}

AddValues.defaultProps = {
  dataValueFormats:[],
  values:[]
}

/*
STUFF FOR MAKING NAME INTO NAMEPAIR


const AddDatasetFields = ({group, fields, availableGroups, handleChange}) =>{
  todo - get rid of name pairs from here - we want user to be able to just select a name
  liek touch, and then select and change teh nrparts. namepairs should be used to show the 
  name elsewhere, such as in AddDatapoint, when user is just selecting from their own 
  datesets that they have already added
  console.log("fields", fields)
  //note- existing custom datasets are available so user can select rather than type,
  //but if identical combination of category, name and nrParticipants chosen, an
  //error is shown on submit
  let availableCategories, availableNames, availableNrParticipants
  let availableNamePairs, selectedNamePair

  //note, 2 diff processes for Select functionality for standard and custom
  if(fields.isStandard){
    const groupStandardDatasets = group.datasets ? group.datasets.filter(d => d.isStandard) : []
    //standard datasets that user hasnt added previously, and hence are still available
    const availableStandardDatasets = findAvailableStandardDatasets(group, standardDatasets)
    availableCategories = findUniqueCategories(availableStandardDatasets)
    availableNamePairs = namePairsForCategory(fields.category, groupStandardDatasets)
    selectedNamePair = datasetNamePair(fields, group.datasets)
    availableNrParticipants =
      findAvailableNrParticipantsFromCatAndName(
          fields.category, fields.name, availableStandardDatasets)

  }else{
    //custom datasets that user has previously created, to enable easy re-use of titles
    const existingCustomeDatasets = customDatasets(group)
    availableCategories = findUniqueCategories(customDatasets(group))
    availableNames = findAvailableNames(fields, group.datasets.filter(d => !d.isStandard))
    //all unused numbers from 0 to 20 can be selected (0 means any number)
    availableNrParticipants = [...Array(21).keys()] 
    //todo - remove once used already, 
    //then make them appear in list with (used) and a diff colour
  }

  console.log("availableCategories", availableCategories)
  //names to select from
  console.log("availableNamePairs", availableNamePairs)
  console.log("availableNames", availableNames)
  console.log("availableNrParticipants", availableNrParticipants)
  return(
    <React.Fragment>
      <AddCategory datasetIsStandard={fields.isStandard} selectedCategory={fields.category} 
        availableCategories={availableCategories} handleChange={handleChange}/>

      {fields.category && fields.isStandard &&
        <SelectStandardName selectedNamePair={selectedNamePair} 
          availableNamePairs={availableNamePairs} handleChange={handleChange}/>}
      {fields.category && !fields.isStandard &&
        <AddCustomName selectedName={fields.name} 
          availableNames={availableNames} handleChange={handleChange}/>}

      {fields.category && fields.name &&
        <SelectNrParticipants datasetIsStandard={fields.isStandard} nrParticipants={fields.nrParticipants} 
          availableNrParticipants={availableNrParticipants} handleChange={handleChange}/>}
      {fields.category && fields.name && fields.nrParticipants &&
        <SelectVisibility 
          visibleTo={fields.visibleTo} handleChange={handleChange}/>}
    </React.Fragment>
    )
}

const AddCategory = ({datasetIsStandard, selectedCategory, availableCategories, handleChange}) => {
  const categoryIsFromSelect = availableCategories.includes(selectedCategory)
  const directionMesg = datasetIsStandard ? 
    'Select a category' : 'Or choose one of your existing categories'
  return(
    <div>
      {!datasetIsStandard &&
        <TextField id="category" label="Category"
            value={selectedCategory} 
            onChange={handleChange('category')} margin="normal"/>}
      <div>
        <h4 style={{margin:5}}>{directionMesg}</h4>
        <Select value={categoryIsFromSelect ? selectedCategory : ''}
           labelId="label" id="select" onChange={handleChange('category')}
           style={{minWidth:120}}>
          {availableCategories.map(category =>
            <MenuItem value={category} key={category}>{category}</MenuItem>
          )}
        </Select>
      </div>
    </div>
    )
}
const AddCustomName = ({selectedName, availableNames, handleChange}) =>
 <div>
  <TextField id="name" label="Name"
      value={selectedName} onChange={handleChange('name')} margin="normal"/>
  {availableNames.length > 0 &&
    <div>
      <h4 style={{margin:5}}>Or choose a name you've used before</h4>
      <Select value={availableNames.includes(selectedName) ? selectedName : ''}
        labelId="label" id="select" onChange={handleChange('name')}
        style={{minWidth:120}}>
        {availableNames.map(name =>
          <MenuItem key={name} value={name}>{name}</MenuItem>
        )}
      </Select>
    </div>}
</div>

AddCustomName.defaultProps = {
  availableNames:[]
}

const SelectStandardName= ({selectedNamePair, availableNamePairs, handleChange}) =>
  <div>
    <div>
      <h4 style={{margin:5}}>Select a name</h4>
      <Select value={selectedNamePair[0]}
        labelId="label" id="select" onChange={handleChange('name')}
        style={{minWidth:120}}>
        {availableNamePairs.map(pair =>
          <MenuItem key={pair[0]} value={pair[0]}>{pair[0]}{pair[1]}</MenuItem>
        )}
      </Select>
    </div>
  </div>
SelectStandardName.defaultProps = {
  availableNamePairs:[]
}
*/