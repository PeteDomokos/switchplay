import React, {Component} from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'

import { addDataset, resetStatus } from '../../../actions/Groups'

import AddDataset from '../AddDataset'


//initially the group is sent from user.groups, but we also make a fetch call from here i think
//to get the rest of group needed for groupSummary
const mapStateToProps = (state, ownProps) => {
	console.log("AddDatasetContainer state", state)
	const { groupId, datasetId } = ownProps.match.params
	return({
		availableGroups:state.user.adminGroups,
		//group undefined in no groupId param
		group:state.user.groups.find(g => g._id === groupId),
		//if group loading, group.datasets etc will be undefined 
		loadingGroup:state.asyncProcesses.loading.group,
		updating:state.asyncProcesses.updating.group.datasets
	})
}
const mapDispatchToProps = dispatch => ({
	onSubmit(dataset, groupId){
		dispatch(addDataset(dataset, groupId))
	},
	resetStatus(){
		dispatch(resetStatus('updating.group.datasets'))
	}
})

//wrap all 4 sections in the same container for now.
const AddDatasetContainer = connect(
	mapStateToProps,
	mapDispatchToProps
	)(AddDataset)

export default AddDatasetContainer

