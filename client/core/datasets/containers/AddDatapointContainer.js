import React, {Component} from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'

import { addDatapoint, resetStatus } from '../../../actions/Groups'

import AddDatapoint from '../AddDatapoint'


//initially the group is sent from user.groups, but we also make a fetch call from here i think
//to get the rest of group needed for groupSummary
const mapStateToProps = (state, ownProps) => {
	//console.log("AddDatapointContainer state", state)
	const { groupId, datasetId } = ownProps.match.params
	return({
		availableGroups:state.user.adminGroups,
		//group undefined in no groupId param
		group:state.user.groups.find(g => g._id === groupId),
		//if group loading, group.datasets etc will be undefined 
		loadingGroup:state.asyncProcesses.loading.group,
		updating:state.asyncProcesses.updating.dataset.datapoints
	})
}
const mapDispatchToProps = dispatch => ({
	onSubmit(datapoint, groupId, datasetId){
		dispatch(addDatapoint(datapoint, groupId, datasetId))
	},
	resetStatus(){
		dispatch(resetStatus('updating.dataset.datapoints'))
	}
})

//wrap all 4 sections in the same container for now.
const AddDatapointContainer = connect(
	mapStateToProps,
	mapDispatchToProps
	)(AddDatapoint)

export default AddDatapointContainer

