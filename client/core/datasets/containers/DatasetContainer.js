import React, {Component} from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'

import { fetchDatapoints } from '../../../actions/Dashboard'

import DatasetLoader from '../DatasetLoader'


//initially the group is sent from user.groups, but we also make a fetch call from here i think
//to get the rest of group needed for groupSummary
const mapStateToProps = (state, ownProps) => {
	console.log("DatasetContainer state", state)
	const { groupId, datasetId } = ownProps.match.params
	return({
		groupId:groupId,
		dataset: state.user.groups
			.find(g => g._id === groupId)
			.datasets
			.find(d => d._id === datasetId),
		loadingDatapoints: state.asyncProcesses.loading.datapoints,
		//if group loading, group.datasets etc will be undefined 
		loadingGroup:state.asyncProcesses.loading.group
	})
}
const mapDispatchToProps = dispatch => ({
	onLoadDatapoints(groupId, datasetId){
		dispatch(fetchDatapoints(groupId, datasetId))
	}
})

//wrap all 4 sections in the same container for now.
const DatasetContainer = connect(
	mapStateToProps,
	mapDispatchToProps
	)(DatasetLoader)

export default DatasetContainer

