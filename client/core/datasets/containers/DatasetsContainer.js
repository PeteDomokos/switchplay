import React, {Component} from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'


import Datasets from '../Datasets'


//initially the group is sent from user.groups, but we also make a fetch call from here i think
//to get the rest of group needed for groupSummary
const mapStateToProps = (state, ownProps) => {
	console.log("DatasetsContainer state", state)
	const { groupId } = ownProps.match.params
	return({
		group: state.user.groups.find(g => g._id === groupId)
	})
}
const mapDispatchToProps = dispatch => ({
})

//wrap all 4 sections in the same container for now.
const DatasetsContainer = connect(
	mapStateToProps,
	mapDispatchToProps
	)(Datasets)

export default DatasetsContainer

