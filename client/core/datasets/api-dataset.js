const createDataset = (params, credentials, dataset) => {
  console.log("client api-dataset create()")
  return fetch('/api/datasets/bygroup/'+params.id, {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + credentials.t
      },
      body: JSON.stringify(dataset)
    })
    .then((response) => {
      return response.json()
    }).catch((err) => console.log(err))
}
const listGroupDatasets = (params, credentials) => {
  console.log("dataset list api")
  return fetch('/api/datasets/bygroup/'+params.id, {
    method: 'GET',
    headers: {
      'Accept': 'application/json', 
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ' + credentials.t
    }
  }).then(response => {
    return response.json()
  }).catch((err) => console.log(err))
}
const listUserDatasets = (params, credentials) => {
  console.log("dataset list api")
  return fetch('/api/datasets/byuser/'+params.userId, {
    method: 'GET',
    headers: {
      'Accept': 'application/json', 
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ' + credentials.t
    }
  }).then(response => {
    return response.json()
  }).catch((err) => console.log(err))
}
const listAllDatasets = (credentials) => {
  console.log("dataset list api")
  return fetch('/api/datasets/', {
    method: 'GET',
    headers: {
      'Accept': 'application/json', 
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ' + credentials.t
    }
  }).then(response => {
    return response.json()
  }).catch((err) => console.log(err))
}

const readDataset = (params, credentials) => {
  console.log("dataset read api")
  return fetch('/api/datasets/' + params.id, {
    method: 'GET',
    headers: {
      'Accept': 'application/json', 
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ' + credentials.t
    }
  }).then((response) => {
    return response.json()
  }).catch((err) => console.log(err))
}

const removeDataset = (params, credentials) => {
  return fetch('/api/datasets/' + params.id, {
    method: 'DELETE',
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ' + credentials.t
    }
  }).then((response) => {
    return response.json()
  }).catch((err) => console.log(err))
}
const removeAllDatasets = () => {
  return fetch('/api/datasets/remove-all', {
    method: 'DELETE',
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json'
    }
  }).then((response) => {
    return response.json()
  }).catch((err) => console.log(err))
}
export {
  createDataset,
  listGroupDatasets,
  listUserDatasets,
  listAllDatasets,
  readDataset,
  removeDataset,
  removeAllDatasets
}
