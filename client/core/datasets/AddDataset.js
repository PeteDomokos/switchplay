import React, {Component} from 'react'
import {Redirect, Link} from 'react-router-dom'
import PropTypes from 'prop-types'
//material-ui
import FormControl from '@material-ui/core/FormControl'
import FormControlLabel from '@material-ui/core/FormControlLabel'
import FormLabel from '@material-ui/core/FormLabel'
import Radio from '@material-ui/core/Radio'
import RadioGroup from '@material-ui/core/RadioGroup'
import Paper from '@material-ui/core/Paper'
import {List, ListItem, ListItemAvatar, 
  ListItemSecondaryAction, ListItemText} from '@material-ui/core'
import Icon from '@material-ui/core/Icon'
import IconButton from '@material-ui/core/IconButton'
import Button from '@material-ui/core/Button'
import Typography from '@material-ui/core/Typography'
import Divider from '@material-ui/core/Divider'
import {Card, CardActions, CardContent} from '@material-ui/core'
import TextField from '@material-ui/core/TextField'
import Select from '@material-ui/core/Select'
import MenuItem from '@material-ui/core/MenuItem'
//styles
import {withStyles} from '@material-ui/core/styles'
//linked and child components
import { AddDatasetGateway } from '../../util/components/Gateways'
import { SelectGroup } from './InputFields'
import { DialogWrapper } from '../../util/components/Dialog'
import DatasetsList from './DatasetsList'
//helpers
import auth from '../../auth/auth-helper'
import { visibilityValue, datasetFormProcessor, datasetAlreadyDefined, findAvailableStandardDatasets, 
  customDatasetsForGroup, standardDatasetsForGroup, findUniqueCategories, findAvailableNames,   
  findAvailableNrParticipants } from './DatasetHelpers'
import { onlyUnique } from '../../util/helpers/ArrayManipulators'
//constant
import { standardDatasets } from '../../constants/TestDataConstants'

const styles = theme => ({
  card: {
    maxWidth: '70vw',
    margin: 'auto',
    textAlign: 'center',
    marginTop: theme.spacing(5),
    marginBottom: theme.spacing(5),
    paddingBottom: theme.spacing(2)
  },
  error: {
    verticalAlign: 'middle'
  },
  title: {
    marginTop: theme.spacing(2),
    color: theme.palette.openTitle
  },
  textField: {
    marginLeft: theme.spacing(1),
    marginRight: theme.spacing(1),
    width: '60vw'
  },
  submit: {
    margin: 'auto',
    marginBottom: theme.spacing(2)
  },
  selectBtns: {
    display:'flex',
    flexDirection: 'column'
  },
  selectBtn: {
    margin: 'auto',
    marginTop: theme.spacing(2)
  },
  additionalFields:{
  }
})

/**
*
* @params group(optional) - if group provided, then categroies already 
**/
//private route - only available to signed in users
class AddDataset extends Component {
  state = {
  	  fields:{
        isStandard:true,
        category:'', 
        name:'', 
        nrParticipants:'',
        visibleTo:['group-admin','group-coaches', 'group-players', 'all-users'],
      },
      open: false
  }
  toggleDatasetType = () =>{
    //if set to '', this should change again as soon as server returns group datasets
    //let updatedNrParticipants = this.state.fields.isStandard ? 1 : ''
    let updatedFields = {
        ...this.state.fields, 
        category:'',
        name:'',
        isStandard:!this.state.fields.isStandard//,
        //nrParticipants:updatedNrParticipants
    }
    this.setState({fields:updatedFields})
  }
  handleChange = key => event => {
    //TODO ----BUG ------------IT DOESNT ALLOW CUSTOME TEST WITH SAME CAT AND NAE BUT DIFFERENT NRPARTICIPANTS
    let updatedFields
    const { fields } = this.state
    const { group } = this.props
    //helper
    const updatedValue = key === 'visibleTo' ? 
      visibilityValue(event.target.value) : event.target.value
    updatedFields = {...fields, [key]:updatedValue}
    //reset fields if category changed
    if(key === 'category' && updatedValue !== fields.category){
      updatedFields.name = ''
      updatedFields.nrParticipants = ''
    }
    //do nothing if combination already used - only check for nrParts in case user still typing
    const previouslyAddedNrParticipants 
      = findAvailableNrParticipants(fields, standardDatasetsForGroup(group))
    if(key == 'nrParticipants' && previouslyAddedNrParticipants.includes(updatedValue)){
      alert("You have already used this number with this name. Change the name or the category first.")
      return
    }
    if(datasetAlreadyDefined(updatedFields, group.datasets)){
      alert("already defined")
      this.setState({
          error: 'Duplicate test details. Change either the category, name or number of participants.'})
      return
    }
    //set nrparts to default is custom dataset and cat/name are set
    if(!updatedFields.isStandard && updatedFields.category 
        && updatedFields.name && !updatedFields.nrParticipants){
        updatedFields.nrParticipants = 1
    }
    //update state
    console.log("setting state fields:", updatedFields)
    this.setState({fields:updatedFields})
  }
  clickSubmit = () => {
    const jwt = auth.isAuthenticated()
    const { group } = this.props
    const dataset = datasetFormProcessor(jwt.user._id, group, this.state.fields)
    if(dataset.error)
      this.setState({error: dataset.error})
    else
      this.props.onSubmit(dataset, group._id)
  }
  partialReset = () =>{
    this.setState(state => {
      const _fields = {
        isStandard:state.isStandard,
        category:'',// bug - label is over value if not resetstate.category, 
        name:'', 
        nrParticipants:'',
        visibleTo:state.visibleTo
      }
      return {
        fields:_fields,
        open: false
      }
    })
  }
  reset = () =>{
    this.setState(state => {
      const _fields = {
        isStandard:true,
        category:'', 
        name:'', 
        nrParticipants:'',
        visibleTo:['group-admin','group-coaches', 'group-players', 'all-users']
      }
      return{
        fields:_fields,
        open: false
      }
    })
  }
  render() {
    const {group, availableGroups, updating, classes, resetStatus } = this.props
    //updating has either...pending, complete, error
    const { fields } = this.state
    const dialogButtonActions = updating.error ? 
      [
        {label:'Try again', onClick:this.clickSubmit},
        {label:'Return Home', link:'/'}
      ]
      :
      [
        {label:'Add Another Dataset', 
          onClick: () =>{
            this.partialReset()
            resetStatus()
          }
        },
        {label:'Return Home', link:'/'}
      ]
    
    const toggleMesg = fields.isStandard ? 
      'Create a custom test instead' : 'Choose a standard test instead'
     const toggleButtonLabel = fields.isStandard ? 
      'Custom Test' : 'Standard Test'

    const fieldsComplete = fields.category && fields.name && fields.nrParticipants
    return (
      <div>
        {!group ? 
          <AddDatasetGateway groups={availableGroups} withSubgroups/>
          :
          <Card className={classes.card}>
            <CardContent>
              <Typography type="headline" component="h1" className={classes.title}>
                Add Test Dataset
              </Typography>
              <div style={{margin:30, width:200, backgroundColor:'aqua'}}>Group: {group.name}</div>
              {group.name &&
                <AddDatasetFields group={group} fields={fields} handleChange={this.handleChange}/>}

              {this.state.error && 
                <Typography component="p" color="error">
                  <Icon color="error" className={classes.error}>error</Icon>
                  {this.state.error}
                </Typography>}
            </CardContent>

            <CardActions>
              {fieldsComplete &&
                <Button color="primary" variant="contained" onClick={this.clickSubmit} 
                  className={classes.submit}>Save</Button>}
            </CardActions>

            {group.name &&
              <div style={{display:'flex', justifyContent:'center'}}>
                <h4 style={{margin:10}}>{toggleMesg}</h4>
                <Button style={{margin:10}} color="primary" variant="contained" 
                  onClick={() => this.toggleDatasetType()} className={classes.submit}>
                  {toggleButtonLabel}</Button>
              </div>}
          </Card>
        }
        <DialogWrapper open={updating.complete || updating.error} title='New Dataset'
          mesg={updating.error ? updating.error : 'New Data Entry successfully created'}
          buttons={dialogButtonActions} />
    </div>

    )
  }
}

AddDataset.propTypes = {
}
AddDataset.defaultProps = {
}


const AddDatasetFields = ({group, fields, handleChange}) =>{
  console.log("AddDatasetFields...fields", fields)
  //note- existing custom datasets are available so user can select rather than type,
  //but if identical combination of category, name and nrParticipants chosen, an
  //error is shown on submit
  let availableCategories, availableNames, availableNrParticipants, previouslyAddedNrParticipants

  //note, 2 diff processes for Select functionality for standard and custom
  if(fields.isStandard){
    //standard datasets not yet added
    const availableStandardDatasets = findAvailableStandardDatasets(group, standardDatasets)
    availableCategories = findUniqueCategories(availableStandardDatasets)
    availableNames = findAvailableNames(fields, availableStandardDatasets)
    //the nrParts options from the datasets not currently added
    availableNrParticipants = findAvailableNrParticipants(fields, availableStandardDatasets)
    //all nrParts options from standard datasets, whether added or not
    previouslyAddedNrParticipants = findAvailableNrParticipants(fields, standardDatasetsForGroup(group))
  }else{
    //custom datasets that user has previously created, to enable easy re-use of titles
    const groupCustomDatasets = customDatasetsForGroup(group)
    availableCategories = findUniqueCategories(customDatasetsForGroup(group))
    availableNames = findAvailableNames(fields, customDatasetsForGroup(group))
    //all unused numbers from 0 to 20 can be selected (0 means any number)
    availableNrParticipants = [...Array(21).keys()] 
    //todo - remove once used already, 
    //then make them appear in list with (used) and a diff colour
  }
  return(
    <React.Fragment>
      <AddCategory datasetIsStandard={fields.isStandard} selectedCategory={fields.category} 
        availableCategories={availableCategories} handleChange={handleChange}/>

      {fields.category &&
        <AddName datasetIsStandard={fields.isStandard} selectedName={fields.name} 
          availableNames={availableNames} handleChange={handleChange}/>}

      {fields.category &&
        <SelectNrParticipants datasetIsStandard={fields.isStandard} nrParticipants={fields.nrParticipants} 
          availableNrParticipants={availableNrParticipants} 
          previouslyAddedNrParticipants={previouslyAddedNrParticipants}
          handleChange={handleChange}/>}
      {fields.category && fields.name && fields.nrParticipants &&
        <SelectVisibility 
          visibleTo={fields.visibleTo} handleChange={handleChange}/>}
    </React.Fragment>
    )
}

const AddCategory = ({datasetIsStandard, selectedCategory, availableCategories, handleChange}) => {
  const categoryIsFromMenu = availableCategories.includes(selectedCategory)
  const instruction = datasetIsStandard ? 
    'Select a category' : 'Or choose one of your existing categories'
  return(
    <div>
      {!datasetIsStandard &&
        <TextField id="category" label="Category"
            value={selectedCategory} 
            onChange={handleChange('category')} margin="normal"/>}
      <div>
        <h4 style={{margin:5}}>{instruction}</h4>
        <Select value={categoryIsFromMenu ? selectedCategory : ''}
           labelId="label" id="select" onChange={handleChange('category')}
           style={{minWidth:120}}>
          {availableCategories.map(category =>
            <MenuItem value={category} key={category}>{category}</MenuItem>
          )}
        </Select>
      </div>
    </div>
    )
}
//TODO - MAKE REMOVE ONHOVER HAV CURSOR:POINTER
const AddName = ({datasetIsStandard, selectedName, availableNames, handleChange}) =>{
  const nameIsFromMenu = availableNames.includes(selectedName)
  const instruction = datasetIsStandard ? 
    'Select a name' : 'Or choose a name from your list'

  return(
     <div>
      {!datasetIsStandard &&
        <TextField id="name" label="Name"
          value={selectedName} onChange={handleChange('name')} margin="normal"/>}
      {availableNames.length > 0 &&
        <div>
          <h4 style={{margin:5}}>{instruction}</h4>
          <Select value={nameIsFromMenu ? selectedName : ''}
            labelId="label" id="select" onChange={handleChange('name')}
            style={{minWidth:120}}>
            {availableNames.map(name =>
              <MenuItem key={name} value={name}>{name}</MenuItem>
            )}
          </Select>
          {selectedName &&
            <div style={{color:'red', fontSize:16, marginTop:10, paddingLeft:80}} 
              onClick={handleChange('name')}>remove</div>}
        </div>}
    </div>
    )
}

AddName.defaultProps = {
  availableNames:[]
}

//todo - use thi nstead fo the other component of same name in SelectDataset
const SelectNrParticipants = ({nrParticipants, availableNrParticipants, previouslyAddedNrParticipants, handleChange}) =>{
  const backgroundColour = (number) =>{
    if(nrParticipants === number)
      return 'aqua'
    else if(previouslyAddedNrParticipants.includes(number))
      return 'grey'
    return 'auto'
  }
  console.log("availableNrParticipants", availableNrParticipants)
  console.log("previouslyAddedNrParticipants", previouslyAddedNrParticipants)
  const allNrParticipantsOrdered = 
    [...availableNrParticipants, ...previouslyAddedNrParticipants]
      .sort((a,b) => a-b)
      .filter(onlyUnique)
  const alreadyAdded = number => previouslyAddedNrParticipants.includes(number)
  const extraLabel = number =>{
    if(alreadyAdded(number))
      return ' (already added)'
  }

  return(
    <div style={{margin:30}}>
      <h4 style={{margin:5}}>Number of players in each test</h4>
      <Select labelId="label" id="select" value={nrParticipants ? nrParticipants : ''} 
        onChange={handleChange('nrParticipants')} 
        style={{minWidth:120}}>
         {allNrParticipantsOrdered.map(number =>
           <MenuItem value={number} key={number+1} style={{backgroundColor:backgroundColour(number)}}>
            {number === 0 ? 'Any number' : number}<span style={{fontSize:12}}>{extraLabel(number)}</span></MenuItem>
         )}
      </Select>
      {nrParticipants &&
            <div style={{color:'red', fontSize:16, marginTop:10, paddingLeft:80}} 
              onClick={handleChange('nrParticipants')}>remove</div>}
    </div>
   )
}
SelectNrParticipants.defaultProps = {
  availableNrParticipants:[],
  previouslyAddedNrParticipants:[]
}

const SelectVisibility  = ({visibleTo, handleChange}) => 
  <FormControl component="fieldset">
    <FormLabel component="legend">Data visible to...</FormLabel>
    <RadioGroup onChange={handleChange('visibleTo')} 
      defaultValue="all-users" aria-label="visibility" name="customized-radios">
      <FormControlLabel value="all-users" control={<Radio />} label="everyone" />
      <FormControlLabel value="group-members" control={<Radio />} label="group members only" />
      <FormControlLabel value="group-coaches" control={<Radio />} label="group coaches only" />
      <FormControlLabel value="group-admin" control={<Radio />} label="group admin only" />
    </RadioGroup>
  </FormControl>

export default withStyles(styles)(AddDataset)
