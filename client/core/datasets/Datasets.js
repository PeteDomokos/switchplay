import React, {Component} from 'react'
import { Route, Link, Switch }from 'react-router-dom'
import PropTypes from 'prop-types'
//linked and child components
import DatasetsHome from './DatasetsHome'
import AddDataset from './AddDataset'
import DatasetContainer from './containers/DatasetContainer'
import AddDatasetContainer from './containers/AddDatasetContainer'
//helpers
import auth from '../../auth/auth-helper'



//todo - links to add datapoint will appear in actions section of each dataset in list
//not in datasets menu.
/*
rethink - do we have groupId as params here? should it even be a route at all???
		      			Perhaps it should just be in addDataset, and we render AddDataset with a null param
		      			eg '/group/none/datasets/none/datapoints/new'
		      			and AddDataset then leaves those options unselected, then when user selects them,
		      			it re-renders the page with teh correct params.
		      			It may be worth breaking up teh Add components into a Gateway and the rest.
		      			The gateway deals with group and dataset selections.
		      			We can then rename teh DataDashoboard filter as the Dashboard gateway too for consistency

		      			*/
class Datasets extends Component{
	render() {
    const { group, datasetId } = this.props
    console.log("Datasets")
	    return (
	      <section className='datasets'>
	      		<Switch>
		      		<Route path="/group/:groupId/datasets/new" component={AddDatasetContainer} />
		      		<Route path="/group/:groupId/datasets/:datasetId" component={DatasetContainer}/>
		      	</Switch> 
	      		<DatasetsHome datasets={group.datasets}/>
	      </section>
	    )
	}
}
Datasets.propTypes = {
}
Datasets.defaultProps = {
}

export default Datasets
