import React, { useEffect }  from 'react'
import Dataset from './Dataset'
/**
*
**/
//npt needed for now, but later we will not send datasets' datapoints with group, we will just send 
//all the other details. So then here we will get the datapoints too 
const DatasetLoader = ({groupId, dataset, loadingDatapoints, onLoadDatapoints}) => {
	console.log("DatasetLoader dataset", dataset)
	useEffect(() => {
		if(!dataset.datapoints){
			console.log("DatasetLoader loading dataset datapoints...")
			onLoadDatapoints(group._id, dataset._id)
		}
	}, [])
	return (
		<Dataset dataset={dataset} loadingDatapoints={loadingDatapoints}/>
		)
}

export default DatasetLoader