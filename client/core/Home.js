import React, {Component} from 'react'
import PropTypes from 'prop-types'
import { Link, Redirect } from 'react-router-dom'
//Child Components
import UserProfile from './../user/UserProfile'
import UserGroups from '../groups/UserGroups'
import SystemAdmin from './SystemAdmin'
//helpers
import {signin} from '../auth/api-auth.js'
import auth from '../auth/auth-helper'
//Constants and Data
import homeSections from '../constants/HomeSections'

/**
* Displays a generic home page if no user is authenticated, 
* or the users personalised home page, with links to their players, groups and data, if they are.
* If user is a system admin user, then it also displays a series of additional buttons for systems-related processes
**/

//  TODO - SORT OUT HOEM ESP LINKS IN HOMESECTIONS SHOULD USE RENDERACTIONBUTTONS METHOD
class Home extends Component {
  constructor(props){
    super(props)
    this.handleSectionClick = this.handleSectionClick.bind(this)
    this.demoSignin = this.demoSignin.bind(this)
  }
  /**
  * Uses History.push() to handle clicks on links to other sections
  * @param {string} section - the name of the active section, such as 'about', 'dashboard'
  **/
  //NOTE - for now, this just signs in user as demo sign in
  handleSectionClick(section){
    this.demoSignin(section)
    //this.props.history.push("/"+section)
  }
  /**
  * Calls signin using a demo users details, tehn pushes the section taht was clicked to history
  * @param {string} redirectPath - the name of the active section, such as 'about', 'dashboard'
  **/
  demoSignin(redirectPath){
    let demoUser = {
      email:'deo@y.z',
      password:'dddddd'
    }
    signin(demoUser).then((data) => {
      if(data.error){
        alert("Server error. Please refresh and try again")
      }else{
        auth.authenticate(data, () => {
          //must push to history even if path stays the same so that menu also updates
          if(redirectPath == 'user-home'){
            this.props.history.push("/")
          }
          if(redirectPath == 'dashboard'){
            this.props.history.push("/dashboard")
          }
        })
      }
    })
  }
  render() {
    const { user } = this.props
    //let userObj = auth.isAuthenticated()
    //let user = userObj ? userObj.user : undefined
    //console.log("Home user from auth", user)
    return (
      <section>
        {auth.isAuthenticated() && user._id ?
          <UserHome user={user}/>
        :
          <NonUserHome handleSectionClick={this.handleSectionClick} demoSignin={this.demoSignin}/>
        }
      </section>
    )
  }
}
Home.defaultProps = {
}

const UserHome = ({user}) => 
  <div className='user-home'>
    <div className='screen-fit'>
      <div className='welcome-and-profile'>
        <div className='not-ms welcome-mesg'>
          Welcome back {user.username /*todo - swap for username*/}!
        </div>
        <div className='profile-cont'>
          <UserProfile user={user}/>
        </div>
      </div>
      <UserGroups user={user}/>
    </div>
    <UserLinks/>
    {user.isSystemAdmin && <SystemAdmin/>}
  </div>

const UserLinks = ({}) =>
  <div className='links-outer-cont' style={{marginTop:120}}>
    <div className='links-cont'>
      <div className='links-title'>View...</div>
      <div className='view-links links'>
        <Link to="/groups">
          <button className='btn btn-primary links-btn'>
           Groups</button>
        </Link>
        <Link to="/players">
          <button className='btn btn-primary'>
           Players</button>
        </Link>
        <Link to="/dashboard">
          <button className='btn btn-primary'>
           Data</button>
        </Link>
      </div>
    </div>
    <div className='links-cont'>
      <div className='links-title'>Create new...</div>
      <div className='creation-links links'>
        <Link to="/groups/new">
          <button className='btn btn-primary'>
           Group</button>
        </Link>
        <Link to="/datasets/new-dataset">
          <button className='btn btn-primary'>
           Dataset</button>
        </Link>
        <Link to="/datasets/new-datapoint">
          <button className='btn btn-primary'>
           Datapoint</button>
        </Link>
      </div>
    </div>
    <div className='below-screen'>
    </div>
  </div>

const NonUserHome =({handleSectionClick, demoSignin}) => 
  <div className='home'>
    <div className='intro-banner'>
      <div className='logo-player-gains'>PLAYER GAINS</div>
      <div className='motto'>
        <div>A data system for coaches and players</div>
      </div>

    </div>
    <div className='sections'>
      {homeSections.map((section,i) => 
        <section className={'section '+section.id} key={i}>
          <div className='banner' key={i} onClick={() => handleSectionClick(section.id)}>
              <h2 className='title'>{section.content.heading}</h2>
              <div className='mesg'>
                <p>{section.content.subheading[0]}</p>
                <p>{section.content.subheading[1]}</p>
              </div>
          </div>
          <div className='section-content'>
            {section.id == 'dashboard' ?
                <Link to='/dashboard' style={{textDecoration:'none'}}>
                    <button className='btn btn-primary'>Go (icon)</button>
                </Link>
                :
                <button className='btn btn-primary' 
                  onClick={() => demoSignin(section.id)}>
                  {section.content.buttonText}</button>}
          </div>
        </section>)
      }
    </div>
  </div>

Home.propTypes = {
}

export default Home
