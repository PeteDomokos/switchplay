import React, { useEffect, useState }  from 'react'
import { MatchesMenu } from './MatchesMenu'
/**
*
**/
//Q - what is fetch returns error - how does await handle this? just passes it to teh function i presume
//ie to setAvailableMatches, so is availableMatches then an error object or what?
export const MatchesMenuLoader = () => {
	const [availableMatches, setAvailableMatches] = useState([])
	useEffect(() => {
		const fetchMatches = async () =>{
			const result = await window.fetch('/matches', {
				method:'GET',
				credentials:'same-origin',
				headers:{'Content-Type':'application/json'}
			})
			setAvailableMatches(await result.json())
		}

		fetchMatches()
	}, [])
	return (
		<MatchesMenu availableMatches={availableMatches} />
		)
}