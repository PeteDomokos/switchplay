import React, {Component} from 'react'
import PropTypes from 'prop-types'
import { Link } from 'react-router-dom'
//material-ui
import TextField from '@material-ui/core/TextField'
import Button from '@material-ui/core/Button'
//api helpers
import auth from '../auth/auth-helper'
import {readGroup, listGroups, removeGroup, removeAllGroups} from '../groups/api-group.js'
import {read, list, remove, removeAllUsers} from '../user/api-user.js'
/**
*
**/
class SystemAdmin extends Component {
  constructor(props){
    super(props)
    this.handleListClick = this.handleListClick.bind(this)
    this.handleChange = this.handleChange.bind(this)
    this.handleReadById = this.handleReadById.bind(this)
    this.handleUpdateById = this.handleUpdateById.bind(this)
    this.handleRemoveById = this.handleRemoveById.bind(this)
    this.state = {
      userId:'',
      username:'',
      groupId:'',
      groupName:'',
      updating:'',
      showRemoveAllOptions:false
    } 
  }
  handleChange(key, e){
    this.setState({[key]: e.target.value})
  }
  handleReadById(collection){
    const jwt = auth.isAuthenticated()
    let readFunc
    let id
    switch (collection) {
      case 'group':
        readFunc = readGroup
        id = this.state.groupId
        break;
      default:
        readFunc = read //user
        id = this.state.userId
        break;
    }
    readFunc({id:id}, {t: jwt.token}).then((data) => {
      if (data.error) {
        console.log("read error", data)
      }else{
        console.log("returned:", data)
      }
    })
  }
  handleUpdateById(collection){
    if(collection === 'group'){
      if(!this.state.groupId && !this.state.groupName){
        alert("enter group id or name first")
      }else{
        this.setState({updating:'group'})
      }
    }
  }
  handleRemoveById(collection){
    const jwt = auth.isAuthenticated()
    let removeFunc
    let id
    switch (collection) {
      case 'group':
        removeFunc = removeGroup
        id = this.state.groupId
        break;
      default:
        removeFunc = remove //user
        id = this.state.userId
        break;
    }
    removeFunc({id:id}, {t: jwt.token}).then(data => {
      if (data.error) {
        console.log("removal error", data)
      }else{
        console.log("removed", data)
        this.setState({userId: ''})
      }
    })
  }
  handleListClick(collection){
    let listFunc
    switch (collection) {
      case 'groups':
        listFunc = listGroups
        break;
      default:
        listFunc = list //user
        break;
    }
    listFunc().then(data => console.log('list: ', data))
  }
  render() {
    const styles = {
      systemAdminStyle:{
        padding:20, display:'flex', flexDirection:'column', marginTop:30, marginBottom:100
      },
      titleStyle:{color:'white'},
      listAllStyle:{margin:20},
      findByStyle:{margin:20},
      //children of listAll
      listAllButtonsStyle:{display:'flex'},
      //children of findBy
      textFieldContainerStyle:{display:'flex'},
      buttonContainerStyle:{display:'flex'},
      textFieldStyle:{
        margin:10, padding:5, backgroundColor:'white', width:300, color:'black'
      },
      largeButtonStyle:{width:300, margin:10},
      buttonStyle:{width:100, margin:10},
      groupUpdateStyle:{minHeight:200, margin:20, backgroundColor:'white'}
    }

    const crudHandlers ={
      handleReadById:this.handleReadById,
      handleUpdateById:this.handleUpdateById,
      handleRemoveById:this.handleRemoveById,
    }

    return (
      <section className='system-admin' style={styles.systemAdminStyle}>
        <ListsAdmin handleClick={this.handleListClick} styles={styles}/>
        <UserAdmin id={this.state.userId} username={this.state.username}
          handleUserChange={this.handleChange} crudHandlers={crudHandlers} 
          updating={this.state.updating} styles={styles}/>
        }
        <GroupAdmin id={this.state.groupId} name={this.state.groupName}
          handleGroupChange={this.handleChange} crudHandlers={crudHandlers} 
          updating={this.state.updating} styles={styles}/>
        <button className='btn btn-primary' style={styles.largeButtonStyle}
          onClick={() => {this.setState({
            showRemoveAllOptions:!this.state.showRemoveAllOptions
          })}}>
          {this.state.showRemoveAllOptions ? 'Hide ' : 'Show '} removeAll options</button>
        {this.state.showRemoveAllOptions &&
          <RemoveAllOptions styles={styles}/>}
      </section>
    )
  }
}

SystemAdmin.propTypes = {
}

const ListsAdmin = ({handleClick, styles}) =>
  <div style={styles.listAllStyle}>
    <h3 style={styles.titleStyle}>List all...</h3>
    <div style={styles.listAllButtonsStyle}>
      <button className='btn btn-primary' style={styles.buttonStyle}
       onClick={() => handleClick('users')}>
      users</button>
      <button className='btn btn-primary' style={styles.buttonStyle}
       onClick={() => handleClick('groups')}>
      groups</button>
      <button className='btn btn-primary' style={styles.buttonStyle}
       onClick={() => handleClick('tests')}>
      tests</button>
      <button className='btn btn-primary' style={styles.buttonStyle}
       onClick={() => handleClick('datapoints')}>
      datapoints</button>
    </div>
  </div>

const UserAdmin = ({id, username, crudHandlers, handleUserChange, updating, styles}) =>
  <div style={styles.findByStyle}>
    <h3 style={styles.titleStyle}>Find user by...</h3>
    <div style={styles.textFieldContainerStyle}>
        <TextField id="userId" label="User Id" value={id} 
        onChange={e => handleUserChange('userId', e)} margin="normal"
            style={styles.textFieldStyle}/><br/>
        <TextField id="username" label="Username" value={username} 
        onChange={e => handleUserChange('username', e)} margin="normal"
            style={styles.textFieldStyle}/><br/>
    </div>
    <div style={styles.buttonContainerStyle}>
        <Button color="primary" variant="contained" 
          onClick={() => crudHandlers.handleReadById('user')}
            style={styles.buttonStyle}>read</Button>
        <Button color="primary" variant="contained" onClick={() => crudHandlers.handleUpdateById('user')}
            style={styles.buttonStyle}>update</Button>
        <Button color="primary" variant="contained" onClick={() => crudHandlers.handleRemoveById('user')}
            style={styles.buttonStyle}>delete</Button>
    </div>
  </div>

const GroupAdmin = ({id, name, crudHandlers, handleGroupChange, updating, styles}) =>
  <div style={styles.findByStyle}>
    <h3 style={styles.titleStyle}>Find group by...</h3>
    <div style={styles.textFieldContainerStyle}>
        <TextField id="groupId" label="Group Id" value={id} 
        onChange={(e) => handleGroupChange('groupId', e)} margin="normal"
            style={styles.textFieldStyle}/><br/>
        <TextField id="groupName" label="Group name" value={name} 
        onChange={(e) => handleGroupChange('groupName', e)} margin="normal"
            style={styles.textFieldStyle}/><br/>
    </div>
    <div style={styles.buttonContainerStyle}>
        <Button color="primary" variant="contained" onClick={() => crudHandlers.handleReadById('group')}
            style={styles.buttonStyle}>read</Button>
        <Button color="primary" variant="contained" onClick={() => crudHandlers.handleUpdateById('group')}
            style={styles.buttonStyle}>update</Button>
        <Button color="primary" variant="contained" onClick={() => crudHandlers.handleRemoveById('group')}
            style={styles.buttonStyle}>delete</Button>
    </div>
    {(updating === 'group') && 
      <SystemAdminUpdateGroup styles={styles}/>}
  </div>

//warning -requires remove functions to be imported
const RemoveAllOptions = ({styles}) =>
  <div>
    <button className='btn btn-primary' style={styles.largeButtonStyle} onClick={removeAllUsers}>
        remove all users</button>
    <button className='btn btn-primary' style={styles.largeButtonStyle} onClick={removeAllGroups}>
        remove all groups</button>
    <button className='btn btn-primary' style={styles.largeButtonStyle} onClick={writeDataFromServer}>
        write scores data from server</button>
  </div>

const SystemAdminUpdateGroup = ({styles}) =>
  <div style={styles.groupUpdateStyle}>
    update
  </div>

export default SystemAdmin
