import React, {Component} from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'

import { fetchUser } from '../../actions/User'

import HomeLoader from '../HomeLoader'


const mapStateToProps = state => {
	return({
		user:state.user,
		loadingUser:state.asyncProcesses.loading.user
	})
}
const mapDispatchToProps = dispatch => ({
	onLoadUser(userId){
		dispatch(fetchUser(userId))
	}
})

//wrap all 4 sections in the same container for now.
const HomeContainer = connect(
	mapStateToProps,
	mapDispatchToProps
	)(HomeLoader)

export default HomeContainer

