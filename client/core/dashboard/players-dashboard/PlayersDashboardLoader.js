import React, { useEffect }  from 'react'
import PlayersDashboard from './PlayersDashboard'
import { flattenedDatasets } from '../../../groups/GroupHelpers'

/**
*
**/
const PlayersDashboardLoader = ({players, groups, dashboard, loading, setFilter, LoadDatapoints}) => {
	console.log("PlayersDashboardLoader")
	useEffect(() => {
		if(!loading){
			const datasets = flattenedDatasets(groups)
				.filter(dataset => dashboard.datasets
					.find(d => d._id === dataset._id))

			const unloadedDatasets = datasets.filter(d => !d.datapoints)
			if(unloadedDatasets.length > 0){
				console.log("Loader loading datapoints...")
				loadDatapoints(unloadedDatasets)
			}
		}
	}, [])
	return (
		<PlayersDashboard 
			players={players} groups={groups} dashboard={dashboard} 
			setFilter={setFilter} loading={loading} />)
}

export default PlayersDashboardLoader