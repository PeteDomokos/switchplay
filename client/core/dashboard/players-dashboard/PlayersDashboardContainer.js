import{ Component } from "react";
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { withRouter, Redirect } from 'react-router'

import { setDashboardFilter } from '../../../actions/Dashboard'
import PlayersDashboardLoader from './PlayersDashboardLoader'
import { filterUniqueByProperty } from '../../../util/helpers/ArrayManipulators'
import { getPlayerGroups } from "../../../groups/GroupHelpers"

const mapStateToProps = (state, ownProps) => {
	console.log("PlayersDashboardContainer state", state)
	const players = state.dashboard.players
		.map(p => p[0])
		.map(p => state.otherItems.players
			.find(player => player._id === p._id))

	const userGroups = state.user.groups ? state.user.groups : []
	const otherGroups = state.otherItems.groups ? state.otherItems.groups : []
	const allGroups = [...userGroups, ...otherGroups]
	const playersGroups = players
		.map(p => getPlayerGroups(p, allGroups))
		.reduce((a,b) => [...a, ...b], [])

		//.filter(onlyUniqueByProperty("_id", self)) todo - impl this
	//not sure if self is referencable in this way
	//todo - replace with above
	const uniquePlayersGroups = filterUniqueByProperty("_id", playersGroups)
	//no need for parents, because if player plays for a subgroup, then
	//they must also play for all of its parents
	return({
		players:players,
		groups:uniquePlayersGroups,
		dashboard: state.dashboard,
		loading:state.asyncProcesses.loading.datapoints
	})
}
const mapDispatchToProps = dispatch => ({
	loadDatapoints(groupId, datasetId){
		dispatch(fetchDatapoints(groupId, datasetId))
	},
	setFilter(path, value){
		dispatch(setDashboardFilter(path, value))
	}
})

const PlayersDashboardContainer = connect(
	mapStateToProps,
	mapDispatchToProps
	)(PlayersDashboardLoader)

export default PlayersDashboardContainer