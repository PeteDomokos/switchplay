import React, { useEffect }  from 'react'
import { merge } from '../../datasets/DatasetHelpers'
import { commonGroupIds, flattenedDatasets } from '../../../groups/GroupHelpers'
/**
*
**/
const PlayersDashboard = ({players, groups, dashboard, loading, setFilter}) => {
	console.log("PlayersDashboard")
	//filter the datasets selected
	const allDatasets = flattenedDatasets(groups)
		.filter(dataset => dashboard.datasets
			.find(d => d._id === dataset._id))
	//merge standard sets
	const mergedStandard = merge(allDatasets.filter(d => d.isStandard))
	//only keep custom sets from common groups
	const commonGroups = commonGroupIds(players)
		.map(id => groups.find(g => g._id === id))

	const customDatasetsFromCommonGroups = 
		flattenedDatasets(commonGroups).filter(d => !d.isStandard)
		
	//put together and filter to ones selected
	const datasets = 
		[...mergedStandard, ...customDatasetsFromCommonGroups]
			.filter(dataset => dashboard.datasets
				.find(d => d._id === dataset._id))
	//todo - give namePart to each dataset in case two custom ones 
	//exist in parent and subgroup with same signature

	return (
		<div style={{margin:30, backgrouncolor:'white'}}>
		Players Dashboard
		</div>)
}

export default PlayersDashboard