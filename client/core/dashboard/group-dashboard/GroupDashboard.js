import React, { useEffect }  from 'react'
import { flattenedDatasets } from '../../../groups/GroupHelpers'
/**
*
**/
const GroupDashboard = ({group, dashboard, loading, setFilter}) => {
	console.log("GroupDashboard ")
	const parentsDatasets = flattenedDatasets(group.parents)
	console.log("GroupDashboard parentDatasets", parentsDatasets)

	//put all datasets together then filter the ones selected
	const datasets = [...parentsDatasets, ...group.selected.datasets]
			.filter(dataset => dashboard.datasets
				.find(d => d._id === dataset._id))

	//todo - give namePart to each dataset in case two custom ones 
	//exist in parent and subgroup with same signature

	return (
		<div style={{margin:30, backgrouncolor:'white'}}>
		Group Dashboard
		</div>)
}

export default GroupDashboard