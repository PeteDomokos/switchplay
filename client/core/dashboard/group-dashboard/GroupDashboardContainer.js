import{ Component } from "react";
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { withRouter, Redirect } from 'react-router'

import { setDashboardFilter } from '../../../actions/Dashboard'
import GroupDashboardLoader from './GroupDashboardLoader'
import { getGroupParents } from "../../../groups/GroupHelpers"

const mapStateToProps = (state, ownProps) => {
	console.log("GroupDashboardContainer state", state)
	//params may be undefined
	const { groupId } = ownProps.match.params
	const userGroups = state.user.groups ? state.user.groups : []
	const otherGroups = state.otherItems.groups ? state.otherItems.groups : []
	const allGroups = [...userGroups, ...otherGroups]
	console.log("allGroups", allGroups)
	//we use params instead of dashboard.groups
	const selectedGroup = allGroups.find(g => g._id === groupId)
	console.log("selectedGroup", selectedGroup)
	const parents = getGroupParents(selectedGroup, allGroups)
	return({
		group:{
			selected: selectedGroup,
			parents:parents
		},
		dashboard: state.dashboard,
		loading:state.asyncProcesses.loading.datapoints
	})
}
const mapDispatchToProps = dispatch => ({
	loadDatapoints(datasets){
		dispatch(fetchDatapoints(datasets))
	},
	setFilter(path, value){
		dispatch(setDashboardFilter(path, value))
	}
})

const GroupDashboardContainer = connect(
	mapStateToProps,
	mapDispatchToProps
	)(GroupDashboardLoader)

export default GroupDashboardContainer