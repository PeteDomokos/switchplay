import React, { useEffect }  from 'react'
import GroupDashboard from './GroupDashboard'
import { flattenedDatasets } from '../../../groups/GroupHelpers'
/**
*
**/
const GroupDashboardLoader = ({group, dashboard, loading, setFilter, LoadDatapoints}) => {
	console.log("DashboardLoader")
	useEffect(() => {
		//no need to load group as DashboardSelectors will have loaded it if not loaded
		if(!loading){
			//get each dataset from its stored group, which is where datapoints will be stored
			//put all groups together and go through them to check all required 
			//datasets are loaded
			const parentsDatasets = flattenedDatasets(group.parents)
			const datasets = [...group.selected.datasets, ...parentsDatasets]
				.filter(dataset => dashboard.datasets.find(d => d._id === dataset._id))

			const unloadedDatasets = datasets.filter(d => !d.datapoints)

			if(unloadedDatasets.length > 0){
				console.log("GroupLoader loading datapoints...")
				loadDatapoints(unloadedDatasets)
			}
		}
	}, [])
	return (
		<GroupDashboard 
			group={group} dashboard={dashboard} 
			setFilter={setFilter} loading={loading} />)
}

export default GroupDashboardLoader