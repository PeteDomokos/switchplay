import React, {Component} from 'react'
import PropTypes from 'prop-types'
import { Route, Switch }from 'react-router-dom'
//children
import DashboardSelectorContainer from './DashboardSelectorContainer'
import GroupDashboardContainer from "./group-dashboard/GroupDashboardContainer"
import PlayerDashboardContainer from "./player-dashboard/PlayerDashboardContainer"
import GroupsDashboardContainer from "./groups-dashboard/GroupsDashboardContainer"
import PlayersDashboardContainer from "./players-dashboard/PlayersDashboardContainer"

//todo - Route /user should redirect to "/signin" or to "/user/:userId" if signed in

//no need to provide user with chance to selct a subgroup if group is in params
//because they have click view data from this group.
//we can still have a change link beside the group in the actual dashboard in the filters section
//but if they wanted to views data for the subgroup, they could have first clicked on the subgroup instaed
//of this group from their home page. Because in home opage in groups list, we will have the subgroups listed 
//under each group too, either as sublists displayed, or with a subgroups extension button 

//however, if no params, then the GroupSelector component does provide that option of choosing a subgroup
//and this is the same component that teh user will come to via the gateway if they click change group
//from within the dashboard itself

//note - PlayerDashboard looks at all of one players data, 
//although in includes comparison options with other players and group averages.
//It can also be accessed for within a GroupDashboard via the filters
// and in that case it will only look at the players data from that group
//GroupDashboard also has a highlight function, which highlights/focuses on one player or subgroups data within the group

//note - DashboardSelector is always displayed, but it shrinks into a corner version when a dashboard is rendered
const DashboardRouter = () =>
	<div>
		<Route component={DashboardSelectorContainer} />
		<Switch>
	    	<Route path="/dashboard/group/:groupId" component={GroupDashboardContainer} />
	    	<Route path="/dashboard/player/:playerId" component={PlayerDashboardContainer} />
	    	<Route path="/dashboard/groups" component={GroupsDashboardContainer} />
	    	<Route path="/dashboard/players" component={PlayersDashboardContainer} />
	  	</Switch>
	</div>

export default DashboardRouter