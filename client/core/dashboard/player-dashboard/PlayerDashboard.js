import React, { useEffect }  from 'react'
import { merge } from '../../datasets/DatasetHelpers'
/**
*
**/
const PlayerDashboard = ({player, groups, dashboard, loading, setFilter}) => {
	console.log("PlayerDashboard groups", groups)
	//filter the datasets selected
	const allDatasets = [...groups.map(g => g.datasets)]
		.filter(dataset => dashboard.datasets
			.find(d => d._id === dataset._id))
	console.log("allDatasets", allDatasets)
	//merge standard sets
	const mergedStandard = merge(allDatasets.filter(d => d.isStandard))
	const custom = allDatasets.filter(d => !d.isStandard)
	//put together and filter to ones selected
	const datasets = [...mergedStandard, ...custom]
		.filter(dataset => dashboard.datasets
			.find(d => d._id === dataset._id))
	//todo - give namePart to each dataset in case two custom ones 
	//exist in parent and subgroup with same signature

	return (
		<div style={{margin:30, backgrouncolor:'white'}}>
		Player Dashboard
		</div>)
}

export default PlayerDashboard