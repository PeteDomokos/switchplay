import React, { useEffect }  from 'react'
import GroupsDashboard from './GroupsDashboard'
import { flattenedDatasets } from '../../../groups/GroupHelpers'
/**
*
**/
const GroupsDashboardLoader = 
	({groups, dashboard, loading, setFilter, loadDatapoints}) => {
	console.log("DashboardLoader filters", dashboard.filters)
	useEffect(() => {
		if(!loading){
			//put all groups together and go through them to check all required 
			//datasets are loaded
			const groupsDatasets = flattenedDatasets(groups.selected)
			const parentsDatasets = flattenedDatasets(groups.parents)
			const datasets = [...groupsDatasets, ...parentsDatasets]
				.filter(d => dashboard.datasets.includes(d._id))

			const unloadedDatasets = datasets.filter(d => !d.datapoints)

			if(unloadedDatasets.length > 0){
				console.log("GroupLoader loading datapoints...")
				loadDatapoints(group._id, unloadedDatasets)
			}
		}
	}, [])
	return (
		<GroupsDashboard 
			groups={groups} dashboard={dashboard} 
			setFilter={setFilter} loading={loading} />)
}

export default GroupsDashboardLoader

/*
	(
		<GroupsDashboard 
			user={user} group={group} dashboard={dashboard} 
			setFilter={setFilter} loading={loading} />)

			*/