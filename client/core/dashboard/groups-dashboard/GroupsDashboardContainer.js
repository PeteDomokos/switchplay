import{ Component } from "react";
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { withRouter, Redirect } from 'react-router'

import { setDashboardFilter } from '../../../actions/Dashboard'
import GroupsDashboardLoader from './GroupsDashboardLoader'
import { getParents } from "../../../groups/GroupHelpers"

const mapStateToProps = (state, ownProps) => {
	console.log("GroupsDashboardContainer state", state)
	const userGroups = state.user.groups ? state.user.groups : []
	const otherGroups = state.otherItems.groups ? state.otherItems.groups : []
	const allGroups = [...userGroups, ...otherGroups]
	const selectedGroups = state.dashboard.groups.map(selection => {
		const selected = selection[selection.length-1]
		return allGroups.find(grp => grp._id === selected._id)
		})
	return({
		//groups already loaded in DatasetsSelectorLoader
		groups:{
			selected:selectedGroups,
			parents:getParents(selectedGroups, allGroups)
		},
		dashboard: state.dashboard,
		loading:state.asyncProcesses.loading.datapoints
	})
}
const mapDispatchToProps = dispatch => ({
	//options can be playerIds to filter what we request, and groupIds 
	//so we filter to only players in that group, as it may be a dataset from a parent
	loadDatapoints(datasets, options){
		dispatch(fetchDatapoints(datasets, options))
	},
	setFilter(path, value){
		dispatch(setDashboardFilter(path, value))
	}
})

const GroupsDashboardContainer = connect(
	mapStateToProps,
	mapDispatchToProps
	)(GroupsDashboardLoader)

export default GroupsDashboardContainer