import React, { Component, useState, useEffect } from 'react'
import PropTypes from 'prop-types'
//material-ui
import Button from 'material-ui/Button'
//children
import GroupsSelectorContainer
	from "../../util/components/selectors/containers/GroupsSelectorContainer"
import PlayersSelectorContainer 
	from "../../util/components/selectors/containers/PlayersSelectorContainer"
import DatasetsSelectorContainer 
	from "../../util/components/selectors/containers/DatasetsSelectorContainer"

//this will sit in corner when a dashboard is rendered eg when path != '/dashboard'
//will expand when viewed, so use styles function to change styles

//TODO - DISABLE ONE OF THEM WHEN TEH OTHER IS BEING FILLED IN

//todo - provide additional options including
// - Group dashboard - an inherit option, which allows user to decide groups should inherit 
//all parent datasets, or only restrict the data to datasets defined within that subgroup
// - Player dashboard - an integrate option, which shows all data for players, even if the other
//selected players dont have that data 
const DashboardSelector = ({match, history, dashboard}) =>{
	console.log("DashboardSelector dashboard", dashboard)
	useEffect(() => {
      if(match.isExact && dashboard.selected && dashboard.datasetsSelected){
      	console.log("finding path...")
      		let path
      		const { players, groups } = dashboard

      		if(players){
      			console.log("finding path...players")
      			//we only need players not any subselections
      			const selectedPlayers = players.map(playerArray => playerArray[0])
      			console.log("finding path...selectedPlayers", selectedPlayers)
      			if(selectedPlayers.length == 1)
      				path = '/player/'+selectedPlayers[0]._id
      			else
      				path = '/players'
      		}else{
      			console.log("finding path...groups")
      			//get the selected group fro each groupSelection, could be a subgroup
      			const selectedGroups = groups.map(groupArray => groupArray[groupArray.length-1])
      			console.log("finding path...selectedGroups", selectedGroups)
      			if(selectedGroups.length == 1)
      				path = '/group/'+selectedGroups[0]._id
      			else
      				path = '/groups'
      		}
      		console.log("pushing to ", path)
			history.push(history.location.pathname +path)
		}
    }, [dashboard.datasetsSelected, dashboard.selected])
	window.scrollTo(0, 0)

	const initActive = (dashboard.players && !dashboard.groups) ? "players" : "groups"
	const [active, setActive] = useState(initActive)
	
	/*todo - 
	also provide user with an option to not include inherited datasets
	also check it all works with varius arrangements, but first need to sort createGroup component
	also impl merge*/
	return(
		<div style={{margin:30}}>
			{match.isExact ?
				<Selectors 
					match={match} history={history} dashboard={dashboard} active={active} />
				:
				<div style={{backgroundColor:'white', height:150}}>
					<div>Dashboard Selection Summary here...</div>
					<Button>change</Button>
				</div>}
		</div>
	)
}
const Selectors = ({match, history, dashboard, active}) =>
	<React.Fragment>
		{!dashboard.selected ?
			<div>
				<h2 style={{margin:30, color:'white', fontSize:20}}>Group Dashboard</h2>
				<GroupsSelectorContainer match={match} history={history} 
					active={active == "groups" || active == ""} onActivate={() => setActive("groups")}/>
				
				<h2 style={{margin:30, color:'white', fontSize:20}}>Player Dashboard</h2>
				<PlayersSelectorContainer match={match} history={history} 
					active={active == "players" || active == ""} onActivate={() => setActive("players")}/>
			</div>
			:
			<div>
				<h2 style={{margin:30, color:'white', fontSize:20}}>Choose Datasets</h2>
				<DatasetsSelectorContainer match={match} history={history}/>
			</div>}
	</React.Fragment>

export default DashboardSelector