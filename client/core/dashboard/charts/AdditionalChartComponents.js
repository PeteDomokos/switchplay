import { targetMet } from './GeneralDataHelpers'
import * as d3 from 'd3'
/**
* renders a vert target line given a set of datavalues for a player
* Desc ...
* @param {string} 
* @param {Object}
* @param {Object[]}  
* @param {number} 
**/
const renderVertTargLine = (svgSelector, Lengths, xScale, yScale, datapoints, xKey, yKey, targ, keyNumberOrders) =>{
	let { marginTop, chartHeight, marginLeft } = Lengths
	datapoints.forEach((d,i) =>{
		if(targetMet(d[yKey], targ, keyNumberOrders[yKey]) && 
		  (!targetMet(datapoints[i-1][yKey], targ, keyNumberOrders[yKey]) || i == 0 )) {
			d3.select(svgSelector).append("line").
				attr("class", "targ-met").
				attr("x1", xScale(d[xKey]) +marginLeft).
				attr("x2", xScale(d[xKey]) +marginLeft).
				attr("y1", yScale(targ) +marginTop).
				attr("y2", marginTop +chartHeight).
				style("stroke", "red").
				style("stroke-width", 2).
				style("stroke-dasharray", "5,5")
		}
	})

}

export { renderVertTargLine }