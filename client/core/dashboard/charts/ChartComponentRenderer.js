import * as d3 from 'd3'
import { legendColor } from 'd3-svg-legend'
import D3Helpers from './D3Helpers'
import DisplayItemHelpers from './DisplayItemHelpers'

const ChartComponentRenderer = {
	/**
	* Desc ...
	* @constructor
	* @param {string} 
	* @param {Object}
	* @param {Object[]}  
	* @param {number} 
	**/
	title(lines, svgSelector, Lengths, customSettings){
		let settings = customSettings ? customSettings : {}
		//if lines a single string, make into array of length 1
		let _lines = !Array.isArray(lines) ? [lines] : lines
		//ms and ls
		_lines.forEach((line,i) =>{
			d3.select(svgSelector).append("text").
			attr("class", "svg-title title not-ss "+(settings.classNames ? settings.classNames : '')).
			attr("x", settings.x ? settings.x(i) : Lengths.marginLeft +Lengths.chartWidth/2).
			attr("y", settings.y ? settings.y(i) : Lengths.marginTop*0.5 +i*30).
			style("text-anchor", "middle").
			style("font-size", settings.fontSize ? 
				settings.fontSize(i) : DisplayItemHelpers.fontSize("title", Lengths.wSF, Lengths.hSF, line.length)-i*4).
			style("stroke", settings.stroke ? settings.stroke : "white").
			style("stroke-width", settings.strokeWidth ? settings.strokeWidth(i) : 2-(i*0.8)).
			text(line)
		})
		//ss
		_lines.forEach((line,i) =>{
			d3.select(svgSelector).append("text").
			attr("class", "svg-title title not-ms not-ls "+(settings.classNames ? settings.classNames : '')).
			attr("x", Lengths.marginLeft +Lengths.chartWidth/2).
			attr("y", Lengths.marginTop*0.4 +i*25).
			style("text-anchor", "middle").
			style("font-size", DisplayItemHelpers.fontSize("title", Lengths.wSF, Lengths.hSF, line.length)-i*4).
			style("stroke", settings.stroke ? settings.stroke : "black").
			style("stroke-width", 2-(i*0.3)).
			text(line)
		})

		if(settings.extraStyle)
			settings.extraStyle.forEach(styleNameAndProp => {
				d3.select(svgSelector).select("text.svg-title").
					style(styleNameAndProp[0], styleNameAndProp[1])
			})
		if(settings.extraAttr)
			settings.extraAttr.forEach(attrNameAndProp => {
				d3.select(svgSelector).select("text.svg-title").
					attr(attrNameAndProp[0], attrNameAndProp[1])
			})
	},
	/**
	* params - lines - may just be one line as a string
	*
	**/
	/**
	* Desc ...
	* @constructor
	* @param {string} 
	* @param {Object}
	* @param {Object[]}  
	* @param {number} 
	**/
	//TODO -USE DIVS INSTEAD OF TEXT SO I CAN USE REMAINING LINES AND CENTER THEM UNDER PRIMELINES
	axisLabel(axisType, textLines, svgSelector, Lengths, customSettings){
		let settings = customSettings ? customSettings : {}
		let { marginLeft, marginTop, marginBottom, chartWidth, chartHeight } = Lengths
		let textLinesArray = Array.isArray(textLines) ? textLines : [textLines]
		switch(axisType){
			case "x":{
				textLinesArray.forEach((line,i) =>{
					d3.select(svgSelector).append("text").
					attr("class", "xAxisText").
					//todo - check x-axis titles in other display items - may hav shifted up
					attr("transform", 'translate(' +(marginLeft +chartWidth*0.5) +', ' 
						+(marginTop +chartHeight +(i*18) +marginBottom*0.75) +')').
					style("text-anchor", "middle").
					style("stroke", "white").
					style("font-size", 12).
						//DisplayItemHelpers.fontSize("xLabel", Lengths.wSF, Lengths.hSF, line.length) -(i*4)).
					text(line)
				})
				break 
			}
			case "y":{
				let primeText = textLinesArray[0]
				let primeLines = primeText.split(" ")
				let maxPrimeLineLength = d3.max(primeLines, d => d.length)
				let primeFont = DisplayItemHelpers.fontSize("yLabel", Lengths.wSF, Lengths.hSF, maxPrimeLineLength)
				let primeLineHeight = primeFont*1.5 //inc space between lines
				//get custom transform style if defined
				let customTransformPair = settings.extraAttr ? settings.extraAttr.find(pair => pair[0] == 'transform') : undefined
				let customTransform = customTransformPair ? customTransformPair[1]: undefined
				//ms and ls
				primeLines.forEach((line,i) =>{
					d3.select(svgSelector).append("text").
						//for ms, only display 1st primeLine
						//attr("class", "yAxisText not-ss not-ms"+(i > 0 ? 'not-ms':'')).
						attr("class", "yAxisText not-ss").
						attr("x", Lengths.marginLeft*0.3).
						attr("y", Lengths.marginTop +Lengths.chartHeight*0.4 +i*primeLineHeight).
						//attr("transform", customTransform ? customTransform :
							//'translate(' +Lengths.marginLeft*0.6 +', ' 
								//+(Lengths.marginTop +Lengths.chartHeight*1/3 +i*primeLineHeight) +')').
						style("text-anchor", "middle").
						style("stroke", (settings.stroke ? settings.stroke : "black")).
						style("stroke-width", 1.2). 
						style("font-size", primeFont).
						text(line)
				})
				//we assume a max of 2 sets of text - prime, and subtext, and remaining lines are very short
				if(textLinesArray.length > 1){
					//find amount to shift remaining lines down by
					let primeLinesHeight = primeLines.length*primeLineHeight
					let remainingLines = textLines[1].split(" ")
					let remainingLinesFont = primeFont*0.8
					remainingLines.forEach((line, j) =>{
						d3.select(svgSelector).append("text").
							attr("class", " yAxisText not-ss").
							attr("x", Lengths.marginLeft*0.3).
							attr("y", Lengths.marginTop +Lengths.chartHeight*0.4 
								+primeLinesHeight +j*remainingLinesFont*1.5).
							style("text-anchor", "middle").
							style("stroke", (settings.stroke ? settings.stroke : "black")).
							style("stroke-width", 1). 
							style("font-size", remainingLinesFont).
							text(line)
					})
				}
				
				let singleLine =  textLinesArray[0]
				//ss and ls - centered y-label - for now it is also ms
				/*d3.select(svgSelector).append("text").
					attr("class", "yAxisText").  //prime
					attr("transform", 'translate(' +(Lengths.marginLeft +Lengths.width*0.5) +', ' +(Lengths.marginTop*0.6) +')').
					style("text-anchor", "middle").
					style("font-size", primeFont).
					style("stroke", "white").
					text(singleLine)*/
				//ms - y-label in left corner
				/*d3.select(svgSelector).append("text").
					attr("class", "yAxisText not-ss not-ls ").  //prime
					attr("transform", 'translate(' +(Lengths.marginLeft*0.5) +', ' +(Lengths.marginTop*0.6) +')').
					style("text-anchor", "end").
					style("font-size", primeFont).
					style("stroke", "white").
					text(singleLine)*/
				break
			}
			default:{}
		}
		let _className = axisType == "x" ? "xAxisText" : "yAxisText"
		if(settings.extraStyle)
			settings.extraStyle.forEach(style => {
				d3.select(svgSelector).selectAll("text."+_className).
					style(style[0], style[1])
			})
	},
	
	/**
	*  params - tickInfo - could just be a number for nrTiks, or an object with nrTicks and tickSize
	*
	**/
	/**
	* Desc ...
	* @constructor
	* @param {string} 
	* @param {Object}
	* @param {Object[]}  
	* @param {number} 
	**/ 
	axis(axisType, svgSelector, Lengths, scale, tickInfo, id, customSettings){
		let settings = customSettings ? customSettings : {}
		let { marginLeft, marginTop, chartHeight } = Lengths
		//helper method
		let tickConfig = (nrTicks, tickSize, axis) =>{
			if(nrTicks || nrTicks === 0)
				axis.ticks(nrTicks)
			if(tickSize || tickSize === 0)
				axis.tickSize(tickSize)
		}
		//nrTicks may be passed as a variable or as part of tickInfo object, or may not be passed
		let _nrTicks  
		let _tickSize 
		if(tickInfo || tickInfo === 0){
			if(tickInfo && tickInfo.nrTicks) 
				_nrTicks = tickInfo.nrTicks
			else _nrTicks = tickInfo
		}
		if(tickInfo && tickInfo.tickSize)
			_tickSize = tickInfo.tickSize

		switch(axisType){
			case "x":{
				//id used if more than 1 axis on svg
				let className = id ? ("xAxisG xAxisG-"+id) : "xAxisG"
				let classNames = id ? ("xAxisG "+className) : className
				let axisSelector = id ? ("g."+className) : "g.xAxisG"
				let xAxis = d3.axisBottom().scale(scale)
				tickConfig(_nrTicks, _tickSize, xAxis)

				d3.select(svgSelector).append("g").
					attr("transform", 'translate(' +marginLeft +',' +(marginTop +chartHeight) +')').
					attr("class", classNames).call(xAxis)
				d3.select(axisSelector).selectAll("line").style("stroke-width", 3).style("stroke", "white")
				d3.select(axisSelector).selectAll("text").style("stroke-width", 1.5).style("stroke", "white")
				break
			}
			/* histogram axis
			d3.select(svgSelector).append("g").
				attr("transform", 'translate(' +marginLeft +', ' +(chartHeight +yPos) +')').
				attr("class", xAxisClassName).
				attr("font-size", 16).call(xAxis)
			*/
			case "y":{
				//id used if more than 1 axis on svg
				let className = id ? ("yAxisG-"+id) : "yAxisG"
				let classNames = id ? ("yAxisG "+className) : className
				let axisSelector = id ? ("g."+className) : "g.yAxisG"
				let yAxis = d3.axisLeft().scale(scale)
				tickConfig(_nrTicks, _tickSize, yAxis)
				d3.select(svgSelector).append("g").
					attr("transform", 'translate(' +marginLeft +',' +marginTop +')').
					attr("class", classNames).call(yAxis)
				d3.select(svgSelector).select(axisSelector).selectAll("line")
					.style("stroke-width", 3)
					.style("stroke", "white")
				d3.select(svgSelector).select(axisSelector).selectAll("text")
					.style("stroke-width", 1.5)
					.style("stroke", "white")

				break
			}
		}
	},
	/**
	*    params - postion (opt) - topRight, bottomLeft(default), bottomRight, topCentral, bottomCentral
	*	 warning - for default (botLeft) pos, if more than 3 values then longer words will clash with an xAxis
	**/
	/**
	* Desc ...
	* @constructor
	* @param {string} 
	* @param {Object}
	* @param {Object[]}  
	* @param {number} 
	**/
	//todo - make size and fontsize dependent on Lengths properties
	legend(itemType, svgSelector, fillScale, nrValues, Lengths, position, customSettings){
		let settings = customSettings ? customSettings : {}
		let { marginTop, chartHeight, marginBottom, marginLeft, chartWidth, marginRight} = Lengths
		let xPos
		let yPos
		switch(position){
			case "bottomRight":{
				xPos = marginLeft +chartWidth*0.9
				yPos = marginTop +chartHeight +marginBottom*0.3
				break
			}
			case "topRight":{
				xPos = marginLeft +chartWidth*0.9
				yPos = marginTop*0.2
				break
			}
			case "topLeft":{
				xPos = marginLeft*0.2
				yPos = marginTop*0.4
				break
			}
			default:{ //bottomLeft
				xPos = marginLeft*0.2
				if(nrValues <= 2)
					yPos = marginTop +chartHeight +marginBottom*0.4
				else if(nrValues <= 3)
					yPos = marginTop +chartHeight +marginBottom*0.3
				else
					yPos = marginTop +chartHeight*0.8
			}

		}
		/*
		need to read doc properly for shapes
		let getLegendShape = (chartType) =>
			switch(chartType){
				case "frequency-poly": return 'line'
				case "line": return 'line'
				case "scatter": return 'circle'
				default: return 'rect'
			}
		let legendShape = getLegendShape(chartType)
		//d3.legendSize().shape(getLegendShape(chartType)) -but how to choose colours?
		*/


		//todo - get nrChars too for fontSize function
		let legend = legendColor().scale(fillScale).
		shapeWidth(15*Lengths.wSF).
		shapeHeight(15*Lengths.wSF).
		shapePadding(0)//.
		//orient("horizontal")
		d3.select(svgSelector).append("g").
			attr("class", "legend").
			attr("transform", "translate(" +xPos +"," +yPos +")").
			style("font-size",DisplayItemHelpers.fontSize('subLabel', Lengths.wSF, Lengths.hSF, 7)).
			call(legend)
		//d3.select("g.legend").selectAll("rect").
			//attr("height",DisplayItemHelpers.legendRectHeight(Lengths.hSF)).
			//attr("width",DisplayItemHelpers.legendRectWidth(Lengths.wSF))
			//---todo - make legend size responsive, but this works above so just need to adjust
	},
	/**
	*   params 
	*    - position (opt) - the flex justify-content value. default is already set in component render: flex-end
	*
	**/
	/**
	* Desc ...
	* @constructor
	* @param {string} 
	* @param {Object}
	* @param {Object[]}  
	* @param {number} 
	**/
	//todo - base font, margin and width on Lengths
	buttons(ctrlsSelector, data, Lengths, buttonNameFunc, onClickFunc, position, customSettings){
		let settings = customSettings ? customSettings : {}
		d3.select(ctrlsSelector).selectAll("button.keys").
		   data(data).enter().
		   append("button").
		   style("margin", "0px").
		   style("margin-right", "10px").
		   style("height", Lengths.ctrlsHeight +"px").
		   style("width", (140*Lengths.wSF) +"px").
		   style("font-size", DisplayItemHelpers.fontSize('subLabel', Lengths.wSF, Lengths.hSF, 7)+'px' ).
		   style("vertical-align", 'middle').
		   style("padding", "0").
		   style("padding-left", "2%").
		   style("padding-right", "2%").
		   attr("class", "btn btn-primary").
		   on("click", onClickFunc).
		   html(d => buttonNameFunc(d))
	},
	/**
	* Desc ...
	* @constructor
	* @param {string} 
	* @param {Object}
	* @param {Object[]}  
	* @param {number} 
	**/
	zoomButton(zoomCtrlsSelector, Lengths, onClickFunc, customSettings){
		let settings = customSettings ? customSettings : {}
		d3.select(zoomCtrlsSelector).append("button").
			style("margin", "0px").
			style("margin-left", "10px").
		    style("font-size", "30px").
		    style("width", Lengths.ctrlsHeight +"px").
		    style("height", Lengths.ctrlsHeight +"px").
		    style("border-radius", "100%").
		    on("click", onClickFunc).
		   	html("+") //change to + or - using ?:
	}

}

export default ChartComponentRenderer