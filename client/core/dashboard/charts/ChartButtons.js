import React, {Component} from 'react'
import PropTypes from 'prop-types'
import { renderButtons } from './RenderButtons'
import * as d3 from 'd3'

import { renderShowOptionsButton, renderOptionsButtons, renderSuboptionButtons, updateShowOptionsButtonText, 
	updateOptionsButtonActive, updateOptionsButtonsVis, updateSuboptionsButtonsVis,
	updateSuboptionButtonsActive } from './RenderButtons'
/**
* 
**/
class ChartButtons extends Component{
	constructor(props){
		super(props)
		this.renderButtons = this.renderButtons.bind(this)
	} 
	static propTypes ={
	    buttons:PropTypes.object,
	    clickHandlers:PropTypes.object,
	    config:PropTypes.object
	}
	/**
    * 
    **/
	componentDidMount(){
		let { buttons, config, clickHandlers } = this.props
		this.renderButtons(buttons, config, clickHandlers)
	}
	/**
    * 
    **/
	componentDidUpdate(prevProps, prevState){
		let { buttons, config, clickHandlers } = this.props
		this.renderButtons(buttons, config, clickHandlers)
	}
	/**
	*  Removes buttons from DOM
	**/
	componentWillUnmount(){
		d3.select('div.ctrls-container').selectAll("*").remove()
	}
	renderButtons(){
		let { buttons, clickHandlers } = this.props
		let { optionsAreShown, activeSuboptions, moreButtonSelected, 
		 currentPopup, fillScale, selectedButtonOption} = this.props.config
		//a button for showing and hiding options
		renderShowOptionsButton('div.show-options', clickHandlers.handleShowOptionsButtonClick, currentPopup)
		updateShowOptionsButtonText('div.show-options', optionsAreShown)
		//main options
		renderOptionsButtons('div.options', buttons, clickHandlers.handleOptionButtonClick, currentPopup)
		updateOptionsButtonActive('div.options', selectedButtonOption)
		//suboptions
		renderSuboptionButtons('div.suboptions', buttons[selectedButtonOption], moreButtonSelected, 
			clickHandlers.handleSuboptionButtonClick)
		updateSuboptionButtonsActive('div.suboptions', activeSuboptions, fillScale)
	}
	render(){
		let { optionsAreShown } = this.props.config
		return(
			<div className={'ctrls-container '
				+(optionsAreShown ? "full-height full-width" : "short-height short-width")}>
				<div className={'suboptions'
					+' '+(optionsAreShown ? " show":" hide")} >
				</div>

				<div className='options-container'>
					<div className={'show-options'}>
					</div>
					<div className={'options'
					+' '+(optionsAreShown ? " show":" hide")}>
					</div>
				</div>
			</div>
		)
	}
}

export default ChartButtons

