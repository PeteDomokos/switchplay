import regression from 'regression'
import * as d3 from 'd3'
/**
*   params - test - a test object, with name, catgerory and score propertis such as time
**/
const renderButtons = (buttons, currentState, prevState, clickHandlers) =>{
	let { handleShowOptionsButtonClick, handleOptionButtonClick, handleSuboptionButtonClick } = clickHandlers
	let { selectors, optionsAreShown, selectedButtonOption, yKey, 
		moreButtonsSelected, selectedPlayers, fillScale, changed, buttonsDisabled, currentPopup } = currentState
	let { buttonsArea, showOptionsCtrl, optionsCtrls, ctrls } = selectors
	
	renderShowOptionsButton(showOptionsCtrl, buttonsArea, handleShowOptionsButtonClick, buttonsDisabled, currentPopup)
	updateShowOptionsButtonText(showOptionsCtrl, optionsAreShown)

	renderOptionsButtons(optionsCtrls, buttons, handleOptionButtonClick, buttonsDisabled, currentPopup)
	updateOptionsButtonActive(optionsCtrls, selectedButtonOption)

	renderSuboptionButtons(ctrls, buttons[selectedButtonOption], yKey, moreButtonsSelected, 
		handleSuboptionButtonClick, buttonsDisabled)
	updateSuboptionButtonsActive(ctrls, yKey, selectedPlayers, fillScale)
}
const renderShowOptionsButton = 
	(showOptionsCtrlSelector, buttonsAreaSelector, handleClick, buttonsDisabled, currentPopup) =>{
	d3.select(showOptionsCtrlSelector).selectAll("*").remove()

	d3.select(showOptionsCtrlSelector).
		append("div").
		attr("class", "show-options-button").
		style("position", "relative")

	d3.select(showOptionsCtrlSelector).select("div.show-options-button").
		append("button").
		attr("class", " btn").
		on("click", (button) =>{
			//doesnt need button
			if(buttonsDisabled){
				alert("Press 'End Demo' to explore")
			}
			else
				handleClick()
		})

	d3.select(showOptionsCtrlSelector).select("div.show-options-button").
		append("div").
		attr("class", "show-options-popup button-popup popup").
		style("display", currentPopup == 'show-options' ? 'initial' : 'none').
		text("Click to edit the options")
}
const renderOptionsButtons = 
	(optionsCtrlsSelector, buttons, handleClick, buttonsDisabled, currentPopup) =>{
		let optionsButtons = Object.keys(buttons)
		let mesg = (button) =>{
			switch(button){
				case "players":{
					return "Select the players to see"
				}
				case "fitness":{
					return "Choose a fitness test and view weekly scores"
				}
				case "skills":{
					return "Or choose a skills test instead"
				}
			}
		}
		d3.select(optionsCtrlsSelector).selectAll("*").remove()
		d3.select(optionsCtrlsSelector).selectAll("div.options-button").
			data(optionsButtons).enter().
			append("div").
			attr("class", d => d+"-options-button options-button").
			style("position", "relative")

		d3.select(optionsCtrlsSelector).selectAll("div.options-button").
			append("button").
			html(d => d).
		    attr("class", "btn").
			on("click", (button) => {
				if(buttonsDisabled){
					alert("Press 'End Demo' if you want to try the buttons")
				}
				else
					handleClick(button)
			})

		d3.select(optionsCtrlsSelector).selectAll("div.options-button").
			append("div").
			//todo - use an external func to get pop up number, or label the popup with the button name
			attr("class", d => d+"-button-popup button-popup popup").
			style("display", d => (currentPopup == d+'-button-popup' ? 'initial' : 'none')).
			text(d => mesg(d))
}
//note: currentYKeys may not be an array if only 1 yKey
const renderSuboptionButtons = (ctrlsSelector, reqButtons, activeYKeys, moreButtonsSelected, handleButtonClick, buttonsDisabled) =>{
	//deal with case of ativeYKey being just yKey
	let _activeYKeys =  Array.isArray(activeYKeys) ? activeYKeys : [activeYKeys]
	d3.select(ctrlsSelector).selectAll("*").remove()
	if(reqButtons){
		//up to 10 buttons - group into first5 and rest
		let buttonsToDisplay
		if(reqButtons.length <= 8)
			buttonsToDisplay = reqButtons
		else{
			//MAX 14 BUTTONS FOR NOW - NEED TO INCREASE
			if(!moreButtonsSelected){
				let first7Buttons = reqButtons.slice(0,7)
				let nextButton = {key:'more', butLabel:'more...'}
				buttonsToDisplay = [...first7Buttons, nextButton]
			}else{
				let lastButtons = reqButtons.slice(7,14)
				let prevButton = {key:'back', butLabel:'back...'}
				buttonsToDisplay = [...lastButtons, prevButton]
			}
		}
		//append the buttons
		d3.select(ctrlsSelector).selectAll("button.suboption-button").
		   data(buttonsToDisplay).enter().
		   append("button").
		   html(d => d.butLabel).
		   attr("class", "suboption-button btn").
		   on("click", (button) => {
			   	if(buttonsDisabled){
			   		alert("Press 'End Demo' if you want to try the buttons")
			   	}
				else
			   		handleButtonClick(button)
		   })
	}
}

//TODO - GET RID OF THIS STUFF - CAN USE CLASSNAME AND ATTACHED TO ELEMENT
//IN REACT.RENDER SEEING AS WE ARE USING REACT.STATE NOW
const updateButtonsArea = (buttonsAreaSelector, optionsAreShown) => {
		if(optionsAreShown)
			d3.select(buttonsAreaSelector)
				.classed("full-height", true)
				.classed("full-width", true)
				.classed("short-height", false)
				.classed("short-width", false)
				//.style("flex", "25% 1 1")
				//.style("max-height", "200px")
		else
			d3.select(buttonsAreaSelector)
				.classed("short-height", true)
				.classed("short-width", true)
				.classed("full-height", false)
				.classed("full-width", false)
				//.style("flex", "10% 1 1")
				//.style("max-height", "70px")
}
const updateShowOptionsButtonText = (showOptionsCtrlSelector, optionsAreShown) => {
	d3.select(showOptionsCtrlSelector).select("button").
		html(optionsAreShown ? "Hide":"Options")
}
const updateOptionsButtonActive = (optionsCtrlsSelector, selectedButtonOption) =>{
	d3.select(optionsCtrlsSelector).selectAll("button").
		style("background-color", 
			button => (selectedButtonOption == button ? '#ffa530':'transparent'))
}
const updateOptionsButtonsVis = (optionsCtrlsSelector, optionsAreShown) => {
	if(optionsAreShown){
		d3.select(optionsCtrlsSelector).selectAll("button").
			style("display", "initial")
	}
	else{
		d3.select(optionsCtrlsSelector).selectAll("button").
			style("display", "none")
	}

	//d3.select(optionsCtrlsSelector).selectAll("button").
		//style("opacity", (optionsAreShown ? 1:1))
}
const updateSuboptionsButtonsVis = (ctrlsSelector, optionsAreShown) => {
	if(optionsAreShown)
		d3.select(ctrlsSelector).selectAll("button").
			style("display", "initial")
	else
		d3.select(ctrlsSelector).selectAll("button").
			style("display", "none")

	//d3.select(ctrlsSelector).selectAll("button").
		//style("opacity", (optionsAreShown ? 1:1))
}
const updateSuboptionButtonsActive = (ctrlsSelector, keysActive, selectedPlayers, fillScale) =>{
	//yKeys may not be an array
	let _keysActive = Array.isArray(keysActive) ? keysActive : [keysActive]
	let colour = (key) => {
		if(_keysActive.includes(key))
			return '#ffa530'
		if(selectedPlayers.map(p => p._id).includes(key))
			return fillScale(key)
		return 'transparent'
	}
	d3.select(ctrlsSelector).selectAll("button.suboption-button").
		style("background-color", button => colour(button.key))
}

export { renderButtons, renderShowOptionsButton, renderOptionsButtons, renderSuboptionButtons, updateButtonsArea, updateShowOptionsButtonText, 
	updateOptionsButtonActive, updateOptionsButtonsVis, updateSuboptionsButtonsVis,
	updateSuboptionButtonsActive }