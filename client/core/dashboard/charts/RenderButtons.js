import regression from 'regression'
import * as d3 from 'd3'

/**
* Desc ...
* @param {string} 
* @param {function}
* @param {string} 
**/
const renderShowOptionsButton = 
	(domContainerElement, handleClick, currentPopup) =>{
	d3.select(domContainerElement).selectAll("*").remove()

	d3.select(domContainerElement).
		append("div").
		attr("class", "show-options-button").
		style("position", "relative")

	d3.select(domContainerElement).select("div.show-options-button").
		append("button").
		attr("class", " btn").
		on("click", (button) =>{
			//doesnt need button
			handleClick()
		})

	d3.select(domContainerElement).select("div.show-options-button").
		append("div").
		attr("class", "show-options-popup button-popup small-popup popup").
		style("display", currentPopup == 'show-options' ? 'initial' : 'none').
		text("Edit options")
}
/**
* Desc ...
* @param {string} 
* @param {Object[]}  
* @param {function}
* @param {string}  
**/
const renderOptionsButtons = 
	(domContainerElement, buttons, handleClick, currentPopup) =>{
		let optionsButtons = Object.keys(buttons)
		let mesg = (button) =>{
			switch(button){
				case "players":{
					return "Select the players to see"
				}
				case "fitness":{
					return "Choose a fitness test and view weekly scores"
				}
				case "skills":{
					return "Or choose a skills test instead"
				}
			}
		}
		d3.select(domContainerElement).selectAll("*").remove()
		d3.select(domContainerElement).selectAll("div.options-button").
			data(optionsButtons).enter().
			append("div").
			attr("class", d => d+"-options-button options-button").
			style("position", "relative")

		d3.select(domContainerElement).selectAll("div.options-button").
			append("button").
			html(d => d).
		    attr("class", "btn").
			on("click", (button) => {
				 handleClick(button)
			})

		d3.select(domContainerElement).selectAll("div.options-button").
			append("div").
			//todo - use an external func to get pop up number, or label the popup with the button name
			attr("class", d => d+"-button-popup button-popup popup").
			style("display", d => (currentPopup == d+'-button-popup' ? 'initial' : 'none')).
			text(d => mesg(d))
}
//note: currentYKeys may not be an array if only 1 yKey

/**
* Desc ...
* @param {string} 
* @param {Object[]}  
* @param {boolean}
* @param {function}  
**/
const renderSuboptionButtons = (domContainerElement, reqButtons, moreButtonSelected, handleButtonClick) =>{
	d3.select(domContainerElement).selectAll("*").remove()
	if(reqButtons){
		//up to 10 buttons - group into first5 and rest
		let buttonsToDisplay
		if(reqButtons.length <= 8)
			buttonsToDisplay = reqButtons
		else{
			//MAX 14 BUTTONS FOR NOW - NEED TO INCREASE
			if(!moreButtonSelected){
				let first7Buttons = reqButtons.slice(0,7)
				let nextButton = {key:'more', butLabel:'more...'}
				buttonsToDisplay = [...first7Buttons, nextButton]
			}else{
				let lastButtons = reqButtons.slice(7,14)
				let prevButton = {key:'back', butLabel:'back...'}
				buttonsToDisplay = [...lastButtons, prevButton]
			}
		}
		//append the buttons
		d3.select(domContainerElement).selectAll("button.suboption-button").
		   data(buttonsToDisplay).enter().
		   append("button").
		   html(d => d.butLabel).
		   attr("class", "suboption-button btn").
		   on("click", (button) => {
			   	handleButtonClick(button)
		   })
	}
}

/**
* Desc ...
* @param {string}  
* @param {boolean} 
**/
const updateShowOptionsButtonText = (domContainerElement, optionsAreShown) => {
	d3.select(domContainerElement).select("button").
		html(optionsAreShown ? "Hide":"Options")
}
/**
* Desc ...
* @param {string} 
* @param {string} 
**/
const updateOptionsButtonActive = (domContainerElement, selectedButtonOption) =>{
	d3.select(domContainerElement).selectAll("button").
		style("background-color", 
			button => (selectedButtonOption == button ? '#ffa530':'transparent'))
}
/**
* Desc ...
* @param {string} 
* @param {string}  
**/
const updateOptionsButtonsVis = (domContainerElement, optionsAreShown) => {
	if(optionsAreShown){
		d3.select(domContainerElement).selectAll("button").
			style("display", "initial")
	}
	else{
		d3.select(domContainerElement).selectAll("button").
			style("display", "none")
	}
}
/**
* Desc ...
* @constructor
* @param {string}  
* @param {boolean} 
**/
const updateSuboptionsButtonsVis = (domContainerElement, optionsAreShown) => {
	if(optionsAreShown)
		d3.select(domContainerElement).selectAll("button").
			style("display", "initial")
	else
		d3.select(domContainerElement).selectAll("button").
			style("display", "none")
}
/**
* Desc ...
* @param {string} 
* @param {Object[]}  
* @param {function} 
**/
const updateSuboptionButtonsActive = (domContainerElement, activeSuboptions, fillScale) =>{
	let colour = (button) => {
		if(activeSuboptions.includes(button.key)){
			if(button.type == 'player')
				return fillScale(button.key)
			return '#ffa530'
		}
		return 'transparent'
	}
	d3.select(domContainerElement).selectAll("button.suboption-button").
		style("background-color", button => colour(button))
}

export { renderShowOptionsButton, renderOptionsButtons, renderSuboptionButtons, updateShowOptionsButtonText, 
	updateOptionsButtonActive, updateOptionsButtonsVis, updateSuboptionsButtonsVis,
	updateSuboptionButtonsActive }