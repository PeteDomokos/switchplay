const DisplayItemHelpers = {
	/**
	*   params - title is optional from server
	**/
	displayTitle(statNames, title){
		if(title && title != "") {
			return title
		}
		else{ 
			return this.titleForStats(statNames)
		}
	},
	evenOddColor(i){
		if(i % 2 == 0)
			return 'blue'
		else
			return 'red'
	},
	statTitle(statName){
		switch(statName){
			case "action": return "Match Actions"
			case "pass": return "Passes"
			case "succPasses": return "Completed Passes"
			case "drib": return "Dribbles"
			case "succDrib": return "Completed Dribbles"
			case "shot": return "Shots"
			case "chance": return "Chances Created"
			case "assist": return "Assists"
			case "secAssist": return "Sec Assists"
			case "goal": return "Goals"
			case "APS": return "APS"
			case "actionSuccRate": return "Overall Success Rate (%)"
			default: return statName
		}
	},
	titleForStats(statNames){
		if(statNames.length == 1)
			return [this.firstToUpper(this.statTitle(statNames[0]))]
		if(statNames.length == 2)
			return [this.firstToUpper(this.statTitle(statNames[0])) + " and " + this.firstToUpper(this.statTitle(statNames[1]))]
		if(statNames.length == 3)
			return [this.firstToUpper(this.statTitle(statNames[0])) +", " +this.firstToUpper(this.statTitle(statNames[1])) +" and " + this.firstToUpper(this.statTitle(statNames[2]))]
		if(statNames.length == 4 && statNames.includes("none"))
			return [this.firstToUpper(this.statTitle(statNames[0])) +", " +this.firstToUpper(this.statTitle(statNames[1])) +" and " + this.firstToUpper(this.statTitle(statNames[2]))]
		else
			return ["Various Stats "]
	},
	firstToUpper(str){
		return str.charAt(0).toUpperCase()+ str.slice(1); 
	},
	titleActors(actorNames){
		if(actorNames.length == 1)
			return " for " +actorNames[0]
		if(actorNames.length == 2)
			return " for " +actorNames[0] + " and " + actorNames[1]
		else
			return ""
	},
	//todo- make this also dependent on number of characters, or just make sf more complex when i pass it thru
	// ie sf is combination of wSF, nrChars and nrActors(for table) etc
	fontSize(aspect, wSF=1, hSF=1, nrChars){
		//let expectedNrChars = this.getExpectedNrChars(aspect)
		let adjustedCharSF = 1 
		/*if(nrChars){
			//we want to divide by 3 the effect of the charSF
			let charSF = expectedNrChars/nrChars
			let diff = charSF- 1
			let thirdOfDiff = diff *0.33
			let adjustedCharSF = 1 + thirdOfDiff
		}*/
		switch(aspect){
			case "title": return Math.max(14, 10 + 8*wSF*hSF*adjustedCharSF)
			case "yLabel": return Math.max(12, 8 + 6*wSF*hSF*adjustedCharSF) 
			case "xLabel": return Math.max(12, 8 + 6*wSF*hSF*adjustedCharSF)
			case "subLabel": return Math.max(10, 4 + 4*wSF*hSF*adjustedCharSF)
			case "sublabel": return this.fontSize("subLabel", wSF,hSF, nrChars)

			default: return Math.max(8, 6 +10*wSF*hSF*adjustedCharSF) 
		}
	},
	legendRectHeight(SF=1){
		return 15// *SF

	},
	legendRectWidth(SF=1){
		return 15// *SF

	},
	getExpectedNrChars(aspect){
		switch(aspect){
			case "title": return 25
			case "yLabel": return 7
			case "subLabel": return 7
			default: return 12
		}
	}
}
export default DisplayItemHelpers