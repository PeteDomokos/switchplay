import regression from 'regression'
import * as d3 from 'd3'
import colorbrewer from 'colorbrewer'
//general
import { whatHasChanged } from '../GeneralDataHelpers'
import { getScore } from '../TestDataHelpers'
import { renderVertTargLine } from '../AdditionalChartComponents'
import ChartComponentRenderer from '../ChartComponentRenderer'
import DisplayItemHelpers from '../DisplayItemHelpers'
//specifc to Progress
import { chartSizes, filterPlayers, formatPlayersData, calcXScaleData, 
	calcTarget, calcProjectedData, calcYScaleData } from './ProgressHelpers'


/**
* Renders (or re-renders) the X-axis and label
*/
const renderXAxisAndLabel = (svg, sizes, xKey, xScale, keyLabels) =>{
	d3.select(svg).selectAll("g.xAxisG").remove()
		ChartComponentRenderer.axis("x", svg, sizes, xScale, 10)

		d3.select(svg).selectAll("text.xAxisText").remove()
		ChartComponentRenderer.axisLabel("x", keyLabels[xKey], svg, sizes,
		{extraStyle:[["stroke","white"],['font-size', '20px']]})
}

/**
* Renders (or re-renders) the Y-axis, and either a labelor chart title depending on device size
*/
const renderYAxisAndLabelOrTitle = (svg, sizes, yKey, yScale, item) =>{
	let { units, measurements, titles } = item
	d3.select(svg).selectAll("text.yAxisText").remove()
	d3.select(svg).selectAll("g.yAxisG").remove()
	d3.select(svg).selectAll("text.title").remove()
	//yAxis
	ChartComponentRenderer.axis("y", svg, sizes, yScale, 8)
	//yLabel or title
	//check if we need units
	let reqUnits = units[yKey].full.toLowerCase() == measurements[yKey].toLowerCase() ? 
		"" : "(" +units[yKey].short +")"
	let label = measurements[yKey] +" "+reqUnits


	let title
	let yLabel
	if(window.innerWidth < 501){
		//need title but no ylabel (label becomes a secondary title line)
		title = [titles[yKey].full, label]
	}else if(window.innerWidth < 980){
		//need ylabel but no title (title becomes the primary yLabel line(s))
		yLabel = [titles[yKey].short, label]
	}else{
		//need both title and ylabel
		title = titles[yKey].full
		yLabel = label
	}

	if(window.innerWidth > 500){
		ChartComponentRenderer.axisLabel("y", yLabel, svg, sizes, 
			{stroke:"white"})
	}
	if(window.innerWidth < 500 || window.innerWidth > 980){
		ChartComponentRenderer.title(title, svg, sizes, {stroke:'white'})
	}
	
}
/**
*  Renders (or re-renders) the circles for the test results and the projected results 
*  for each selected player each xKey entry (eg week)
*/
const renderDatapoints = (svg, sizes, xKey, yKey, dataWrapper, fillScale, keyNumberOrders) =>{
	let { playersData, playersProjectedData, xScaleData, yScaleData } = dataWrapper
	let { xScale } = xScaleData
	let { yScale } = yScaleData

	//all datapoints are renewed because yScale may have changed due to player changes
	d3.select(svg).selectAll("g.actual").remove()
	d3.select(svg).selectAll("g.proj").remove()
	d3.select(svg).selectAll("path.actual").remove()
	/*playersToRemove.forEach(p =>{
		d3.select(svg).selectAll("g.actual-"+p._id).remove()
		d3.select(svg).selectAll("g.proj-"+p._id).remove()
		d3.select(svg).selectAll("path.actual-"+p._id).remove()
	})*/
	//all players are re-rendered each time because any change may affect yScale
	playersData.forEach(playerData => {
		renderChartGs(svg, playerData, xKey, yKey, xScale, yScale,
			 "actual actual-"+playerData.playerId, sizes, fillScale, keyNumberOrders)
	})
	playersProjectedData.forEach(playerProjData => {
		renderChartGs(svg, playerProjData, xKey, yKey, xScale, yScale,
			 "proj proj-"+playerProjData.playerId, sizes, fillScale, keyNumberOrders)
	})
}
/**
* Renders (or re-renders) the circles for a set of values (real or projected) 
* for each player for each xKey entry (eg week) 
*/
const renderChartGs = (svg, data, xKey, yKey, xScale, yScale, className, sizes, fillScale, keyNumberOrders) =>{
	let { wSF, hSF, marginTop, chartHeight, marginBottom, marginLeft, chartWidth, marginRight} = sizes
	//if actual data, filter data down to max for each week, in case player has two entries in same week
	if(className.includes("actual")){
		//order tests by date and time
		let orderedTests = data.sort((d,e) =>{
			return new Date(d.date) - new Date(e.date);
		})
		//split into weekNrs
		let testsByWeek = d3.nest().key(d => d.weekNr).entries(orderedTests)
		let bestTestsEachWeek = testsByWeek.map(nest => nest.values)
												.map(testsThatWeek =>{
			//find best score
			let bestScore
			if(keyNumberOrders[yKey] == 'low-to-high')
				bestScore = d3.max(testsThatWeek, d => getScore(yKey, d))
			else
				bestScore = d3.min(testsThatWeek, d => getScore(yKey, d))
			//return the first one that has the best score
			return testsThatWeek.find(d => getScore(yKey, d) == bestScore)
		})
	}
	let id = data[0].playerId
	let playerColour = fillScale(id)
	//helpers
	//if projectd score, then score is a simple property
	let yScore = (className, d) => {
		if(className.includes("proj"))
			return d[yKey]
		return d[yKey]
	}
	let circleFill = () =>{
		if(className.includes("actual"))
			return playerColour
		if(className.includes("proj"))
			return "none"
	}

	let circleStroke = (i) => i == data.length-1 ? "none" : playerColour
	//bind
	let updatedChartG = d3.select(svg).selectAll("g." +className).
		data(data, d => JSON.stringify(d))
	//enter
	//clasName can be same for all circles belonging to a player, but separated into proj and actual
	let chartGEnter = updatedChartG.enter().append("g").
		  attr("class", d => className).
		  attr("transform", (d,i) => 
					"translate(" +(xScale(d[xKey])+marginLeft) +", " 
					+(yScale(yScore(className,d))+marginTop) +")"
			)
	chartGEnter.append("circle").
	   attr("r", 5). //d => high;ightedActorNames.includes(d.actorName) ? 12 : 7
	   style("fill", circleFill()). //d => fillColour(d.actorName)).
	   style("stroke", (d,i) => circleStroke(i)).
	   on("mouseover", showInfo).
	   on("mouseout", hideInfo)

	//update 
	updatedChartG.attr("transform", (d,i) => 
		"translate(" +(xScale(d[xKey])+marginLeft) +", " 
					+(yScale(yScore(className,d))+marginTop) +")")
	//exit
	updatedChartG.exit().remove()

	//update the line for this dataset
	//remove prev equivalent line
	d3.select(svg).selectAll("path."+className).remove()
	let lineFunc = d3.line().
		x(d => xScale(d[xKey]) +marginLeft).
		y(d => yScale(yScore(className,d)) +marginTop).
		curve(d3.curveBasis)
	if(!className.includes('proj')){
		d3.select(svg).append("path").
			attr("class", className).
			attr("d", lineFunc(data)).
			attr("fill", "none").
			attr("stroke", playerColour).
			attr("stroke-width", 2)
	}
}
/**
* Renders the target line for player, or the coaches target line if more than 1 player
*/
const renderTargetLine = (svg, sizes, xScale, yScale, xKey, targ, nrOfPlayers, allValues) =>{
	//todo - bug - team target line is not dashed
	let { marginLeft, marginTop, chartWidth } = sizes
	d3.select(svg).selectAll("g.targG").remove()
	let targLineFunc = d3.line().
		x(d => xScale(d[xKey]) +marginLeft).
		y(yScale(targ) +marginTop)
	d3.select(svg).append("g").attr("class", "targG")
	d3.select(svg).select("g.targG").append("path").
		attr("d", targLineFunc(allValues)).
		attr("fill", "none").
		attr("stroke", "red").
		style("stroke-dasharray", "5,5").
		attr("stroke-width", 2)

	let targetText = nrOfPlayers <= 1 ? 'My Target Line' : 'Team Target Line'
	d3.select(svg).select("g.targG").append("text").
		attr("transform","translate(" +(marginLeft +chartWidth*0.2) +"," +(yScale(targ) +marginTop) +")").
		style("text-anchor", "middle").
		style("font-size", 14).
		style("stroke", "red").
		style("dominant-baseline", "hanging").
		text(targetText)

	d3.select(svg).selectAll("line.targ-met").remove()

}
/**
* Applys a shaded rectangle over the last xKey entry (eg week) that has real data values (rather than projected values)
*/
const shadeCurrentWeek = (svg, sizes, xKey, xScaleData, playersData) =>{
	let { marginTop, chartHeight, chartWidth, marginLeft} = sizes
	let { xMaxRoundedUp, xScale } = xScaleData
	let eachPlayersLastXKeyEntry = playersData.map(playerData => d3.max(playerData, d => d[xKey]))
	let lastXKeyEntry = d3.max(eachPlayersLastXKeyEntry, d => d)
	let xIntervalGap = chartWidth/xMaxRoundedUp
	d3.select(svg).selectAll("rect.current-week-shade").remove()
	let rectWidth = xIntervalGap
	d3.select(svg).append("rect").
		attr("class", "current-week-shade").
		attr("x", (xScale(lastXKeyEntry) -rectWidth/2  +marginLeft)).
		attr("y", marginTop).
		attr("width", rectWidth). //check it is xMaxRoundedUp not xMax
		attr("height", chartHeight).
		style("fill", "black").
		style("opacity", 0.2)
}
/**
* Renders background zones based on the expected standards data from props for the current skills 
* or fitness test displayed. ony shows zones close to selected payers' scores
*/
const renderZones = (svg, sizes, item, settings, dataWrapper, allValues) =>{
	let { yKey, xKey } = settings
	let { xScaleData, yScaleData} = dataWrapper
	let { xScale } = xScaleData
	let { yMinMinus, yMaxPlus, yScale } = yScaleData
	let { wSF, hSF, marginTop, chartHeight, marginBottom, marginLeft, chartWidth, marginRight} = sizes
	let { keyNumberOrders, zones } = item
	//stacked area (this must be re-rendered in all cases)
	d3.select(svg).selectAll("path.zone-area").remove()
	d3.select(svg).selectAll("text.zone-name").remove()

	let zonesToShow = (numberOrders) =>{
		let keyZones = zones[yKey]
		let zonesRequired
		if(numberOrders == 'high-to-low'){
			//remember yMax is the worst score
			zonesRequired =keyZones.filter(z => z.upperThreshold < yMaxPlus).
									filter(z => z.lowerThreshold > yMinMinus)
		}else{
			zonesRequired =keyZones.filter(z => z.lowerThreshold < yMaxPlus).
									filter(z => z.upperThreshold > yMinMinus)
		}
		return zonesRequired
	}
	let reqZones = zonesToShow(keyNumberOrders[yKey])
	let lowestZoneNr = reqZones[0].nr  //TODO - WHY IS THIS SOMETIMES UNDEFINED?
	//PROBABLY IF SCORES ARE OUTSIDE THE ZONES - NEEDS TO CHECK THE VALUES
	let highestZoneNr = reqZones[reqZones.length-1].nr
	//amend thresholds to fit chart
	let amendedZones
	if(keyNumberOrders[yKey] == 'high-to-low'){
		amendedZones = reqZones.map(z =>{
			let amendedLowerThreshold = z.nr == lowestZoneNr ? yMaxPlus : z.lowerThreshold
			let amendedUpperThreshold = z.nr == highestZoneNr ? yMinMinus : z.upperThreshold
			return {...z, lowerThreshold:amendedLowerThreshold, 	
						  upperThreshold:amendedUpperThreshold }
		})
	}else{
		amendedZones = reqZones.map(z =>{
			let amendedLowerThreshold = z.nr == lowestZoneNr ? yMinMinus : z.lowerThreshold
			let amendedUpperThreshold = z.nr == highestZoneNr ? yMaxPlus : z.upperThreshold
			return {...z, lowerThreshold:amendedLowerThreshold, 	
						  upperThreshold:amendedUpperThreshold }
		})
	}
	let secondZone = amendedZones[1]
	let lowerThresholdPos = (zone) => yScale(zone.lowerThreshold)
	let upperThresholdPos = (zone) => yScale(zone.upperThreshold)
	let thresholdHeight = (zone) => Math.abs(upperThresholdPos(zone) - lowerThresholdPos(zone))
	//todo - threshold height must be at least 70 - 
	let checkedAmendedZones = amendedZones.map((z,i) =>{
		//todo - impl this - check lowest and highest zones - extend if necc so 70
		//note - will need to extend diff threshols depending on numberOrder
	})

	//todo -move to main method so not repeating zones[yKey].map(z => z.id)
	let zonesFillScale = d3.scaleQuantize().domain([1,zones[yKey].length]).
									   range(colorbrewer.Blues[zones[yKey].length])
	amendedZones.forEach((z,i) =>{
		let colour = i%2 == 0 ? "blue":"aqua"
		//fillsclae for all zones, not just the ones currently showing
		let zoneArea = d3.area().
			x(d => xScale(d[xKey]) +marginLeft).
			y0(d =>  yScale(z.lowerThreshold) +marginTop).
			y1(d => yScale(z.upperThreshold) +marginTop)

		d3.select(svg).append("path").
			attr("class", 'zone-area zone-area-'+z.nr).
			attr("d", zoneArea(allValues)).
			attr("fill",zonesFillScale(z.nr)).
			//style("opacity", 0.3). //todo - use stack order 
			lower()
		let zoneChartHeight = (zone) => Math.abs(yScale(zone.lowerThreshold) - yScale(zone.upperThreshold))
		let zonesAbove = amendedZones.filter(zone => zone.nr > z.nr)
		let zonesAboveChartHeight = zonesAbove.length == 0 ? 
			0 : zonesAbove.map(zone => zoneChartHeight(zone)).reduce((a,b) => a + b)
		let thisZoneChartHeight = zoneChartHeight(z)
		d3.select(svg).append("text").
			attr("class", "zone-name").
			attr("transform", 'translate(' +(marginLeft +chartWidth*0.85) +', ' 
					+ (zonesAboveChartHeight +thisZoneChartHeight/2 +marginTop) +')').
			text(z.name).
			style("text-anchor", "middle").
			style("font-size", 14).
			style("stroke", "white").
			style("opacity", 0.9).
			style("dominant-baseline", "central")
	})

}
const showInfo = (datapoint) => {
	/*let scores = datapoint.tests.map(test => test.name +": " +test.score() +"\n")
	d3.select(svg).selectAll("g."+datapoint.uName).
		append("text").
		attr("class", datapoint.uName +"DescText").
		attr("y", 10).
		text("scores for " +datapoint.uName +"....")
		*/
}
const hideInfo = (datapoint) =>{
	/*
	d3.select(svg).selectAll("g." +datapoint.uName).
		select("text." +datapoint.uName +"DescText").remove()
		*/
}

export { renderXAxisAndLabel, renderYAxisAndLabelOrTitle, renderDatapoints, renderChartGs, renderTargetLine, shadeCurrentWeek, renderZones }