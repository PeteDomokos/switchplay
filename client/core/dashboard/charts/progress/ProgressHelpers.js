import regression from 'regression'
import * as d3 from 'd3'
//general
import D3Helpers from '../D3Helpers'
import { calculateXValue, createXScale, createYScale, calculateTarget, createRegressionFunc,
		getMaxValuePlusFromRange, getMinValueMinusFromRange} from '../GeneralDataHelpers'
import { getScore } from '../TestDataHelpers'

/**
* Returns an object containing the size information for the d3 components to use  
* @param {number} 
* @param {number} 
**/
const chartSizes = (width, height) => {
	const sizesLS = {height:height, width:width, wSF:width/550, hSF:height/320, 
				 marginTop:height *0.15, chartHeight:height *0.64, marginBottom:height *0.18,
				 marginLeft:width *0.17, chartWidth:width *0.75, marginRight:width*0.08 }
	const sizesMS = {height:height, width:width, wSF:width/550, hSF:height/320, 
				 marginTop:height *0.11, chartHeight:height *0.68, marginBottom:height *0.18, 
				 marginLeft:width *0.2, chartWidth:width *0.7, marginRight:width*0.1 }
	const sizesSS = {height:height, width:width, wSF:width/550, hSF:height/320, 
				 marginTop:height *0.19, chartHeight:height *0.63, marginBottom:height *0.18, 
				 marginLeft:width *0.11, chartWidth:width *0.82, marginRight:width*0.05 }
	return window.innerWidth < 501 ? sizesSS : window.innerWidth < 980 ? sizesMS : sizesLS

}
/**
* Returns all players in the selectedPlayers array that have at least one data entry
* for the selected test
* @param {Object[]}
* @param {string} selectedOption - the test category currently being views eg skills or fitness
* @param {string} yKey - the test currently being viewed  
**/
const filterPlayers = (selectedPlayers, selectedOption, yKey) =>{
	if(yKey){
		if(selectedPlayers && Array.isArray(selectedPlayers))
			return selectedPlayers.filter(p => p[selectedOption][yKey] && p[selectedOption][yKey].length != 0)
		return []
	}else{
		//implement so it filters for all players that have at least one data entry 
		//for at least one yKey from selectedOption
	}
}

/**
* Returns an array of dataObjects, one for each player, formatted for the d3 chart to use
* @param {Object[]}
* @param {string} 
* @param {string}
* @param {string}  
**/	
const formatPlayersData = (filteredPlayers, selectedOption, yKey, xKey) =>{
	let playersData = filteredPlayers.map(player => {
		let reqTestArr = player[selectedOption][yKey]
		//add playerId to each individual test
		let reqTestArrWithId = reqTestArr.map(testObj => {
			return {...testObj, playerId:player._id}
		})
		//add playerId and startdate to teh array object itself (not needed)
		reqTestArrWithId.startDate = player.startDate
		reqTestArrWithId.playerId = player._id
		return reqTestArrWithId
	})
	//add simple values for d[xKey] and d[yKey] for the xKey and yKey values
	playersData.forEach(player => {
		player.forEach(d =>{
			d[xKey] = calculateXValue(d.date, player.startDate, xKey)
			d[yKey] = getScore(yKey, d)
		})
	})
	return playersData

}

/**
* Returns an object containing the xScale and other associated values
* @param {Object[]}
* @param {string} 
* @param {number} 
* @param {Object}
* @param {Object}  
**/
const calcXScaleData = (playersData, xKey, chartWidth, xMaxes, keyNumberOrders) => {
	let nextXMax
	if(xMaxes[xKey]){
			nextXMax = xMaxes[xKey]
	}else{
		//if no xMax in props, then determine it from players data, or default to 12
		let xMaxDefault = xKey => {
			switch(xKey){
				case 'dayNr': return 30
				case 'weekNr': return 12
				case 'monthNr': return 12
				case 'yearNr': return 5
			}
		}
		let nextXMaxesForPlayers = playersData.map(player => d3.max(player, d => d[xKey]))
		nextXMax = xMaxesForPlayers.length > 0 ? d3.max(nextXMaxesForPlayers, d => d) : xMaxDefault(xKey)
	}
	let nextXMinsForPlayers = playersData.map(player => d3.min(player, d => d[xKey]))
	//default xMin = 0
	let nextXMin = nextXMinsForPlayers.length > 0 ? d3.min(nextXMinsForPlayers, d => d) : 0
	let nextXMaxRoundedUp = Math.ceil(nextXMax)
	let nextXMinRoundedDown = Math.floor(nextXMin)
	let nextXScale = createXScale(nextXMinRoundedDown, nextXMaxRoundedUp, 
		chartWidth, keyNumberOrders[xKey])
	return {xMin:nextXMin, xMinRoundedDown:nextXMinRoundedDown, xMax:nextXMax, 
		xMaxRoundedUp:nextXMaxRoundedUp, xScale:nextXScale}
}

/**
* Returns a target for the player(s). If only 1 player is selected, then it is calculated based on 
* several factors. If more than 1 player selected, then the target is simply the one set by the coach 
* and delivered as props
* @param {Object[]}  
* @param {string} 
* @param {string} 
* @param {string} 
* @param {Object}
* @param {Object}
* @param {Object}
**/
//todo - merge with calculateTarget 
const calcTarget = (playersData, selectedOption, yKey, xKey, xScaleData, targets, coachTargets, keyNumberOrders) =>{
	if(playersData.length > 1)
		return coachTargets[selectedOption][yKey]
	else
		return calculateTarget(playersData[0], xKey, yKey, 
			xScaleData.xMin, xScaleData.xMax, targets, keyNumberOrders)
}
/**
* Returns an array of player arrays, each one of which contains 
* the projected future scores for the current test, based on previous progress
* @param {number}
* @param {Object}
* @param {string}
* @param {string}  
* @param {Object[]}  
* @param {number} 
**/
const calcProjectedData = (targ, playersData, yKey, xKey, xMaxRoundedUp, keyNumberOrders) =>{
	let nextPlayersProjectedData = playersData.map(playerData =>{
		let regressionFunc = createRegressionFunc(playerData, xKey, yKey, xMaxRoundedUp, 
			targ, keyNumberOrders[yKey])
		//rounds to nearest whole for now so it works for weeks -todo - make it depend on xKey
		let lastActualX = d3.max(playerData, d => d[xKey])
		let lastActualXRoundedUp = Math.ceil(lastActualX)
		let firstProjX = lastActualXRoundedUp + 1
		let nrProjValues = (xMaxRoundedUp - lastActualXRoundedUp)
		let projectedXValuesArray = Array.from(new Array(nrProjValues),(val,index)=>firstProjX +index)
		//access  xKey and yKey property keys !
		let projectedData = projectedXValuesArray.
			//map(n => expRegFunc.predict(n)).
			map(n =>  regressionFunc.predict(n)).
			map(datapair => {
				return {[xKey]: datapair[0], [yKey]:datapair[1], playerId:playerData.playerId}
			})
		projectedData.playerId = playerData.playerId
		return projectedData
	})
	return nextPlayersProjectedData
}
/**
* Returns an object containing the yScale and other associated values
* @param {Object[]}
* @param {string} 
* @param {number} 
* @param {Object}
* @param {Object}  
**/
const calcYScaleData = (playersData, playersProjectedData, chartHeight, yKey, targ, keyNumberOrders) =>{
	let nextActualYMaxes = playersData.map(player => d3.max(player, d => d[yKey]))
	//todo - check if we need all these variables to be declared earlier
	let nextActualYMax = d3.max(nextActualYMaxes, d => d)
	//note: projData is just a simple array-pair, no need for helper functions
	let nextProjYMaxes = playersProjectedData.map(playerProj => d3.max(playerProj, d => d[yKey]))
	let nextProjYMax = d3.max([...nextProjYMaxes, targ], d => d)

	let nextYMax = Math.max(nextActualYMax, nextProjYMax)
	if(nextYMax == undefined){
		//default use zones
		let reqZones = zones[yKey]
		nextYMax = keyNumberOrders[yKey] == 'low-to-high'
			? reqZones.find(z => z.nr == d3.max(reqZones, d => d.nr)).upperThreshold
			: reqZones.find(z => z.nr == d3.min(reqZones, d => d.nr)).lowerThreshold
	}
	let nextActualYMins = playersData.map(player => d3.min(player, d => d[yKey]))
	let nextActualYMin = d3.min(nextActualYMins, d => d)
	let nextProjYMins = playersProjectedData.map(playerProj => d3.min(playerProj, d => d[yKey]))
	let nextProjYMin = d3.min([...nextProjYMins, targ], d => d)
	let nextYMin = Math.min(nextActualYMin, nextProjYMin)
	if(nextYMin == undefined){
		//default use zones
		let reqZones = zones[yKey]
		nextYMin = keyNumberOrders[yKey] == 'low-to-high'
			? reqZones.find(z => z.nr == d3.min(reqZones, d => d.nr)).lowerThreshold 
			: reqZones.find(z => z.nr == d3.max(reqZones, d => d.nr)).upperThreshold
	}

	let nextYMaxPlus = D3Helpers.getMaxValuePlusFromRange(nextYMax, nextYMin)
	let nextYMinMinus = D3Helpers.getMinValueMinusFromRange(nextYMax, nextYMin)
	let nextYScale = createYScale(nextYMinMinus, nextYMaxPlus, chartHeight, 
		keyNumberOrders[yKey])

	return {yMin:nextYMin, yMinMinus:nextYMinMinus, yMaxPlus:nextYMaxPlus, yScale:nextYScale}
}

export { chartSizes, filterPlayers, formatPlayersData, calcXScaleData, 
	calcTarget, calcProjectedData, calcYScaleData }