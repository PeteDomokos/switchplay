 import regression from 'regression'
import * as d3 from 'd3'
//specific to Progress
import { chartSizes, filterPlayers, formatPlayersData, calcXScaleData, 
	calcTarget, calcProjectedData, calcYScaleData } from './ProgressHelpers'

/**
* Returns the Formatted data to be used by d3 functionality
* @return {object} formatted data wrapper, containing array of playersData and data for scales
*/
const chartData = (item, settings, sizes, changed) => {
	let { selectedPlayers, selectedOption, yKey, xKey } = settings
	let { xMaxes, keyNumberOrders, targets, coachTargets} = item
	//todo - put conditions eg if(sizesChanged) as not all data needs to update each time
	let filteredPlayers = filterPlayers(selectedPlayers, selectedOption, yKey)
	let playersData = formatPlayersData(filteredPlayers, selectedOption, yKey, xKey)

	let xScaleData = calcXScaleData(playersData, xKey, sizes.chartWidth, xMaxes, keyNumberOrders)

	let targ = calcTarget(playersData, selectedOption, yKey, xKey, xScaleData, 
		targets, coachTargets, keyNumberOrders)

	let playersProjectedData = calcProjectedData(targ, playersData, yKey, 
		xKey, xScaleData.xMaxRoundedUp, keyNumberOrders)
	let yScaleData = calcYScaleData(playersData, playersProjectedData, 
		sizes.chartHeight, yKey, targ, keyNumberOrders)
	
	return { 
		filteredPlayers:filteredPlayers, 
		playersData:playersData,
		xScaleData:xScaleData,
		targ:targ,
		playersProjectedData:playersProjectedData,
		yScaleData:yScaleData
	}
}


export default chartData