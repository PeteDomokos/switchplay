 import regression from 'regression'
import * as d3 from 'd3'
//general
import { renderVertTargLine } from '../AdditionalChartComponents'
//specific to Progress
import chartData from './ProgressChartData'
import { renderXAxisAndLabel, renderYAxisAndLabelOrTitle, renderDatapoints, renderChartGs, 
	renderTargetLine, shadeCurrentWeek, renderZones } from './RenderProgressChartComponents'

/**
* Calls method to calculate and format data for the chart, 
* and then renders the components of the chart (via several subrender methods)
*/
const renderChart = (svg, item, settings, sizes, changed) =>{
	//format all the data necessary for rendering d3 components
	let dataWrapper = chartData(item, settings, sizes, changed)
	//deconstruction
	let { playersData, playersProjectedData, targ, xScaleData, yScaleData } = dataWrapper
	let { selectedPlayers, selectedOption, yKey, xKey, fillScale } = settings

	//axes, labels and title
	if(changed.length == 0 || changed.includes('xKey') || changed.includes('sizes'))
		renderXAxisAndLabel(svg, sizes, xKey, xScaleData.xScale, item.keyLabels)

	if(changed.length == 0 || changed.includes('yKey') || changed.includes('selectedPlayers') 
		|| changed.includes('sizes'))
		renderYAxisAndLabelOrTitle(svg, sizes, yKey, yScaleData.yScale, item)

	//player circles
	renderDatapoints(svg, sizes, xKey, yKey, dataWrapper, fillScale, item.keyNumberOrders)

	//flatten all data
	let allValuesByPlayer = playersData.map(player =>
		player.concat(playersProjectedData.find(playerProj => playerProj.playerId = player.playerId)))
	let allValues = allValuesByPlayer.reduce((player1, player2) => player1.concat(player2))

	//target line
	renderTargetLine(svg, sizes, xScaleData.xScale, yScaleData.yScale, xKey, targ, playersData.length, allValues)
	//target-met vertical line - only do if 1 player
	if(playersData.length == 1){
		renderVertTargLine(svg, sizes, xScaleData.xScale, yScaleData.yScale, playersData[0], xKey, yKey, targ, item.keyNumberOrders)
	}
	if(changed.length == 0 || changed.includes('xKey') || changed.includes('selectedPlayers') || changed.includes('sizes'))
		shadeCurrentWeek(svg, sizes, xKey, xScaleData, playersData)
	
	//background zones
	renderZones(svg, sizes, item, settings, dataWrapper, allValues)
}

export { renderChart }