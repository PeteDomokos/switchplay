import React, {Component} from 'react'
import PropTypes from 'prop-types'
import * as d3 from 'd3'
import { legendColor } from 'd3-svg-legend'
//general
import D3Helpers from '../D3Helpers'
import { updatedSelectedPlayers, whatHasChanged } from '../GeneralDataHelpers'
import { getScore, nrOfDataEntries, filterForPlayersThatHaveData  } from '../TestDataHelpers'
import ChartButtons from '../ChartButtons'
//specific to Progress
import { renderChart } from './RenderProgressChart'
import ProgressAnimation from './ProgressAnimation'
import { chartSizes } from './ProgressHelpers'

/**
* A Scatter Chart which shows the progress over time for 
* the scores of players in sports tests, with buttons which 
* enable to user to select different players and tests
**/
class ProgressSVG extends Component{
	constructor(props){
		super(props)
		this.initChartValues = this.initChartValues.bind(this)
		this.handleShowOptionsButtonClick = this.handleShowOptionsButtonClick.bind(this)
		this.handleOptionButtonClick = this.handleOptionButtonClick.bind(this)
		this.handleSuboptionButtonClick = this.handleSuboptionButtonClick.bind(this)
		this.handlePlayerClick = this.handlePlayerClick.bind(this)
		this.handleAddPlayer = this.handleAddPlayer.bind(this)
		this.handleRemovePlayer = this.handleRemovePlayer.bind(this)
		this.updateState = this.updateState.bind(this)
		this.onAnimationChange = this.onAnimationChange.bind(this)
		this.state = {
			initValuesSet:false,
			chartRendered:false,
			chartStateUpdated:false,
			latestPrimaryChange:'',
			optionsAreShown:false,
			selectedButtonOption:'',
			selectedOption:'',
			moreButtonSelected:false,
			xKey:'',
			yKey:'',
			players:[],
			selectedPlayers:[],  
			buttonsDisabled:false,
			currentPopup:'',
			fillScale:''
		}
	} 
	static propTypes ={
	    item: PropTypes.object,
	}
	/**
    * Calls function that initialises state values based on props, and sets a timeout for window size changes
    **/
	componentDidMount(){
		this.initChartValues()
		//WINDOW CHANGES - sizes will change
		window.addEventListener('resize', () =>{
				this.forceUpdate()
		})
	}
	/**
    * Determines latest svg sizes, and passes them along with other info about state changes to render methods
    **/
	componentDidUpdate(prevProps, prevState){
		//new sizes
		let width = this.svgContainer ? this.svgContainer.getBoundingClientRect().width : 500
		let height = this.svgContainer ? this.svgContainer.getBoundingClientRect().height : 500

		let sizes = chartSizes(width, height)

		//ascertain what has changed to avoid unnecessary re-rendering
		let changed = whatHasChanged(this.state, prevState, width, height)

		let chartSettings = {
			selectedPlayers:this.state.selectedPlayers,
			selectedOption:this.state.selectedOption,
			yKey:this.state.yKey,
			xKey:this.state.xKey,
			fillScale:this.state.fillScale
		}

		renderChart("svg.progress-svg", this.props.item, chartSettings, sizes, changed)
		//ease in svg
		d3.select('svg.progress-svg').style('opacity',1)
	}
	/**
	*  Removes chart from DOM and removes resize listener
	**/
	componentWillUnmount(){
		d3.select(this.state.svg).selectAll("*").remove()
		window.removeEventListener('resize', () => {})
	}
	/**
	*  Sets or resets state to initial values based on props 
	*  Except buttonsDisabled, which is handled in onAnimationChange()
	**/
	initChartValues(){
		let { id, players, initSelectedOption, initXKey, initYKey } = this.props.item
		//INIT VALUES
		let initChartValues = {}
		initChartValues.selectedButtonOption = 'players'
		initChartValues.selectedOption = initSelectedOption
		initChartValues.xKey = initXKey
		initChartValues.yKey = initYKey
		//remove players with no fitness or skills data
		let playersThatHaveData = filterForPlayersThatHaveData(players)
		//TODO - CHECK SORTER WORKS FULLY 
		let playersSortedByRelevance = playersThatHaveData.sort((p1,p2) => {
			return nrOfDataEntries(p1) - nrOfDataEntries(p2)
		})
		initChartValues.players = playersThatHaveData
		initChartValues.fillScale = d3.scaleOrdinal().
			domain(playersThatHaveData.map(p => p._id)).
			range(["blue", "red", "yellow", "orange", "navy","green"])
		//todo - deal wth case of no playersthatHaveData.length == 0 
		//todo - make user be selected by default if they are a player, not denilson
		let denilson = playersThatHaveData.find(p => p.firstName == "Denilson")
		let initPlayer = denilson ? denilson : playersThatHaveData[0]
		initChartValues.selectedPlayers = [initPlayer]

		initChartValues.optionsAreShown = false
		initChartValues.optionsAreShown = false
		initChartValues.moreButtonSelected = false
		initChartValues.buttonsRendered = false
		initChartValues.chartRendered = false
		initChartValues.initValuesSet = true
		this.setState(initChartValues)
	}
    /**
    * Switches the flag on and off to reveal the button controls, 
    * and uses opacity to ensure a smooth transition (buttons and newly sized chart
    * in sync with each other). The chart is smaller when buttons are showing.
    **/
	handleShowOptionsButtonClick(){
		if(!this.state.buttonsDisabled){
			//force-hide all buttons and svg ready for a size change
			d3.select("svg.progress-svg").
				style("transition", 'opacity 200ms ease-in 0ms').
				style("opacity",0)

			d3.select("div.ctrls-container").selectAll('button').
				style("transition", 'opacity 200ms ease-in 0ms').
				style("opacity",0)
			setTimeout(() =>{
				//change sizes and show svg (buttons handled in Buttons component)
				d3.select("svg.progress-svg").style("opacity",1)
				if(!this.state.optionsAreShown){
				}
				else{
				}
				this.setState({optionsAreShown:!this.state.optionsAreShown})
			},300)
		}
	}
    /**
    * Updates selectedButtonOption in state to the option button which has been pressed.
    * This affects which suboption buttons are rendered.
    **/
	handleOptionButtonClick(button){
		if(!this.state.buttonsDisabled){
			this.setState({
				selectedButtonOption:button,
				moreButtonSelected:false
			})
		}
	}
   /**
   	*  Processes the click of a suboption button. If it is the more/back button, 
   	*  it turns this flag on/off state. If it is a player, it calls handlePlayerClick. 
   	*  If it is a yKey (ie a sports test), then it sets this yKey in state.
   	*
   	* @param (string) button - a button object representing either (a) the more/back button, 
   	*                          (b) a player, or (c) a yKey representing a sports test 
   	*                          which is to be measured on the y-axis
    */
	handleSuboptionButtonClick(button){
		let { selectedButtonOption, moreButtonSelected } = this.state
		if(!this.state.buttonsDisabled){
			//if the 'more/back' button clicked, then no need to update chart
	   		if(button.key == 'more' || button.key == 'back')
	   			this.setState({moreButtonSelected:!moreButtonSelected})
			else{
				//button is either a player or a yKey
				if(selectedButtonOption == 'players')
					this.handlePlayerClick(button.key)
				else
					this.setState({
						yKey:button.key,
						selectedOption:selectedButtonOption
					})				
			}
		}
	}
	/**
    * Processes the click of a player button. If this player is already selected,
    * it calls handleRemovePlayer. Otherwise, it calls handleAddPlayer
    * 
    **/
	handlePlayerClick(playerId){
		if(this.state.selectedPlayers.find(p => p._id == playerId))
			this.handleRemovePlayer(playerId)
		else
			this.handleAddPlayer(playerId)
	}
	/**
    * Adds this player to selectedPlayers in state. If the max number of players
    * to display is reached, it alerts the user that they must remove another player.
    **/
	//todo - put limit on players to add depending on swarm chart size
	handleAddPlayer(playerId){
		this.setState(state =>{
			return({
				selectedPlayers:[
					...this.state.selectedPlayers, 
					this.state.players.find(p => p._id == playerId)
					]
				})
		})
	}
	/**
    * Removes this player from selectedPlayers in state. If this is the only player
    * selected, it alerts the user that they must first add another player.
    **/
	handleRemovePlayer(playerId){
		if(this.state.selectedPlayers.length == 1)
			alert("You cannot remove all players. Select another player first.")
		else
			this.setState(state =>{
				return({
					selectedPlayers:this.state.selectedPlayers.filter(p => p._id != playerId)
				})
			})
	}
	
  	/**
   	* ...
    * @param {object} customProps props object (used e.g. when
    */
	updateState(updateObject){
		this.setState(updateObject)
	}
    /**
    * ...
    * @param {string} propName name of the prop you want to get
    */
	onAnimationChange(change){
		if(change == 'end'){
			this.setState({
				buttonsDisabled:false,
				currentPopup:'demo-ctrls'})
			this.initChartValues()
		}
		else if(change == 'start'){
			this.setState({
				buttonsDisabled:true
			})
			this.initChartValues()
		}
	}

	render(){
		//prepare props for buttons
		//add updated players to buttons  
		let requiredButtons = this.props.item.buttons
		requiredButtons.players = this.state.players.map(p => {
	    		return {key:p._id, butLabel:p.firstName, type:'player'}
	    	})
		//determine which suboptions are active - depends on selectedButtonOption
		let activeSuboptions = this.state.selectedButtonOption == 'players' ?
			this.state.selectedPlayers.map(p => p._id) : [this.state.yKey]

		let clickHandlers = { 
			handleShowOptionsButtonClick:this.handleShowOptionsButtonClick,
			handleOptionButtonClick:this.handleOptionButtonClick,
			handleSuboptionButtonClick:this.handleSuboptionButtonClick
		}
		let buttonsConfig = {
			optionsAreShown:this.state.optionsAreShown, 
			selectedButtonOption:this.state.selectedButtonOption,
			moreButtonSelected:this.state.moreButtonSelected,	
			activeSuboptions:activeSuboptions, 
			currentPopup:this.state.currentPopup,
			fillScale:this.state.fillScale
		}
		return(
			<div className='svg-wrapper'>
				<div className={'svg-cont '+(this.state.optionsAreShown ? 'small' : 'large')}
						ref={svgContainer => this.svgContainer = svgContainer} >
					<svg className='progress-svg' width='100%' height='100%' 
						style={{opacity:0, transition:'opacity 200ms ease-in 0ms'}}>
					</svg>
				</div>
				<ChartButtons buttons={requiredButtons} config={buttonsConfig} clickHandlers={clickHandlers}/>
				<ProgressAnimation readyToStart={this.state.initValuesSet} 
					updateState={this.updateState} onChange={this.onAnimationChange} currentPopup={this.state.currentPopup}
					yKey={this.state.yKey} players={this.state.players} />
			</div>
		)
	}
}

ProgressSVG.defaultProps = {
}

export default ProgressSVG


