import React, {Component} from 'react'
import PropTypes from 'prop-types'
import * as d3 from 'd3'

let demo = [{nr:1, delay:1000, name:''}, {nr:2, delay:1000, name:''}, {nr:3, delay:1000, name:''},
 {nr:4, delay:1000, name:''}, {nr:5, delay:1000, name:''}, {nr:6, delay:1000, name:''},
 {nr:7, delay:1000, name:''}, {nr:8, delay:1000, name:''}, {nr:9, delay:1000, name:''},
 {nr:10, delay:1000, name:''}, {nr:11, delay:1000, name:''}, {nr:12, delay:1000, name:''},
 {nr:13, delay:1000, name:''}, {nr:14, delay:1000, name:''}, {nr:15, delay:1000, name:''},
 {nr:16, delay:1000, name:''}, {nr:17, delay:1000, name:''}, {nr:18, delay:1000, name:''},
 {nr:19, delay:1000, name:''}]
 demo.end = 21
/**
* A series of popup messages, and functions provided as props to communicate to the
* parent component every time the animation progresses so that it can make changes if necc.
**/
//TODO - make this a HOC and pass in the changes
class ProgressAnimation extends Component{
	constructor(props){
		super(props)
		this.onStart = this.onStart.bind(this)
		this.onEnd = this.onEnd.bind(this)
		this.onStartStopClick = this.onStartStopClick.bind(this)
		this.onPauseResumeClick = this.onPauseResumeClick.bind(this)
		this.onForwardClick = this.onForwardClick.bind(this)
		this.onBackClick = this.onBackClick.bind(this)
		this.state = {
			hasRun:false,
			running:false,
			paused:false,
			count:1
		}
	}
	static propTypes ={
		updateState:PropTypes.func,
		onChange:PropTypes.func,
		currentPopup:PropTypes.string,
		readyToStart:PropTypes.bool
	}
	componentDidMount(){
		//show start button 
		setTimeout(() => {this.props.updateState({currentPopup:'demo-ctrls'})}, 2000);
	}
	componentDidUpdate(prevProps, prevState){
		//check initValueReset, unless animation already started
		if(this.props.readyToStart || this.state.count > 1){
			//execute, but only if current count has not been executed already
			if(this.state.running && !this.state.hasRun){
				this.setState({hasRun:true}, this.runAnimation)
			}
		}
	}
	/*
	Changes to popup are propogated up to parent, as it may need to know taht to 
	eg so it can highlight the relevant part of the chart, or to show the popup if the popup 
	is attached to a button instead of being rendered here*/
	runAnimation(){
		switch(this.state.count){
			case(1):{
				//welcome message
				this.props.updateState({currentPopup:'welcome'})
				break
			}
			case(2):{
				//point to actual score circles
				this.props.updateState({currentPopup:'real-values'})
				break
			}
			case(3):{
				//point to projected score circles
				this.props.updateState({currentPopup:'proj-values'})
				break
			}
			case(4):{
				//point to zones
				this.props.updateState({currentPopup:'zones'})
				break
			}
			case(5):{
				//point to show-options button
				this.props.updateState({optionsAreShown:false, currentPopup:'show-options'})
				break
			}
			case(6):{
				//simulate show-Options-Button click
				//This is the gateway to changing values -> must update that initValues have changed
				this.props.updateState({optionsAreShown:true, initValuesChanged:true})
				break
			}
			case(7):{
				//popup to point to players option button
				this.props.updateState({currentPopup:'players-button-popup'})
				break
			}
			case(8):{
				//players selected are Denilson and Jerome - these must be in database for demo
				let nextSelectedPlayers = this.props.players.filter(p => 
					p.firstName == 'Denilson' || p.firstName == 'Jerome')
				this.props.updateState({selectedPlayers:nextSelectedPlayers})
				break
			}
			case(9):{
				//players selected are Denilson, Jerome and Caleb
				let nextSelectedPlayers = this.props.players.filter(p =>
					p.firstName == 'Denilson' || p.firstName == 'Jerome' || p.firstName == 'Caleb')
				this.props.updateState({selectedPlayers:nextSelectedPlayers})
				break
			}
			case(10):{
				//popup points to fitness
				this.props.updateState({currentPopup:'fitness-button-popup'})
				break
			}
			case(11):{
				//selectedButtonOption and selectedOption is fitness, yKey is tTest
				this.props.updateState({selectedButtonOption:'fitness', yKey:'tTest', selectedOption:'fitness'})
				break
			}
			case(12):{
				//selectedButtonOption and selectedOption is fitness, yKey is yoyo
			 	this.props.updateState({selectedButtonOption:'fitness', yKey:'yoyo', selectedOption:'fitness'})
				break
			}
			case(13):{
				//selectedButtonOption and selectedOption is fitness, yKey is shuttles
			 	this.props.updateState({selectedButtonOption:'fitness', yKey:'shuttles', selectedOption:'fitness'})
				break
			}
			case(14):{
				//popup pointing to skills
				this.props.updateState({currentPopup:'skills-button-popup'})
				break
			}
			case(15):{
				//selectedButtonOption is skills, but selectedOption is fitness, yKey is still shuttles
				this.props.updateState({selectedButtonOption:'skills', selectedOption:'fitness', yKey:'shuttles'})
				break
			}
			case(16):{
				//selectedButtonOption and selectedOption is skills, yKey is touch
				this.props.updateState({selectedButtonOption:'skills', selectedOption:'skills', yKey:'touch'})
				break
			}
			case(17):{
				//selectedButtonOption and selectedOption is skills, yKey is switchplay
				this.props.updateState({selectedButtonOption:'skills', selectedOption:'skills', yKey:'switchplay'})
				break
			}
			case(18):{
				//selectedButtonOption and selectedOption is skills, yKey is dribble
				this.props.updateState({optionsAreShown:true, 
					selectedButtonOption:'skills', selectedOption:'skills', yKey:'dribble'})
				break
			}
			case(19):{
				//stop pointing to skills, hide all buttons, and point to Compare link
				this.props.updateState({optionsAreShown:false, currentPopup:'demo-ctrls'})
				break
			}
			case(20):{
				//stop pointing to skills, hide all buttons, and point to Compare link
				this.props.updateState({currentPopup:'compare'})
				//timeout in case user is stuck on compare as button is hidden
				setTimeout(() =>{
					if(this.props.currentPopup =='compare'){
						this.onEnd()
					}
				}, 4000)
				break
			}
		}
		setTimeout(() =>{
			if(!this.state.paused){
				if(this.state.count < demo.end)
					this.setState(state => ({count:state.count+1, hasRun:false}))
				else
					this.onEnd()
			}
		},1000)
	}
	onStart(){
		//tell parent so it can reset state
		this.props.onChange('start')
		//begin animation
		setTimeout(() =>{
			this.setState({running:true, paused:false, count:1})
		},1000)
	}
	onEnd(){
		//end demo with a time-limited compare link-pointer popup
		this.setState({running:false, hasRun:false, count:1},
			() => {
				setTimeout(() =>{
					this.setState({currentPopup:'demo-ctrls'})
				},3000)
			})
		this.props.onChange('end')
	}
	onStartStopClick(){
		if(this.state.running)
			this.onEnd()
		else
			this.onStart()
	}

	onPauseResumeClick(){
		if(!this.state.paused)
			this.setState(state => ({paused:true}))
		else
			this.setState(state => ({paused:false, count:state.count+1, hasRun:false}))

	}
	//only called if animation not finished
	onForwardClick(){
		if(this.state.count == demo.end){
			this.onEnd()
		}else{
			if(!this.state.demoPaused)
				this.setState(state => ({paused:true, count:state.count+1, hasRun:false}))
			else
				this.setState(state => ({count:state.count+1, hasRun:false}))
		}
	}
	//only called if count != 1
	onBackClick(){
		if(this.state.count != 1){
			if(!this.state.demoPaused)
				this.setState(state => ({paused:true, count:state.count-1, hasRun:false}))
			else
				this.setState(state => ({count:state.count-1, hasRun:false}))
		}
	}
	render(){
		return(
			<div className='progress-animation'>
				<div className={'welcome-popup large-popup popup '+(this.props.currentPopup == 'welcome' ? 'show':'hide')}>
					View the Demonstration and then explore!</div>

				<div className={'demo-ctrls corner '+(this.props.currentPopup == '' || this.props.currentPopup == 'compare' ? 'hide ':'show ')}>
						<button className={'start-stop-button button'}
							onClick={this.onStartStopClick}>
							{this.state.running ? 'End' : 'Demo'}
						</button>
						<button onClick={this.onPauseResumeClick} 
							 className={'pause-btn button '+(this.state.running ? 'displayed':'not-displayed')}>
							{this.state.paused ? 'Resume' : 'Pause'}</button>
						<div className={'navigation '+(this.state.running ? 'displayed':'not-displayed')}>
							<button onClick={this.onBackClick} 
								className={'button ' +(this.state.running ? '':'not-displayed')}>Back</button>
							<button onClick={this.onForwardClick} 
								className={'button ' +(this.state.running ? '':'not-displayed')}>Next</button>
						</div>
					</div>
				<div className={'real-values-popup screen-attached-popup popup '+(this.props.currentPopup == 'real-values' ? 'show':'hide')}>
					Filled circles are actual scores from tests taken</div>
				<div className={'proj-values-popup screen-attached-popup popup '+(this.props.currentPopup == 'proj-values' ? 'show':'hide')}>
					Unfilled circles are projected future scores</div>
				<div className={'zones-popup screen-attached-popup large-popup popup '+(this.props.currentPopup == 'zones' ? 'show':'hide')}>
					Blue zones show how players compare to the expected standards at different football levels</div>
				<div className={'compare-popup screen-attached-popup popup '+(this.props.currentPopup == 'compare' ? 'show':'hide')}>
					Now select 'Compare' in the menu to compare player's scores</div>
			</div>
		)
	}
}

export default ProgressAnimation

