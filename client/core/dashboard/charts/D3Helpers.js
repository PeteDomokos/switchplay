
const D3Helpers = {
	/**
	* Desc ...
	* @constructor
	* @param {string} 
	* @param {Object}
	* @param {Object[]}  
	* @param {number} 
	**/
	configureChartData(repMatch, item){
		let { actorNames, statNames, dispType } = item
		switch(dispType){
			case "line":
				return this.configureLineGraphData(repMatch, actorNames)
			default:
				return []
		}
	},
	//note - actorNames will normally be players or teams, but could be actions -they are basically the entities that will have a line each
	/**
	*   
	**/
	configureLineGraphData(repMatch, actorNames){
		let reqActorsDataArr = actorNames.map((name,i) =>{
			let actorsTeam = repMatch.details.teams.find(team => team.playerUNames.includes(name))
			let actorsTeamId = actorsTeam.id
			//todo - change team names to actual names in matchMoves
			let teamsMoves = repMatch.data.teamsMatchMoves.find(matchMoves => matchMoves.teamId == actorsTeamId).moves
			//note: may not need moves here so take it out, but it will be useful somewhere else
			//let actorsMoves = teamsMoves.filter(move => move.actions.find(a => a.player == name).length != 0)
			let teamsActions = this.movesToActions(teamsMoves)
			let actorsActions = teamsActions.filter(action => action.player == name)
		})
		return reqActorsDataArr
	},
	/**
	* data - an array of objects, each of which must have a time property of type Int
	*/
	/**
	* Desc ...
	* @constructor
	* @param {string} 
	* @param {Object}
	* @param {Object[]}  
	* @param {number} 
	**/
	phaseData(data, nrPhases){
		//phaseDuration
		let phaseDuration = Math.max(...(data.map(item => item.time)))/nrPhases
		//todo - impl phaseNrs properly so its flexible
		let phaseNrs = [1,2,3,4,5,6]
		//todo - use a groupBy func instead of iterating through the data mulitple times
		let phasesMoves = phaseNrs.map(phaseNr => {
			let phaseMoves = data.filter(move => (move.time >= (phaseNr-1)*phaseDuration && move.time < phaseNr*phaseDuration))
			return {nr:phaseNr, moves:phaseMoves}
		})
		return phasesMoves
	},
	/**
	* Desc ...
	* @constructor
	* @param {string} 
	* @param {Object}
	* @param {Object[]}  
	* @param {number} 
	**/
	getPhase(min, phaseDuration){
		return Math.floor(min / phaseDuration)+1
	},
	/**
	* Desc ...
	* @constructor
	* @param {string} 
	* @param {Object}
	* @param {Object[]}  
	* @param {number} 
	**/
	movesToActions(movesArray){
		return movesArray.map(move => move.actions).reduce((acc, val) => acc.concat(val), [])
	},
	/*
	*/
	 //WARNING - NOT GOOD FOR DECIMAL CASES IF REQUIRED EG SPRINTS
	/**
	* Desc ...
	* @constructor
	* @param {string} 
	* @param {Object}
	* @param {Object[]}  
	* @param {number} 
	**/
	getMaxValuePlusFromRange(maxValue, minValue){
		if(maxValue - minValue < 2) return this.nextMultiple(maxValue, 0.2)
		if(maxValue - minValue < 10) return Math.ceil(maxValue)
		if(maxValue - minValue < 24) return this.nextMultiple(maxValue, 2)
		if(maxValue - minValue < 50) return this.nextMultiple(maxValue, 5)
		if(maxValue - minValue < 130) return this.nextMultiple(maxValue, 10)
		if(maxValue - minValue < 240) return this.nextMultiple(maxValue, 20)
		if(maxValue - minValue < 600) return this.nextMultiple(maxValue, 50)
		if(maxValue - minValue< 1500) return this.nextMultiple(maxValue, 100)
		if(maxValue - minValue< 2000) return this.nextMultiple(maxValue, 200)
		if(maxValue - minValue< 5000) return this.nextMultiple(maxValue, 500)
		if(maxValue - minValue< 20000) return this.nextMultiple(maxValue, 1000)
		if(maxValue - minValue< 100000) return this.nextMultiple(maxValue, 5000)
		if(maxValue - minValue< 1000000) return this.nextMultiple(maxValue, 50000)
		return maxValue*1.1
	},
	/**
	* Desc ...
	* @constructor
	* @param {string} 
	* @param {Object}
	* @param {Object[]}  
	* @param {number} 
	**/
	getMaxValuePlus(maxValue){
		if(maxValue <= 12) return Math.ceil(maxValue) //may not be good for binning eg 11
		if(maxValue < 24) return this.nextMultiple(maxValue, 2)
		if(maxValue < 50) return this.nextMultiple(maxValue, 5)
		if(maxValue < 130) return this.nextMultiple(maxValue, 10)
		if(maxValue < 240) return this.nextMultiple(maxValue, 20)
		if(maxValue < 600) return this.nextMultiple(maxValue, 50)
		if(maxValue< 1500) return this.nextMultiple(maxValue, 100)
		if(maxValue< 2000) return this.nextMultiple(maxValue, 200)
		if(maxValue< 5000) return this.nextMultiple(maxValue, 500)
		if(maxValue< 20000) return this.nextMultiple(maxValue, 1000)
		if(maxValue< 100000) return this.nextMultiple(maxValue, 5000)
		if(maxValue< 1000000) return this.nextMultiple(maxValue, 50000)		
		return maxValue*1.1
	},
	/**
	*	params - nrGroups(optional)  - this can be nr of ticks or nr of bins
	**/
	 //WARNING - NOT GOOD FOR DECIMAL CASES IF REQUIRED EG SPRINTS
	/**
	* Desc ...
	* @constructor
	* @param {string} 
	* @param {Object}
	* @param {Object[]}  
	* @param {number} 
	**/
	getMaxValuePlusFromGroups(maxValue, nrGroups){
		//if(maxValue < 12) return Math.ceil(maxValue) //may not be good for binning eg 11
		if(Math.ceil(maxValue) <= nrGroups) return maxValue
			//todo - impl mfully where nrGroups determines the result
		if(maxValue <= 12) return Math.ceil(maxValue)
		if(maxValue < 24) return this.nextMultiple(maxValue, 2)
		if(maxValue < 50) return this.nextMultiple(maxValue, 5)
		if(maxValue < 130) return this.nextMultiple(maxValue, 10)
		if(maxValue < 240) return this.nextMultiple(maxValue, 20)
		if(maxValue < 600) return this.nextMultiple(maxValue, 50)
		if(maxValue - minValue< 1500) return this.nextMultiple(maxValue, 100)
		if(maxValue - minValue< 2000) return this.nextMultiple(maxValue, 200)
		if(maxValue - minValue< 5000) return this.nextMultiple(maxValue, 500)
		if(maxValue - minValue< 20000) return this.nextMultiple(maxValue, 1000)
		if(maxValue - minValue< 100000) return this.nextMultiple(maxValue, 5000)
		if(maxValue - minValue< 1000000) return this.nextMultiple(maxValue, 50000)
		return maxValue*1.1
	},
	/**
	* Desc ...
	* @constructor
	* @param {string} 
	* @param {Object}
	* @param {Object[]}  
	* @param {number} 
	**/
	getMinValueMinusFromRange(maxValue, minValue){
		if(maxValue - minValue < 2) return this.prevMultiple(minValue, 0.2)
		if(maxValue - minValue < 10) return Math.floor(minValue)
		if(maxValue - minValue < 5) return 0 
		if(maxValue - minValue < 24) return this.prevMultiple(minValue, 2)
		if(maxValue - minValue < 50) return this.prevMultiple(minValue, 5)
		if(maxValue - minValue < 130) return this.prevMultiple(minValue, 10)
		if(maxValue - minValue < 240) return this.prevMultiple(minValue, 20)
		if(maxValue - minValue < 600) return this.prevMultiple(minValue, 50)
		if(maxValue - minValue< 1500) return this.prevMultiple(minValue, 100)
		if(maxValue - minValue< 2000) return this.prevMultiple(minValue, 200)
		if(maxValue - minValue< 5000) return this.prevMultiple(minValue, 500)
		if(maxValue - minValue< 20000) return this.prevMultiple(minValue, 1000)
		if(maxValue - minValue< 100000) return this.prevMultiple(minValue, 5000)
		if(maxValue - minValue< 1000000) return this.prevMultiple(minValue, 50000)
		return maxValue*1.1
	},
	//WARNING - NOT GOOD FOR DECIMAL CASES IF REQUIRED EG SPRINTS
	/**
	* Desc ...
	* @constructor
	* @param {string} 
	* @param {Object}
	* @param {Object[]}  
	* @param {number} 
	**/
	getMinValueMinus(minValue){
		//if(maxValue < 12) return Math.ceil(maxValue) //may not be good for binning eg 11
		if(minValue < 5) return 0 
		if(minValue < 24) return this.prevMultiple(minValue, 2)
		if(minValue < 50) return this.prevMultiple(minValue, 5)
		if(minValue < 130) return this.prevMultiple(minValue, 10)
		if(minValue < 240) return this.prevMultiple(minValue, 20)
		if(minValue < 600) return this.prevMultiple(minValue, 50)
		if(maxValue - minValue< 1500) return this.prevMultiple(minValue, 100)
		if(maxValue - minValue< 2000) return this.prevMultiple(minValue, 200)
		if(maxValue - minValue< 5000) return this.prevMultiple(minValue, 500)
		if(maxValue - minValue< 20000) return this.prevMultiple(minValue, 1000)
		if(maxValue - minValue< 100000) return this.prevMultiple(minValue, 5000)
		if(maxValue - minValue< 1000000) return this.prevMultiple(minValue, 50000)
		return minValue*1.1
	},
	/**
	* Desc ...
	* @constructor
	* @param {string} 
	* @param {Object}
	* @param {Object[]}  
	* @param {number} 
	**/
	nextMultiple(origNumber, timesTable){
		//round up first if necc - doesnt ork fro decimal times tables coz it rounds up
		//todo - make it round up to teh appropriate dp so it works for eg 0.2 times table
		//let origInt = Math.ceil(origNumber)
		let remainder = origNumber % timesTable
		let ans = origNumber + timesTable - remainder
		return ans
	},
	/**
	* Desc ...
	* @constructor
	* @param {string} 
	* @param {Object}
	* @param {Object[]}  
	* @param {number} 
	**/
	prevMultiple(origNumber, timesTable){
		//round down first if necc
		//let origInt = Math.floor(origNumber)
		let remainder = origNumber % timesTable
		let ans = origNumber - remainder
		return ans
	},
	/**
	* params   - xMax - the maximum possible value, which will be a multiple of either 2,5,10,20,50,100 etc
	*		   - nrBins (optional) - assumed to be 5 by default
	*returns - an array of thresholds that are equal in width and result in the right number of bins
	**/ 

	getThresholds(xMax, nrBinsRequired){
		let nrBins = 5
		if(nrBinsRequired)
			nrBins = nrBinsRequired

		if(xMax <= 5)
			return [1,2,3,4,5]
		return []
	},
	/**
	*   return - a flexible array of values, which I can change in this method to match needs
	*   todo - shouldnt really need this should be able to get all options from the array
	**/
	getSecKeyValues(secKey){
		switch(secKey){
			case "wasSucc": return ["true", "false"]
			default: return []
		}
	},
	getSecKeyValueWords(secKey){
		switch(secKey){
			case "wasSucc": return ["successful", "unsuccesful"]
			default: return []
		}
	},
	convertSecKeyValueToWord(secKey, secKeyValue){
		switch(secKey){
			case "wasSucc": {
				switch(secKeyValue){
					case "true": return "successful"
					default: "unsuccesful"
				}
			}
			default: return secKeyValue
		}
	}
}

export default D3Helpers
