import regression from 'regression'
import * as d3 from 'd3'

const hasChanged = (property, prevState, state) =>{
}
/**
*   params - test - a test object, with name, catgerory and score propertis such as time
**/
const getScore = (testName, testData) => {
	//add penalties property if it doesnt exist
	let data = testData.penalties ? testData :{...testData, penalties:0}
	switch(testName){
		case 'tTest':return data.time +(0.5*data.penalties)
		case 'yoyo':return data.distance
		case 'shuttles':return data.time +(0.5*data.penalties)
		case 'touch':return data.points
		case 'switchplay':return data.points -data.penalties
		case 'dribble':return data.time +data.penalties
		case 'wins':return data.score // 1 = win, -1 = loss, 0 = draw
		case 'pointsDiff':return data.pointsFor - data.pointsAgainst
		case 'keepUps':return data.score
		default:return data.score
	}
}
const createScoreDataObject = (testName) => {
	switch(testName){
		case 'tTest':return {time:'', penalties:0}
		//todo - change yoyo to level, and convert automatically to distance !!!!!!!!!!!!
		case 'yoyo':return {distance:''}
		case 'shuttles':return {time:'', penalties:0}
		case 'touch':return {score:''}
		//todo - change these for switchplay (see model) !!!!!!!!!!!!!!!!!!
		case 'switchplay':return {reps:'', basicReps:'', goodReps:'', perfectReps:''}
		case 'dribble':return {time:'', penalties:0}
		case 'wins':return {score:''}
		case 'pointsDiff':return {points:''}
		case 'keepUps':return {score:''}
		default:return {score:''}
	}
}

const getLabel = (testName) => {
		switch(testName){
			case 'tTest':return 'T-Test'
			case 'yoyo':return 'Yoyo'
			case 'shuttles':return 'Shuttles'
			case 'touch':return 'Touch'
			case 'switchplay':return 'Switchplay'
			case 'dribble':return 'Dribble'
			case 'wins':return 'Wins'
			case 'pointsDiff':return 'Points'
			case 'skills':return 'Skills'
			case 'fitness':return 'Fitness'
			case 'keepUps':return 'Kick-Ups'
			default:return 'Test'
		}
}



const getMeasure = (testName) => {
		switch(testName){
			case 'tTest':return 'best-score'
			case 'yoyo':return 'best-score'
			case 'shuttles':return 'best-score'
			case 'touch':return 'best-score'
			case 'switchplay':return 'best-score'
			case 'dribble':return 'best-score'
			case 'wins':return 'total'
			case 'pointsDiff':return 'total'
			case 'keepUps':return 'best-score'

			default:return 'best-score'
		}
}
//todo - generalise this
const nrOfDataEntries = (player) =>{
	let tEntries = player['fitness']['tTest'] ? player['fitness']['tTest'].length : 0
	let yoyoEntries = player['fitness']['yoyo'] ? player['fitness']['yoyo'].length : 0
	let shuttlesEntries = player['fitness']['shuttles'] ? player['fitness']['shuttles'].length : 0
	let touchEntries = player['skills']['touch'] ? player['skills']['touch'].length : 0
	let dribbleEntries = player['skills']['dribble'] ? player['skills']['dribble'].length : 0
	let switchplayEntries = player['skills']['switchplay'] ? player['skills']['switchplay'].length : 0

	return tEntries + yoyoEntries + shuttlesEntries + touchEntries + dribbleEntries + switchplayEntries
}
//todo - generalise this
const filterForPlayersThatHaveData =(players) =>{
	return players.filter(p => 
		(p['fitness']['t-Test'] && p['fitness']['tTest'].length != 0) ||
		(p['fitness']['yoyo'] && p['fitness']['yoyo'].length != 0 ) ||
		(p['fitness']['shuttles'] && p['fitness']['shuttles'].length != 0 ) ||
		(p['skills']['touch'] && p['skills']['touch'].length != 0 ) ||
		(p['skills']['dribble'] && p['skills']['dribble'].length != 0 ) ||
		(p['skills']['switchplay'] && p['skills']['switchplay'].length != 0 )
	)
}

export { getScore, getLabel, createScoreDataObject, getMeasure, nrOfDataEntries, filterForPlayersThatHaveData }