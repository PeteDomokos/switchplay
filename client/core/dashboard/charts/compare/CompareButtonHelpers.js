import regression from 'regression'
import * as d3 from 'd3'
/**
*   params - test - a test object, with name, catgerory and score propertis such as time
**/
const renderShowOptionsButton = 
	(showOptionsCtrlSelector, buttonsAreaSelector, optionsAreShown, handleClick) =>{
	d3.select(showOptionsCtrlSelector).
		append("div").
		attr("class", "show-options-button").
		style("position", "relative")

	d3.select(showOptionsCtrlSelector).select("div.show-options-button").
		append("button").
		attr("class", " btn").
		on("click", (button) =>{
			handleClick(button)
		})
	/*d3.select(showOptionsCtrlSelector).select("div.show-options-button").
		append("div").
		attr("class", "show-options-popup button-popup popup").
		text("Click to edit the options")*/


		//init update of buttons area and show options button text
		updateButtonsArea(buttonsAreaSelector, optionsAreShown)
		updateShowOptionsButtonText(showOptionsCtrlSelector, optionsAreShown)
}
const renderOptionsButtons = 
	(optionsCtrlsSelector, optionsButtons, optionsAreShown, selectedButtonOption, handleOptionsButtonClick) =>{
		let mesg = (button) =>{
			switch(button){
				case "players":{
					return "Select the players you want to see"
				}
				case "fitness":{
					return "Choose a fitness test and view the scores each week"
				}
				case "skills":{
					return "Or choose a skills test instead"
				}
			}
		}
		//console.log("optionsButtons", optionsButtons)
		d3.select(optionsCtrlsSelector).selectAll("*").remove()
		d3.select(optionsCtrlsSelector).selectAll("div.options-button").
			data(optionsButtons).enter().
			append("div").
			attr("class", d => d+"-options-button options-button").
			style("position", "relative")

		d3.select(optionsCtrlsSelector).selectAll("div.options-button").
			append("button").
			html(d => d).
		    attr("class", "btn").
			on("click", (button) => {
				handleOptionsButtonClick(button)
			})

		/*d3.select(optionsCtrlsSelector).selectAll("div.options-button").
			append("div").
			//todo - use an external func to get pop up number, or label the popup with the button name
			attr("class", d => d+"-button-popup button-popup popup").
			text(d => mesg(d))*/

		//init styling
		updateOptionsButtonActive(optionsCtrlsSelector, selectedButtonOption)
		updateOptionsButtonsVis(optionsCtrlsSelector, optionsAreShown)
}
//note: currentYKeys may not be an array if only 1 yKey
const renderButtons = (ctrlsSelector, buttons, playersToShow, optionsAreShown, selectedButtonOption, selectedOption, moreButtonsSelected, activeYKeys, fillScale, handleButtonClick) =>{
	d3.select(ctrlsSelector).selectAll("*").remove()
	let reqButtons = buttons[selectedButtonOption]
	if(reqButtons){
		//up to 10 buttons - group into first5 and rest
		let buttonsToDisplay
		if(reqButtons.length <= 8)
			buttonsToDisplay = reqButtons
		else{
			//MAX 14 BUTTONS FOR NOW - NEED TO INCREASE
			if(!moreButtonsSelected){
				let first7Buttons = reqButtons.slice(0,7)
				let nextButton = {key:'more', butLabel:'more...'}
				buttonsToDisplay = [...first7Buttons, nextButton]
			}else{
				let lastButtons = reqButtons.slice(7,14)
				let prevButton = {key:'back', butLabel:'back...'}
				buttonsToDisplay = [...lastButtons, prevButton]
			}
		}
		//append the buttons
		d3.select(ctrlsSelector).selectAll("button.suboption-button").
		   data(buttonsToDisplay).enter().
		   append("button").
		   html(d => d.butLabel).
		   attr("class", "suboption-button btn").
		   style("background-color", 'transparent').
		   on("click", (button) => {
		   	handleButtonClick(button)
		   })
	}
	//init styling for suboption buttons - active and visibility
	updateButtonsActive(ctrlsSelector, activeYKeys, playersToShow, fillScale)
	updateSuboptionsButtonsVis(ctrlsSelector, optionsAreShown)
}


const updateButtonsArea = (buttonsAreaSelector, optionsAreShown) => {
		if(optionsAreShown)
			d3.select(buttonsAreaSelector)
				.classed("full-height", true)
				.classed("full-width", true)
				.classed("short-height", false)
				.classed("short-width", false)
				//.style("flex", "25% 1 1")
				//.style("max-height", "200px")
		else
			d3.select(buttonsAreaSelector)
				.classed("short-height", true)
				.classed("short-width", true)
				.classed("full-height", false)
				.classed("full-width", false)
				//.style("flex", "10% 1 1")
				//.style("max-height", "70px")
}
const updateShowOptionsButtonText = (showOptionsCtrlSelector, optionsAreShown) => {
	d3.select(showOptionsCtrlSelector).select("button").
		html(optionsAreShown ? "Hide":"Options")
}
const updateOptionsButtonActive = (optionsCtrlsSelector, selectedButtonOption) =>{
	d3.select(optionsCtrlsSelector).selectAll("button").
		style("background-color", button => (selectedButtonOption == button ? '#b52e24':'transparent'))
}
const updateOptionsButtonsVis = (optionsCtrlsSelector, optionsAreShown) => {
	if(optionsAreShown){
		d3.select(optionsCtrlsSelector).selectAll("button").
			style("opacity", 1)
	}
	else{
		d3.select(optionsCtrlsSelector).selectAll("button").
			style("opacity", 0)
	}

	//d3.select(optionsCtrlsSelector).selectAll("button").
		//style("opacity", (optionsAreShown ? 1:1))
}
const updateSuboptionsButtonsVis = (ctrlsSelector, optionsAreShown) => {
	if(optionsAreShown)
		d3.select(ctrlsSelector).selectAll("button").
			style("opacity", 1)
	else
		d3.select(ctrlsSelector).selectAll("button").
			style("opacity", 0)

	//d3.select(ctrlsSelector).selectAll("button").
		//style("opacity", (optionsAreShown ? 1:1))
}
const updateButtonsActive = (ctrlsSelector, keysActive, playersToShow, fillScale) =>{
	//yKeys may not be an array
	let _keysActive = keysActive.length ? keysActive : [keysActive]
	let colour = (key) => {
		if(_keysActive.includes(key))
			return '#ffa530'
		if(playersToShow.map(p => p._id).includes(key))
			return fillScale(key)
		return 'transparent'
	}
	d3.select(ctrlsSelector).selectAll("button.suboption-button").
		style("background-color", button => colour(button.key))
}

export { renderShowOptionsButton, renderOptionsButtons, renderButtons, updateButtonsArea, updateShowOptionsButtonText, 
	updateOptionsButtonActive, updateOptionsButtonsVis, updateSuboptionsButtonsVis,
	updateButtonsActive }