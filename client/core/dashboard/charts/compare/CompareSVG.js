import React, {Component} from 'react'
import PropTypes from 'prop-types'
import * as d3 from 'd3'
import { legendColor } from 'd3-svg-legend'
import { clone } from 'lodash'
//general
import { updatedSelectedPlayers } from '../GeneralDataHelpers'
import { getScore, nrOfDataEntries, filterForPlayersThatHaveData  } from '../TestDataHelpers'
import ChartButtons from '../ChartButtons'
//specific to Compare
import { renderChart } from './RenderCompareChart'
import { chartSizes } from './CompareHelpers'
/**
* A set of Bee Swarm Plots which presents a group of players best scores in sports tests.
* Buttons enable the user to select different players and tests.
**/
class CompareSVG extends Component{
	constructor(props){
		super(props)
		this.initChartValues = this.initChartValues.bind(this)
		this.handleWindowSizeChange = this.handleWindowSizeChange.bind(this)
		this.handleShowOptionsButtonClick = this.handleShowOptionsButtonClick.bind(this)
		this.handleOptionButtonClick = this.handleOptionButtonClick.bind(this)
		this.handleSuboptionButtonClick = this.handleSuboptionButtonClick.bind(this)
		this.handlePlayerClick = this.handlePlayerClick.bind(this)
		this.handleAddPlayer = this.handleAddPlayer.bind(this)
		this.handleRemovePlayer = this.handleRemovePlayer.bind(this)
		this.handleYKeyClick = this.handleYKeyClick.bind(this)
		this.handleAddYKey = this.handleAddYKey.bind(this)
		this.handleRemoveYKey = this.handleRemoveYKey.bind(this)
		this.state = {
			sizes:{},
			optionsAreShown:false,
			selectedButtonOption:'',
			selectedOption:'',
			moreButtonSelected:false,
			yKeys:[],
			players:[],
			selectedPlayers:[],  
			buttonsDisabled:false,
			fillScale:'',
			roleScale:'',
		}
	}
	static propTypes ={
	    item: PropTypes.object,
	 }
	static defaultProps = {
  	}
	/**
    * Calls function that initialises state values based on props, and sets a timeout for window size changes
    **/
	componentDidMount(){
		this.initChartValues()

		//WINDOW CHANGES - sizes will change
		window.addEventListener('resize', () =>{
				this.forceUpdate()
		})
	}
	/**
	* Calls handler if svg size has changed, which updates state. 
	* Otherwise calls renderChart, passing in the chart settings from state
	**/
	componentDidUpdate(prevProps, prevState){
			if(window.innerWidth != prevState.sizes.windowWidth){
				this.handleWindowSizeChange(window.innerWidth, prevState.sizes.windowWidth)
			}else{
				//new sizes
				let svgWidth = this.svgContainer ? this.svgContainer.getBoundingClientRect().width : 500
				let svgHeight = this.svgContainer ? this.svgContainer.getBoundingClientRect().height : 500
				let sizes = chartSizes(svgWidth, svgHeight, window.innerWidth, window.innerHeight)

				let chartSettings = {
					selectedPlayers:this.state.selectedPlayers,
					selectedOption:this.state.selectedOption,
					yKeys:this.state.yKeys,
					fillScale:this.state.fillScale,
					roleScale:this.state.roleScale
				}
				renderChart("svg.compare-svg", sizes, this.props.item, chartSettings)
				//ease in svg
				setTimeout(() =>{
					d3.select('svg.compare-svg').style('opacity',1)
				},1)
			}
	}
	/**
	* Updates yKeys in state if screen size has passed the threshold 
	* for displaying either 1 or 3 swarm plots ( if less than 980, only display 1 swarm plot)
	**/
	handleWindowSizeChange(currentWindowWidth, prevWindowWidth){
		//remove or add extra yKeys if screen sizes crossed threshold
		//note - based on window width so that showing buttons doesnt make it cross threshold
		let updatedYKeys
		if(prevWindowWidth >= 980 && currentWindowWidth < 980){
			//gone from large to small screen
			updatedYKeys = this.state.yKeys.slice(0,1)
		}
		else if(prevWindowWidth < 980 && currentWindowWidth >= 980){
			//gone from small to large screen
			let currentYKey = this.state.yKeys[0]
			let otherYKeys = this.props.item.buttons[this.state.selectedOption]
				.map(button => button.key)
				.filter(key => key != currentYKey)
			//add two more yKeys to the current selected yKey
			updatedYKeys = [...this.state.yKeys, ...otherYKeys.slice(0,2)]
		}else
			updatedYKeys = this.state.yKeys

		this.setState({yKeys:updatedYKeys})
	}
	/**
	*  Sets or resets state to initial values based on props 
	*  Except, does not reset buttonsDisabled, as this is handled in onAnimationChange()
	**/
	initChartValues(){
		let { players, buttons, initSelectedOption } = this.props.item

		let initChartValues = {}
		//sizes for overall chart
		let windowWidth = window.innerWidth
		let windowHeight = window.innerHeight
		let width = this.svgContainer ? this.svgContainer.getBoundingClientRect().width : 500
		let height = this.svgContainer ? this.svgContainer.getBoundingClientRect().height : 500
		let sizes = chartSizes(width, height, windowWidth, windowHeight)
		initChartValues.sizes = sizes
		//selected options
		initChartValues.selectedButtonOption = 'players'
		initChartValues.selectedOption = initSelectedOption
		//if screen wide enough, display 3 charts to begin with
		let allYKeys = buttons[initSelectedOption].map(button => button.key)
		//use window sizes so that buttons displayed dont affect which charts shown
		let yKeys = sizes.windowWidth < 980 ? allYKeys.slice(0,1) : allYKeys.slice(0,3) 
		initChartValues.yKeys = yKeys
		//remove players with no fitness or skills data
		let playersThatHaveData = filterForPlayersThatHaveData(players)
		//TODO - CHECK SORTER WORKS FULLY 
		let playersSortedByRelevance = playersThatHaveData.sort((p1,p2) => {
			return nrOfDataEntries(p1) - nrOfDataEntries(p2)
		})
		initChartValues.players = playersThatHaveData
		initChartValues.selectedPlayers = playersThatHaveData
		initChartValues.fillScale = d3.scaleOrdinal().
			domain(playersThatHaveData.map(p => p._id)).
			range(["blue", "red", "yellow", "orange", "navy","green"])
		initChartValues.roleScale = d3.scaleOrdinal().range(["red", "blue", "yellow"])
		//todo - deal wth case of no playersthatHaveData.length == 0 
		//all players are selected by default for the compare app
		let denilson = playersThatHaveData

		initChartValues.optionsAreShown = false
		initChartValues.moreButtonSelected = false
		this.setState(initChartValues)
	}
	/**
    * Switches the flag on and off to reveal the button controls, 
    * and uses opacity to ensure a smooth transition (buttons and newly sized chart
    * in sync with each other). The chart is smaller when buttons are showing.
    **/
	handleShowOptionsButtonClick(){
		if(!this.state.buttonsDisabled){
			//force-hide all buttons and svg ready for a size change
			d3.select("svg.compare-svg").
				style("transition", 'opacity 200ms ease-in 0ms').
				style("opacity",0)

			d3.select("div.ctrls-container").selectAll('button').
				style("transition", 'opacity 200ms ease-in 0ms').
				style("opacity",0)
			setTimeout(() =>{
				//change sizes and show svg (buttons handled in Buttons component)
				d3.select("svg.compare-svg").style("opacity",1)
				if(!this.state.optionsAreShown){
				}
				else{
				}
				this.setState({optionsAreShown:!this.state.optionsAreShown})
			},400)
		}
	}
 	/**
  	* Updates selectedButtonOption in state to the option button which has been pressed.
  	* This affects which suboption buttons are rendered.
  	**/
	handleOptionButtonClick(button){
		if(!this.state.buttonsDisabled){
			this.setState({
				selectedButtonOption:button,
				moreButtonSelected:false
			})
		}
	}
    /**
   	*  Processes the click of a suboption button. If it is the more/back button, 
   	*  it turns this flag on/off state. If it is a player, it calls handlePlayerClick. 
   	*  If it is a yKey (ie a sports test), then it calls handleYKeyClick.
   	*
   	* @param (string) button - a button object representing either (a) the more/back button, 
   	*                          (b) a player, or (c) a yKey representing a sports test 
   	*                          which is to be measured on the y-axis
    */
	handleSuboptionButtonClick(button){
		let { selectedButtonOption, moreButtonSelected } = this.state
		if(!this.state.buttonsDisabled){
			//if the 'more/back' button clicked, then no need to update chart
	   		if(button.key == 'more' || button.key == 'back')
	   			this.setState({moreButtonSelected:!moreButtonSelected})
			else{
				//button is either a player or a yKey
				if(selectedButtonOption == 'players')
					this.handlePlayerClick(button.key)
				else
					this.handleYKeyClick(button.key)				
			}
		}
	}
	/**
    * Processes the click of a player button. If this player is already selected,
    * it calls handleRemovePlayer. Otherwise, it calls handleAddPlayer
    * 
    **/
	handlePlayerClick(playerId){
		if(this.state.selectedPlayers.find(p => p._id == playerId))
			this.handleRemovePlayer(playerId)
		else
			this.handleAddPlayer(playerId)
	}
	/**
    * Adds this player to selectedPlayers in state. If the max number of players
    * to display is reached, it alerts the user that they must remove another player.
    **/
	//todo - put limit on players to add depending on swarm chart size
	handleAddPlayer(playerId){
		this.setState(state =>{
			return({
				selectedPlayers:[
					...this.state.selectedPlayers, 
					this.state.players.find(p => p._id == playerId)
					]
				})
		})
	}
	/**
    * Removes this player from selectedPlayers in state. If this is the only player
    * selected, it alerts the user that they must first add another player.
    **/
	handleRemovePlayer(playerId){
		if(this.state.selectedPlayers.length == 1)
			alert("You cannot remove all players. Select another player first.")
		else
			this.setState(state =>{
				return({
					selectedPlayers:this.state.selectedPlayers.filter(p => p._id != playerId)
				})
			})
	}
	/**
    * Processes the click of a yKey button. If the yKey is already displayed, it calls handleRemoveYKey.
    * If it is not displayed, it calls handleAddYKey.
    **/
	handleYKeyClick(key){
		if(!this.state.yKeys.includes(key))
			this.handleAddYKey(key)
		else
			this.handleRemoveYKey(key)
	}
	/**
    * If screen is small, it replaces the existing displayed yKey with this one. If the screen is large,
    * it adds this yKey to the existing displayed yKeys, unless there are already 3 yKeys displayed.
    **/
	handleAddYKey(key){
		if(this.state.sizes.windowWidth < 980){
			//replace current yKey with new one
		 	this.setState({
				yKeys:[key],
				selectedOption:this.state.selectedButtonOption
			})
		}else{
			//screen can display up to 3 yKeys so must test how many displayed
			if(this.state.yKeys.length < 3)
				this.setState(state =>{
					return {
						yKeys:[...state.yKeys, key]
					}
				})
			else
				alert("You can only display 3 tests. Remove another one first.")
		}

	}
	/**
	* Removes this yKey from the yKeys that are displayed. If it is the only yKey displayed, it doesn't remove 
	* it but alerts user that they cannot remove all the yKeys.
    * If screen is small, then it will automatically be the only yKey displayed.
    **/
	handleRemoveYKey(key){
		if(this.state.sizes.windowWidth < 980)
				alert("To remove this test chart, select a different one")
		else{
			//screen can display up to 3 yKeys so must test how many displayed
			if(this.state.yKeys.length == 1)
				alert("You must have at least one test selected. To remove this one, select another one first. You can display up to 3 at one time.")
			else
				this.setState(state =>{
					return {
						yKeys:state.yKeys.filter(yKey => yKey != key)
					}
				})
		}
	}
	render(){
		//prepare props for buttons
		//add updated players to buttons  
		let requiredButtons = this.props.item.buttons
		requiredButtons.players = this.state.players.map(p => {
	    		return {key:p._id, butLabel:p.firstName, type:'player'}
	    	})
		//determine which suboptions are active - depends on selectedButtonOption
		let activeSuboptions = this.state.selectedButtonOption == 'players' ?
			this.state.selectedPlayers.map(p => p._id) : this.state.yKeys

		let clickHandlers = { 
			handleShowOptionsButtonClick:this.handleShowOptionsButtonClick,
			handleOptionButtonClick:this.handleOptionButtonClick,
			handleSuboptionButtonClick:this.handleSuboptionButtonClick
		}
		let buttonsConfig = {
			optionsAreShown:this.state.optionsAreShown, 
			selectedButtonOption:this.state.selectedButtonOption,
			moreButtonSelected:this.state.moreButtonSelected,	
			activeSuboptions:activeSuboptions, 
			fillScale:this.state.fillScale
		}
		return(
			<div className='svg-wrapper'>
				<div className={'svg-cont '+(this.state.optionsAreShown ? 'small' : 'large')}
						ref={svgContainer => this.svgContainer = svgContainer}>
					<svg className={'compare-svg'} width='100%' height='100%'
						style={{opacity:0, transition:'opacity 200ms ease-in 0ms'}} >
					</svg>
				</div>
				<ChartButtons buttons={requiredButtons} config={buttonsConfig} clickHandlers={clickHandlers}/>
			</div>
		)
	}
}

CompareSVG.defaultProps = {
}

export default CompareSVG
