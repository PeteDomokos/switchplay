import regression from 'regression'
import * as d3 from 'd3'
//general
import D3Helpers from '../D3Helpers'
import { calculateXValue, createXScale, createYScale, getMaxValuePlusFromRange, getMinValueMinusFromRange} 
	from '../GeneralDataHelpers'
import { getMeasure, getScore } from '../TestDataHelpers'

/**
* Returns an object containing the size information for the d3 components to use  
* @param {number} 
* @param {number} 
**/
const chartSizes = (width, height, windowWidth, windowHeight) => {
	const sizesLS = {windowWidth:windowWidth, windowHeight:windowHeight,
		height:height, width:width, wSF:width/550, hSF:height/320, 
		marginTop:height *0.24, chartHeight:height *0.64, marginBottom:height *0.12, 
		marginLeft:width*0.05, chartWidth:width*0.9, marginRight:width*0.05}
	const sizesMS = {windowWidth:windowWidth, windowHeight:windowHeight,
		height:height, width:width, wSF:width/550, hSF:height/320, 
		marginTop:height *0.15, chartHeight:height *0.72, marginBottom:height *0.13, 
		marginLeft:width*0.25, chartWidth:width*0.7, marginRight:width*0.05}
	const sizesSS = {windowWidth:windowWidth, windowHeight:windowHeight,
		height:height, width:width, wSF:width/550, hSF:height/320, 
		marginTop:height *0.23, chartHeight:height *0.64, marginBottom:height *0.13, 
		marginLeft:width*0.15, chartWidth:width*0.8, marginRight:width*0.05}
	return window.innerWidth < 501 ? sizesSS : window.innerWidth < 980 ? sizesMS : sizesLS

}
/**
* Returns an array of dataObjects, one for each player, formatted for the d3 chart to use
* @param {Object[]}
* @param {string} 
* @param {string}
* @param {string}  
**/
//todo - move methods like getMeasure into item (from TestDataConstants) rather than helper methods	
const formatData = (players, selectedOption, yKey, keyNumberOrders) =>{
	return players.map(p => {
		let score
		if(getMeasure(yKey) == 'total'){
			let scoresObject = p[selectedOption]
			score = d3.sum(p[selectedOption][yKey], d => getScore(yKey, d))
		}
		else{
			//by default, getMeasure is 'best-score'
			score = keyNumberOrders[yKey] == 'low-to-high' ?
				d3.max(p[selectedOption][yKey], d => getScore(yKey, d)) :
				d3.min(p[selectedOption][yKey], d => getScore(yKey, d))
		}
		return {value:score, r:25, ...p}
	})
}


/**
* Returns an object containing the yScale and other associated values
* @param {Object[]}
* @param {string} 
* @param {number} 
* @param {Object}
* @param {Object}  
**/
const calcYScaleData = (datapoints, chartHeight, yKey, targ, keyNumberOrders) =>{
	//y-scale and axis
	let yMin = d3.min(datapoints, d => d.value)
	let yValues = datapoints.map(d => d.value)
	let yMax = d3.max([...yValues, targ])
	let yMinMinus = D3Helpers.getMinValueMinusFromRange(yMax,yMin)
	let yMaxPlus = D3Helpers.getMaxValuePlusFromRange(yMax,yMin)

	let yScale = createYScale(yMinMinus, yMaxPlus, chartHeight, keyNumberOrders[yKey])

	return {yMin:yMin, yMinMinus:yMinMinus, yMax:yMax, yMaxPlus:yMaxPlus, yScale:yScale}
}

export { chartSizes, formatData, calcYScaleData }