import regression from 'regression'
import * as d3 from 'd3'
import colorbrewer from 'colorbrewer'
//general
import { getScore } from '../TestDataHelpers'
import { renderVertTargLine } from '../AdditionalChartComponents'
import ChartComponentRenderer from '../ChartComponentRenderer'
import DisplayItemHelpers from '../DisplayItemHelpers'
//specific to Compare
import { chartSizes, filterPlayers, formatPlayersData, calcXScaleData, 
	calcTarget, calcYScaleData } from './CompareHelpers'


/**
* Renders the x-axis and label
*/
const renderXAxisAndLabel = (svg, sizes, xKey, xScale, keyLabels) =>{
	d3.select(svg).selectAll("g.xAxisG").remove()
		ChartComponentRenderer.axis("x", svg, sizes, xScale, 10)

		d3.select(svg).selectAll("text.xAxisText").remove()
		ChartComponentRenderer.axisLabel("x", keyLabels[xKey], svg, sizes,
		{extraStyle:[["stroke","white"],['font-size', '20px']]})
}

/**
* Renders the y-axis, and either a label or chart title depending on svg dimensions
*/
const renderYAxisAndLabelOrTitle = (svg, sizes, yScale, xValue, yKey, item) =>{
	let { units, measurements, titles } = item
	//yAxis
	ChartComponentRenderer.axis("y", svg, sizes, yScale, 8, yKey)

	//Title / Label
	let reqUnits = units[yKey].full.toLowerCase() == measurements[yKey].toLowerCase() ? 
		"" : "(" +units[yKey].full +")"
	let label = measurements[yKey] +" "+reqUnits
	let yLabel
	//if svg size is short but wide, put title as a side label instead
	if(sizes.width > 400 && sizes.height < 400){
		yLabel = [titles[yKey].short, label]
		ChartComponentRenderer.axisLabel("y", yLabel, svg, sizes, {stroke:"white"})
	}else{
		let title = [titles[yKey].full, label]
		ChartComponentRenderer.title(title, svg, sizes, {
			stroke:'white',
			strokeWidth:function(i){return 1.8-(i*0.8)}, 
			x:function(i){ return xValue },
			y:function(i){ return sizes.marginTop*0.5 +(i*20) }, 
			fontSize:function(i){
			 	if(i == 0) return '1.1rem' 
			 		return '0.9rem'
			},
			classNames:' title-'+yKey
		})
	}
}
/**
*  Renders a circle and photo for each datapoint in the correct position
*  on the swarm plot according to the scale on the y-axis
*/
const renderDatapoints = (svg, datapoints, xValue, yScale, yKey, sizes, fillScale, roleScale) =>{
	var manyBody = d3.forceManyBody().strength(10)
	var force =d3.forceSimulation()
		.force("charge", manyBody)
		.force("x", d3.forceX(xValue))
		.force("y", d3.forceY(d => (yScale(d.value)+sizes.marginTop)).strength(3))
		.force("collision", d3.forceCollide(d => d.r))
		.nodes(datapoints)
		.on("tick", updateNetwork)

	let circleG = "g."+yKey

	//remove
	//d3.select(svg).selectAll(circleG).remove()
	//update
	d3.select(svg).selectAll(circleG)
		.data(datapoints)
		.enter()
		.append("g")
		.attr("class", 'circleG ' +yKey)
	//circle
	d3.select(svg).selectAll(circleG)
		.append("circle")
		.style("fill", d => fillScale(d._id))
		.attr("r", d => d.r)
	//images and onclick info
	d3.select(svg).selectAll(circleG)
		.insert("image","text")
		.attr("xlink:href", d => photoUrl(d))
		//.attr("xlink:href", '/denilson.png') //note: can serve statically instead
		.attr("height", "40px")
		.attr("width", "40px")
		.attr("x", -20)
		.attr("y",-20)
		
	function updateNetwork(){
		d3.select(svg).selectAll(circleG)
			.attr("transform", d => "translate(" +d.x +" , " +d.y +")")
	}

	function photoUrl(player){
		if(player.photo)
			return `/api/players/photo/${player._id}?${new Date().getTime()}`
		return '/api/players/defaultphoto'
	}
}

/**
* Renders the coach's target line onto the chart for the given yKey and scale on the y-axis
*/
const renderTargetLine = (svg, sizes, yScale, targ, yKey) =>{
	//todo - bug - team target line is not dashed
	let { marginLeft, marginTop, chartWidth } = sizes

	d3.select(svg).append("g").attr("class", "targetG targetG-"+yKey)

	d3.select(svg).select("g.targetG-"+yKey).append("line").
				attr("x1", marginLeft).
				attr("x2", marginLeft+chartWidth*0.75).
				attr("y1", yScale(targ) +marginTop).
				attr("y2", yScale(targ) +marginTop).
				style("stroke", "red").
				style("stroke-width", 2).
				style("stroke-dasharray", "5,5")

	d3.select(svg).select("g.targetG-"+yKey).append("text").
		attr("transform","translate(" +(marginLeft +chartWidth*0.75) +"," 
			+(yScale(targ)+marginTop-20) +")").
		style("text-anchor", "end").
		style("font-size", 14).
		style("stroke", "red").
		style("dominant-baseline", "hanging").
		text("target")

	
}
/**
* Renders background zones based on the expected standards data from props for the current skills 
* or fitness test displayed. ony shows zones close to selected payers' scores
*/
const renderZones = (svg, item, state, dataWrapper, allValues) =>{
}
const showInfo = (datapoint) => {
}
const hideInfo = (datapoint) =>{
}

export { renderXAxisAndLabel, renderYAxisAndLabelOrTitle, renderDatapoints, renderTargetLine, renderZones }