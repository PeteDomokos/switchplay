 import regression from 'regression'
import * as d3 from 'd3'
//specific to Compare
import { formatData, calcYScaleData } from './CompareHelpers'

/**
* Returns the Formatted data to be used by d3 functionality
* @return {object} formatted data wrapper, containing array of playersData and data for scales
*/
const swarmData = (selectedPlayers, selectedOption, yKey,  keyNumberOrders, coachTargets, sizes) => {
	let xValue = sizes.marginLeft +(sizes.chartWidth *0.4)
	//SAME AS PROGRESS
	let datapoints = formatData(selectedPlayers, selectedOption, yKey, keyNumberOrders)
	let targ = coachTargets[selectedOption][yKey]
	let yScaleData = calcYScaleData(datapoints, sizes.chartHeight, yKey, targ, keyNumberOrders)

	//update state
	return {  
		datapoints:datapoints,
		xValue:xValue,
		yScaleData:yScaleData,
		targ:targ
	}
}


export default swarmData