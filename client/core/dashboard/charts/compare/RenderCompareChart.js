 import regression from 'regression'
import * as d3 from 'd3'
//specific to Compare
import swarmData from './CompareChartData'
import { renderYAxisAndLabelOrTitle, renderDatapoints, renderTargetLine, renderZones } 
	from './RenderCompareChartComponents'

/**
* Renders one (or more) bee swarm plots using one (or more) calls to a render method.
* For each selected yKey stored in settings, it calls a method to format the data 
* and passes the data to a render method to create a bee swarm plot.
* The svg space is divided horizontally between the plots.
*/
//TODO - GIVE PLAYERS THAT HAVE NO DATA A SCORE OF -1 AND THEY ARE DISPLAYED BELOW THE BUNCH
const renderChart = (svg, sizes, item, settings) =>{
	d3.select(svg).selectAll('g.circleG').remove()
	d3.select(svg).selectAll("text.yAxisText").remove()
	d3.select(svg).selectAll("g.yAxisG").remove()
	d3.select(svg).selectAll("text.title").remove()
	d3.select(svg).selectAll("g.targetG").remove()

	let { fillScale, roleScale, selectedPlayers, selectedOption, yKeys } = settings

	yKeys.forEach((yKey, i) =>{
		//sizes for this bee swarm  
		let swarmChartWidth = sizes.chartWidth/yKeys.length
		let swarmMarginLeft = sizes.marginLeft + i*swarmChartWidth
		//sizes stays the same if only one yKey to display 
		let swarmSizes =  {...sizes, chartWidth:swarmChartWidth, marginLeft:swarmMarginLeft}
		//data for swarm plot
		let swarmDataForThisYKey= swarmData(selectedPlayers, selectedOption, yKey, item.keyNumberOrders, item.coachTargets, swarmSizes)

		renderBeeSwarm(svg, swarmDataForThisYKey, item, selectedOption, yKey, fillScale, roleScale, swarmSizes)

	})
}
/**
* Renders a single bee swarm plot (using several subrender methods) for a given yKey 
* and set of datapoints into part (or all) of the svg.
* The amount and location of the svg taken up is specified in the sizes parameter.
*
*/
const renderBeeSwarm = (svg, swarmData, item, selectedOption, yKey, fillScale, roleScale, sizes) =>{
	let { datapoints, targ, yScaleData, xValue } = swarmData
	let { yScale } = yScaleData
	//axis, label/title
	renderYAxisAndLabelOrTitle(svg, sizes, yScale, xValue, yKey, item)
	//player circles
	renderDatapoints(svg, datapoints, xValue, yScale, yKey, sizes, fillScale, roleScale)
	//target line
	renderTargetLine(svg, sizes, yScale, targ, yKey)
	//background zones
	//flatten all data for zones
	//let allValues = playersData.reduce((player1, player2) => player1.concat(player2))
	//renderZones(svg, item, settings, swarmData, allValues)
}

export { renderChart, renderBeeSwarm }