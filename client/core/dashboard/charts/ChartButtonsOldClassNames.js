import React, {Component} from 'react'
import PropTypes from 'prop-types'
import { renderButtons } from './RenderButtons'
import * as d3 from 'd3'

class ChartButtons extends Component{
	static propTypes ={
	    buttons:PropTypes.object,
	    clickHandlers:PropTypes.object,
	    config:PropTypes.object
	}
	/**
    * 
    **/
	componentDidMount(){
		let { buttons, config, clickHandlers } = this.props
		//selectors
		let selectors = {
			ctrls:'div.svg-ctrls-cont'+config.id,
			showOptionsCtrl:'div.show-options-'+config.id,
		    optionsCtrls:'div.svg-options-ctrls-cont'+config.id,
		    buttonsArea:'div.options-and-suboptions-area'
		}
		renderButtons(buttons, selectors, config, clickHandlers)
	}
	/**
    * 
    **/
	componentDidUpdate(prevProps, prevState){
		let { buttons, config, clickHandlers } = this.props
		//selectors
		let selectors = {
			ctrls:'div.svg-ctrls-cont'+config.id,
			showOptionsCtrl:'div.show-options-'+config.id,
		    optionsCtrls:'div.svg-options-ctrls-cont'+config.id,
		    buttonsArea:'div.options-and-suboptions-area'
		}
		renderButtons(buttons, selectors, config, clickHandlers)
	}
	/**
	*  Removes buttons from DOM
	**/
	componentWillUnmount(){
		d3.select('div.options-and-suboptions-area').selectAll("*").remove()
	}
	render(){
		let { id, optionsAreShown } = this.props.config
		return(
			<div className={'options-and-suboptions-area '
				+(optionsAreShown ? "full-height full-width" : "short-height short-width")}>
				<div className={'suboptions svg-ctrls-cont'+id
					+' '+(optionsAreShown ? " show":" hide")} >
				</div>

				<div className='options-area'>
					<div className={'show-options show-options-'+id}>
					</div>
					<div className={'options svg-options-ctrls-cont'+id
					+' '+(optionsAreShown ? " show":" hide")}>
					</div>
				</div>
			</div>
		)
	}
}

export default ChartButtons

