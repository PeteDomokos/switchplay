import regression from 'regression'
import * as d3 from 'd3'

//todo - use hasChanged within changed method
const whatHasChanged = (state, prevState, width, height) =>{
	let changed = []
	if(prevState.selectedButtonOption != state.selectedButtonOption)
		changed.push('selectedButtonOption')
	if(prevState.moreButtonSelected != state.moreButtonSelected)
		changed.push('moreButtonSelected')
	if(prevState.yKey != state.yKey)
		changed.push('yKey')
	if(prevState.yKeys != state.yKeys)
		changed.push('yKeys')
	if(prevState.xKey != state.xKey)
		changed.push('xKey')
	if(width && height && prevState.sizes)
		if(prevState.sizes.width != width || prevState.sizes.height != height)
			changed.push('sizes')
	let playersAdded = state.selectedPlayers ? 
		state.selectedPlayers.filter(p => !prevState.selectedPlayers.includes(p)) : []
	let playersRemoved = prevState.selectedPlayers ? 
		prevState.selectedPlayers.filter(p => !state.selectedPlayers.includes(p)) : []
	if(playersAdded.length != 0 || playersRemoved.length != 0)
		changed.push('selectedPlayers')
	return changed
}

const hasChanged = (property, prevState, state) =>{
	switch(property){
		case 'sizes':return prevState.sizes.chartWidth != state.sizes.chartWidth || 
			prevState.sizes.chartHeight != state.sizes.chartHeight
		case 'xKey':return prevState.xKey != state.xKey
		case 'yKey':return prevState.yKey != state.yKey
		case 'players':{
			let playersToAdd = state.filteredPlayers.filter(p => !prevState.filteredPlayers.includes(p))
			let playersToRemove = prevState.filteredPlayers.filter(p => !state.filteredPlayers.includes(p))
			return playersToAdd.length > 0 || playersToRemove.length > 0
		}

		default: return false
	}
}
//todo - remove this - handle add and remove separately and no need for a method
const updatedSelectedPlayers = (currentSelectedPlayers, players, playerId) =>{
	if(currentSelectedPlayers.find(p => p._id == playerId) && currentSelectedPlayers.length > 1){
		return currentSelectedPlayers.filter(p => p._id != playerId)
	}
	if(!currentSelectedPlayers.find(p => p._id == playerId)){
		return currentSelectedPlayers.concat(players.find(p => p._id == playerId))
	}
}
const calculateTarget = (data, xKey, yKey, xMin, xMax, targets, keyNumberOrders) =>{
	let xMinDatapoint = data.find(d => d[xKey] == xMin)
	let firstYValue = xMinDatapoint[yKey]//eg week 1 score
	let _pcChangeTarg = targets.find(targs => targs[xKey] == xMax)[yKey].val 
	// could be pc or abs
	let pcChangeTarg = _pcChangeTarg ? _pcChangeTarg : 20
	return keyNumberOrders[yKey] == 'high-to-low' ? 
				firstYValue*(100 - pcChangeTarg)/100 : firstYValue*(100 + pcChangeTarg)/100
}
const createRegressionFunc = (playerData, xKey, yKey, xMaxRoundedUp, targ, keyNumberOrder) =>{
	if(playerData.length > 1){
		let datapairs = playerData.map(d => [d[xKey], d[yKey]])
		let expRegFunc = regression.exponential(datapairs);
		//let regFunc = regression.polynomial(datapairs, {order:3});
		//ALT - LINEAR BASED ON LAST 2 POINTS
		let lastPoint = playerData[playerData.length-1]
		let secondLastPoint = playerData[playerData.length-2]
		let lastTwoDatapairs = [ [secondLastPoint[xKey],secondLastPoint[yKey] ] , [lastPoint[xKey],lastPoint[yKey] ] ]
		let linearRegFuncBasedOnTwoPoints = regression.linear(lastTwoDatapairs)
		//ALT - LINEAR BASED ON LAST 3 POINTS (	OR 2 IF ONLY 2 EXIST)
		let thirdLastPoint = playerData.length >=3 ? playerData[playerData.length-3] : []
		let thirdLastPair = thirdLastPoint ? [thirdLastPoint[xKey], thirdLastPoint[yKey]] : []
		let lastTwoOrThreeDatapairs= [thirdLastPair].concat(lastTwoDatapairs)
		let linearRegFuncBasedOnTwoOrThreePoints = regression.linear(lastTwoOrThreeDatapairs)

		let expRegFuncBasedOnLastThree = regression.exponential(lastTwoOrThreeDatapairs)
		return expRegFunc
	}else{
		//as only 1 point, we make line reach targ at last x.
		//If targ is below 1st value which can happen if its a team targ, then we * 1st value by 1.2(low-to-high) or 0.8(high-to-low)
		let firstPoint = playerData[0]
		let firstYValue = firstPoint[yKey]
		let targAlreadyAchieved = () =>{
			if(targ == firstYValue) 
				return true
			if(keyNumberOrder == 'low-to-high' && targ < firstYValue)
				return true
			if(keyNumberOrder == 'high-to-low' && targ > firstYValue)
				return true
			return false 
		}

		if(targAlreadyAchieved())
			if(keyNumberOrder == 'low-to-high')
				targ = firstYValue*1.1
			else
				targ = firstYValue*0.8

		//todo -sort out this linear func - should need to do *0.99 to make it equal to targ at x=12
		//if it donesnt work, i should just replace with an svg line
		let firstPointAndTargPairs = [ 
			[firstPoint[xKey], firstYValue], 
			[xMaxRoundedUp, targ*0.99] 
		]
		return regression.linear(firstPointAndTargPairs)
	}

}
const targetMet = (datavalue, targ, keyNumberOrders) =>{
	if(keyNumberOrders == 'high-to-low' && datavalue <= targ)
		return true
	if((!keyNumberOrders || keyNumberOrders == 'low-to-high') && 
		datavalue >= targ)
		return true
	return false
}

const calculateXValue = (date, startingDate, xKey) => {
	switch(xKey){
		//todo - impl calcMonthc
		case 'monthNr': return calculateWeek(date, startingDate)
		default: return calculateWeek(date, startingDate) 
	}
}
const calculateWeek = (date, startingDate) => {
	//convert time to milli if String or Date
  	let startMil = 
	   typeof startingDate == 'number' ? startingDate : 
	   typeof startingDate == 'string' ? Date.parse(startingDate) : startingDate.getTime()
    let requiredMil = 
       typeof date == 'number' ? date : 
       typeof startingDate == 'string' ? Date.parse(date) : date.getTime()
	if(requiredMil - startMil >= 0)
	   return weeksElapsed(requiredMil, startMil)
	return weeksElapsed(requiredMil, startMil) - 1
}
const weeksElapsed = (requiredMil, startMil) => {
  //get elapsed time in milliseconds
  var mil = requiredMil - startMil
  //calculate the elapsed time in weeks, days, hours, mins and secs
  var seconds = (mil / 1000) | 0;
  mil -= seconds * 1000;

  var minutes = (seconds / 60) | 0;
  seconds -= minutes * 60;

  var hours = (minutes / 60) | 0;
  minutes -= hours * 60;
  var days = (hours / 24) | 0;
  hours -= days * 24;

  var weeks = (days / 7) | 0;
  days -= weeks * 7;

  return weeks
}
//doesnt depend on keyNumberOrder
const lowestZoneBoundary = (zones) =>{
	return 0
}
const highestZoneBoundary = (zones) =>{
	return 100
}

const createXScale = (xMinMinus, xMaxPlus, chartWidth, numberOrder) =>{
	let scale =  d3.scaleLinear().domain([xMinMinus,xMaxPlus]).range([0,chartWidth])
	//reverse axis if necc
	if(numberOrder == 'high-to-low')
		return d3.scaleLinear().domain([xMaxPlus,xMinMinus]).range([chartWidth,0])
	return d3.scaleLinear().domain([xMinMinus,xMaxPlus]).range([0,chartWidth])
}
const createYScale = (yMinMinus, yMaxPlus, chartHeight, numberOrder) =>{
	//reverse axis if necc
	if(numberOrder == 'high-to-low')
		return d3.scaleLinear().domain([yMaxPlus, yMinMinus]).range([chartHeight,0])
	return d3.scaleLinear().domain([yMinMinus,yMaxPlus]).range([chartHeight,0])
}

export { whatHasChanged, hasChanged, updatedSelectedPlayers, calculateXValue,  calculateTarget, createRegressionFunc, 
	targetMet, createXScale, createYScale }