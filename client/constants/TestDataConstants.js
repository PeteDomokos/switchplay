export const standardDatasets = [
  {category:'Skills', name:'Touch', nrParticipants:1, 
   values:[{name:'Score', dataType:'number'}], isStandard:true},
  {category:'Skills', name:'Touch', nrParticipants:2, 
   values:[{name:'Score', dataType:'number'}], isStandard:true},
  {category:'Skills', name:'Touch', nrParticipants:3, 
   values:[{name:'Score', dataType:'number'}], isStandard:true},
  {category:'Skills', name:'Switchplay', nrParticipants:1, 
   values:[{name:'Score', dataType:'number'}], isStandard:true},
  {category:'Skills', name:'Dribble', nrParticipants:1, 
   values:[{name:'Time', unit:'Seconds', dataType:'number'}], isStandard:true},
  {category:'Fitness', name:'Yoyo', nrParticipants:1, 
   values:[{name:'Distance', unit:'Metres', dataType:'number'}], isStandard:true},
  {category:'Fitness', name:'T-Test', nrParticipants:1, 
   values:[{name:'Time', unit:'Seconds', dataType:'number'}], isStandard:true},
  {category:'Fitness', name:'Shuttles', nrParticipants:1, 
   values:[{name:'Time', unit:'Seconds', dataType:'number'}], isStandard:true}
]

export const TestDataConstants ={
	targets:[{weekNr:12, tTest:20, yoyo:20, shuttles:20, touch:20, switchplay:20, dribble:20}],
	coachTargets:{
			skills:{touch:75, switchplay:30, dribble:6},
	    	fitness:{tTest:12, yoyo:2000, shuttles:8},
	    	games:{wins:6, points:15, keepUps:10}
    },
    zones:{ tTest:[ {id:'social', name:'Social', nr:1, lowerThreshold:25, upperThreshold:14}, 
					{id:'amateur', name:'Amateur', nr:2, lowerThreshold:14, upperThreshold:12.5}, 
					{id:'semiPro', name:'Semi-Pro', nr:3, lowerThreshold:12.5, upperThreshold:11}, 
					{id:'pro', name:'Pro', nr:4, lowerThreshold:11, upperThreshold:10}, 
					{id:'elite', name:'Elite', nr:5, lowerThreshold:10, upperThreshold:9}, 
					{id:'worldClass', name:'World Class', nr:6, lowerThreshold:9, upperThreshold:5}],
					
			yoyo:[  {id:'social', name:'Social', nr:1, lowerThreshold:1000, upperThreshold:1600}, 
					{id:'amateur', name:'Amateur', nr:2, lowerThreshold:1600, upperThreshold:1800}, 
					{id:'semiPro', name:'Semi-Pro', nr:3, lowerThreshold:1800, upperThreshold:2100}, 
					{id:'pro', name:'Pro', nr:4, lowerThreshold:2100, upperThreshold:2300}, 
					{id:'elite', name:'Elite', nr:5, lowerThreshold:2300, upperThreshold:2500}, 
					{id:'worldClass', name:'World Class', nr:6, lowerThreshold:2500, upperThreshold:3000}],
			shuttles:[  
					{id:'social', name:'Social', nr:1, lowerThreshold:20, upperThreshold:13}, 
					{id:'amateur', name:'Amateur', nr:2, lowerThreshold:13, upperThreshold:11}, 
					{id:'semiPro', name:'Semi-Pro', nr:3, lowerThreshold:11, upperThreshold:9.5}, 
					{id:'pro', name:'Pro', nr:4, lowerThreshold:9.5, upperThreshold:8.5}, 
					{id:'elite', name:'Elite', nr:5, lowerThreshold:8.5, upperThreshold:7.5}, 
					{id:'worldClass', name:'World Class', nr:6, lowerThreshold:7.5, upperThreshold:0}],

			touch:[ {id:'social', name:'Social', nr:1, lowerThreshold:0, upperThreshold:20}, 
					{id:'amateur', name:'Amateur', nr:2, lowerThreshold:20, upperThreshold:40}, 
					{id:'semiPro', name:'Semi-Pro', nr:3, lowerThreshold:40, upperThreshold:60}, 
					{id:'pro', name:'Pro', nr:4, lowerThreshold:60, upperThreshold:75}, 
					{id:'elite', name:'Elite', nr:5, lowerThreshold:75, upperThreshold:90}, 
					{id:'worldClass', name:'World Class', nr:6, lowerThreshold:90, upperThreshold:100}],
					
			switchplay:[  {id:'social', name:'Social', nr:1, lowerThreshold:0, upperThreshold:20}, 
					{id:'amateur', name:'Amateur', nr:2, lowerThreshold:20, upperThreshold:40}, 
					{id:'semiPro', name:'Semi-Pro', nr:3, lowerThreshold:40, upperThreshold:60}, 
					{id:'pro', name:'Pro', nr:4, lowerThreshold:60, upperThreshold:75}, 
					{id:'elite', name:'Elite', nr:5, lowerThreshold:75, upperThreshold:90}, 
					{id:'worldClass', name:'World Class', nr:6, lowerThreshold:90, upperThreshold:100}],
			dribble:[  
					{id:'social', name:'Social', nr:1, lowerThreshold:30, upperThreshold:10.5}, 
					{id:'amateur', name:'Amateur', nr:2, lowerThreshold:10.5, upperThreshold:9}, 
					{id:'semiPro', name:'Semi-Pro', nr:3, lowerThreshold:9, upperThreshold:7.5}, 
					{id:'pro', name:'Pro', nr:4, lowerThreshold:7.5, upperThreshold:6}, 
					{id:'elite', name:'Elite', nr:5, lowerThreshold:6, upperThreshold:4.5}, 
					{id:'worldClass', name:'World Class', nr:6, lowerThreshold:4.5, upperThreshold:3}]
	},	
	keyLabels:{weekNr:'Weeks', tTest:['T-Test', '(agility)'], yoyo:['YoYo', '(CV)'], 
			   shuttles:['Shuttles', '(agility)'], touch:'Touch', switchplay:'Switchplay', 
			   dribble:'Dribble', wins:'Wins', points:'Points', keepUps:'Keep Ups'},
	measurements:{weekNr:"Time", tTest:"Time", yoyo:"Distance", shuttles:"Time", touch:"Points", switchplay:"Points", 
		dribble:"Time", wins:"Wins", points:"Points", keepUps:"Total"
	},
	titles:{weekNr:{full:'Week Number', short:'Week', vShort:'Week'},
	     tTest:{full:'Agility T-Test', short:'Agility T-Test', vShort:'Ttest'}, 
	     yoyo:{full:'Yoyo Endurance Test L2', short:'Yoyo Endurance', vShort:'Yoyo'},
		 shuttles:{full:'Shuttle Runs', short:'Shuttle Runs', vShort:'Shut'},
		 touch:{full:'Touch Test', short:'Touch Test', vShort:'Touch'},
		 switchplay:{full:'Switchplay Passing Test', short:'Switchplay Passing', vShort:'Switch'},
		 dribble:{full:'Timed Dribble Test', short:'Dribble Test', vShort:'Drib'},
		 wins:{full:'Number of Wins', short:'Wins', vShort:'Wins'},
		 points:{full:'Number of Points', short:'Points', vShort:'Pts'},
		 keepUps:{full:'Number of Kick-Ups', short:'Kick-Ups', vShort:'Kicks'}
	},
	//unit may be blank
	units:{
		weekNr:{full:"weeks", short:"wks", symbol:""},
		tTest:{full:"seconds", short:"secs", symbol:"s"},
		yoyo:{full:"metres", short:"mtrs", symbol:"m"},
		shuttles:{full:"seconds", short:"secs", symbol:"s"}, 
		touch:{full:"points", short:"pts", symbol:""}, 
		switchplay:{full:"points", short:"pts", symbol:""}, 
		dribble:{full:"seconds", short:"secs", symbol:"s"}, 
		wins:{full:"wins", short:"wins", symbol:""}, 
		points:{full:"points", short:"pts", symbol:""},
		keepUps:{full:"kick-ups", short:"kicks", symbol:""},
	},
	keyNumberOrders:{weekNr:'low-to-high', tTest:'high-to-low', yoyo:'low-to-high',
					shuttles:'high-to-low', touch:'low-to-high', switchplay:'low-to-high',
					dribble:'high-to-low', wins:'low-to-high', points:'low-to-high', keepUps:'low-to-high'}, 
    dispType:'scatter',
    initSelectedOption:'fitness', 
    initXKey:'weekNr', 
    initYKey:'tTest',
    buttons:{
    	//todo - give each option a label
    	skills:[{key:'touch', butLabel:'Touch', type:'skill'}, {key:'switchplay', butLabel:'Switchplay', type:'skill'}, 
    		 {key:'dribble', butLabel:'Dribble', type:'skill'}],
    	fitness:[{key:'tTest', butLabel:'T-Test', type:'fitness'}, {key:'yoyo', butLabel:'YoYo', type:'fitness'}, 
    		 {key:'shuttles', butLabel:'Shuttles', type:'fitness'}],
    	//games:[{key:'wins', butLabel:'Wins'}, {key:'points', butLabel:'Points'}, 
    		 //{key:'keepUps', butLabel:'Keep Ups'}],
    	//players default to []
    	players:[]
    },
    titleLines:'My Progress', 
    xMaxes:{'weekNr':12}
}