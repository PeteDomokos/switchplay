 import C from './constants/ConstantsForStore'
import _ from 'lodash'
import * as cloneDeep from 'lodash/cloneDeep'

export const user= (state={}, action) =>{
	switch(action.type){
		case C.SAVE_USER:{
			return action.value
		}
		case C.UPDATE_USER:{
			return state
		}
		case C.DELETE_USER:{
			return {}
		}
		case C.SAVE_GROUP:{
			return {...state, groups:groups(state.groups, action)}
		}
		case C.SAVE_NEW_GROUP:{
			const { group } = action
			//add shallow version of group to adminGroups
			const shallowGroup = {
				name:group.name, desc:group.desc, _id:group._id, players:group.players,
				coaches:group.coaches, parent:group.parent, admin:group.admin}
			const _adminGroups = [...state.adminGroups, shallowGroup]
			//also add full version of group to user.groups
			const _groups = [...state.groups, shallowGroup]
			return {...state, adminGroups:_adminGroups, groups:_groups}
		}
		case C.UPDATE_GROUP:{
			const group = { action }
			//update shallow version of in user.adminGroups
			const shallowGroup = {
				name:group.name, desc:group.desc, _id:group._id, players:group.players,
				coaches:group.coaches, parent:group.parent, admin:group.admin}
			const otherAdminGroups = state.adminGroups.filter(g => g._id !== group._id)
			const _adminGroups = [...otherAdminGroups, group]
			//also update full version of group in user.groups
			const otherGroups = state.groups.filter(g => g._id !== group._id)
			const _groups = [...otherGroups, group]
			return {...state, adminGroups:_adminGroups, groups:_groups}
		}
		case C.DELETE_GROUP:{
			//note it can only be an admin group
			return {
				...state, 
				adminGroups:groups(state.adminGroups, action),
				groups:groups(state.groups, action)
			}
		}
		case C.ADD_PLAYER:{
			return {...state, groups:groups(state.groups, action)}
		}
		case C.REMOVE_PLAYER:{
			return {...state, groups:groups(state.groups, action)}
		}
		default:{
			return state
		}
	}
}

const groups = (state={}, action) =>{
	switch(action.type){
		//todo - change to update group
		//if no group exists with that id, it adds it.Otherwise it updates existing
		case C.SAVE_GROUP:{
			const existingGroup = state.find(g => g._id === action.group._id)
			const groupToUpdate = existingGroup ? existingGroup : {}
			const otherGroups = state.filter(g => g._id !== action.group._id)
			return [...otherGroups, {...groupToUpdate, ...action.group}]
		}
		case C.DELETE_GROUP:{
			return state.filter(g => g._id !== action.id)
		}
		case C.ADD_PLAYER:{
			const groupToUpdate = state.find(g => g._id === action.groupId)
			const otherGroups = state.filter(g => g._id !== action.groupId)
			return [...otherGroups, group(groupToUpdate, action)]
		}
		case C.REMOVE_PLAYER:{
			const groupToUpdate = state.find(g => g._id === action.groupId)
			const otherGroups = state.filter(g => g._id !== action.groupId)
			return [...otherGroups, group(groupToUpdate, action)]
		}
		default:
			return state
	}

}
const group = (state={}, action) =>{
	switch(action.type){
		//todo - change to update group
		//note - no need to check group as it was done in groups reducer above
		case C.ADD_PLAYER:{
			const otherPlayers = state.players.filter(p => p._id !== action.player._id)
			return {...state, players:[...otherPlayers, action.player]}
		}
		case C.REMOVE_PLAYER:{
			const otherPlayers = state.players.filter(p => p._id !== action.player._id)
			return {...state, players:otherPlayers}
		}
		default:
			return state
	}

}

export const available = (state={}, action) =>{
	switch(action.type){
		case C.SAVE_AVAILABLE:
			return {...state, [action.entity]:action.value}
		default:
			return state
	}
}


export const dashboard= (state={}, action) =>{
	const { type, path, value } = action
	
	switch(type){
		case C.SET_DASHBOARD_FILTER:{
			//if(type == subgroup)
				//reset players, player
				//if(!inherit) reset datasets
			return {
				state, 
				filters:{...state.filters, [path]:value}
			}
		}
		case C.SAVE_SELECTIONS:{
			//todo - swicth all this around so we have a selections reducer which routes the path
			if(action.path.includes("dashboard")){
				const innerPath = action.path.replace("dashboard.", "")
				//for now, innerPath is 1 level, but when we have subpaths then use lodash _.set
				if(action.path.includes("datasets"))
					return {
						...state,
						datasetsSelected:true,
						[innerPath]:action.selections
					}
				else
					return {
						...state,
						selected:true,
						[innerPath]:action.selections
					}

			}
		}
		case C.START_CHANGING_SELECTIONS:{
			return {
				...state,
				selected:false
			}
		}
		default:
			return state
	}
}
//todo - look into whether we can use the same groups reducer for otherGroups instead,
//and user for otherPlayers

//todo - change name to nonUserItems
export const otherItems = (state={}, action) =>{
	switch(action.type){
		case C.SAVE_GROUPS:
			return {...state, groups:action.groups}
		case C.SAVE_PLAYERS:
			return {...state, players:action.players}		
		default:
			return state
	}
}

//todo - generalise action to just FETCH_START

export const asyncProcesses= (state={}, action) =>{
	const { type, path, value } = action
	switch(type){
		case C.START:{
			let _state = cloneDeep(state)
			_.set(_state, path, {pending:true})
			return _state
		}
		case C.END:{
			let _state = cloneDeep(state)
			if(path.includes('updating'))
				_.set(_state, path, {complete:true})
			//if loading, then no need for the referring components to receive 
			//completion message as they will receive the data via the store anyway
			//todo - decide how to handle open being set to true and error messages
			//else if(path.includes('creating'))
				//_.set(_state, path, {complete:true})
			else
				_.set(_state, path, false)
			return _state			
		}
		case C.RESET_STATUS:{
			let _state = cloneDeep(state)
			_.set(_state, path, false)
			return _state
		}
		case C.SET_DASHBOARD_FILTER:{
			//if(type == subgroup)
				//reset players, player
				//if(!inherit) reset datasets
			return {
				state, 
				dashboardFilters:{...state.dashboardFilters, [path]:value}
			}
		}
		default:
			return state
	}
}

//todo - instead of a boolean, use a count so that dialogs can haev multiple stages
//may use that for question answer tracking
export const dialogs = (state={}, action) =>{
	const { type, path, value } = action
	switch(type){
		case C.OPEN_DIALOG:{
			let _state = cloneDeep(state)
			_.set(_state, path, true)
			return _state
		}
		case C.CLOSE_DIALOG:{
			let _state = cloneDeep(state)
			_.set(_state, path, false)
			return _state			
		}
		//automatically close dialog upon deletion
		case C.DELETE_GROUP:{
			let _state = cloneDeep(state)
			_.set(_state, "deleteGroup", false)
			return _state			
		}
		default:
			return state
	}
}

/*
//TODO throw an error if uName and pw are incorrect format. This can be caught in error middleware (see redux middleware tutorial on you tube) 
export const user= (state={}, action) =>{
	switch(action.type){
		case C.LOGIN:
			return action.user
		case C.DEMO_LOGIN:
			return users.find((u => u.uName == "Brian" && u.pw == "p"))
		default:
			return state
	}
}

export const profiles = (state={}, action) =>{
	switch(action.type){
		default:
			return state
	}
}

export const tracker = (state={}, action) =>{
	switch(action.type){
		case C.SET_TRACKER:
			return action.tracker
		case C.UPDATE_FEATURE_ACTIVE:
			return {...state, featureActive: action.featureName}
		case C.UPDATE_REP_ACTIVE:
			return {...state, report: reportTracker(state.report, action)}
		case C.UPDATE_REPORT_SECT_ACTIVE:
			return {...state, report: reportTracker(state.report, action)}
		case C.UPDATE_REPORT_ACT_ACTIVE:
			return {...state, report: reportTracker(state.report, action)}
		case C.OVERRIDE_Q_ACTIVE:
			return {...state, report: reportTracker(state.report, action)}
		default:
			return state
		}
}
export const reportTracker = (state={}, action) =>{
	switch(action.type){
		case C.UPDATE_REP_ACTIVE:
			return {repActive: action.repId, sectActive: 0, actActive: 0, qActiveOverride: -1}
		case C.UPDATE_REPORT_SECT_ACTIVE:
			return {...state, sectActive: action.sectNr, actActive: 0, qActiveOverride: -1}
		case C.UPDATE_REPORT_ACT_ACTIVE:
			return {...state, actActive: action.actNr, qActiveOverride: -1}
		case C.OVERRIDE_Q_ACTIVE:
			return {...state, qActiveOverride: action.qNr}
	}
}

export const reports = (state={}, action) =>{
	switch(action.type){
		case C.SET_REPORTS:{
			return action.reports
		}
		case C.SAVE_PART_SEC_ANSWER:{
			return state.map(rep => report(rep, action))
		}
		//case C.UPDATE_ACT_INTRO_COMPLETED:
			//return state.map(rep => report(rep, action))
		case C.PROCESS_ACT_INTRO_COMPLETED:{
			return state.map(rep => report(rep, action))
		}
		case C.UPDATE_PART_ACTIVE:{
			return state.map(rep => report(rep, action))
		}
		case C.Q_MOVE_ON:
			return state.map(rep => report(rep, action))
		default:
			return state
	}
}
export const report = (state={}, action) =>{
	switch(action.type){
		case C.SAVE_PART_SEC_ANSWER:
			return (state.id !== Number(action.repId)) ?
				state
				:
				{...state, userAnswers: state.userAnswers.map(uAns  => userAnswers(uAns, action))}
		//case C.UPDATE_ACT_INTRO_COMPLETED:
			//return (state.id !== action.repTracker.repActive) ?
				//state
				//:
				//{...state, sects: state.sects.map(sect => reportSect(sect, action))}
		case C.PROCESS_ACT_INTRO_COMPLETED:{
			return (state.id !== action.repId) ?
				state
				:
				{...state, sects: state.sects.map(sect => reportSect(sect, action))}
		}
		case C.UPDATE_PART_ACTIVE:
			return (state.id !== Number(action.repId)) ?
				state
				:
				{...state, partsActive: state.partsActive.map(partAct  => partsActive(partAct, action))}
		
		case C.Q_MOVE_ON:
			return (state.id !== action.repTracker.repActive) ?
				state
				:
				{...state, sects: state.sects.map(sect => reportSect(sect, action))}
		default:
			return state
	}
}
export const partsActive = (state={}, action) =>{
	switch(action.type){
		case C.UPDATE_PART_ACTIVE:{
			if(state.sectNr !== action.sectNr)
				return state
			if(state.actNr !== action.actNr)
				return state
			if(state.qNr !== action.qNr)
				return state
			else
				return {...state, nr: action.partNrActive}
		}
		default:
			return state
	}
}
export const userAnswers = (state={}, action) =>{
	switch(action.type){
		case C.SAVE_PART_SEC_ANSWER:{
			if(state.sectNr !== action.sectNr)
				return state
			if(state.actNr !== action.actNr)
				return state
			if(state.qNr !== action.qNr)
				return state
			if(state.partNr !== action.partNr)
				return state
			if(state.ansSecNr !== action.ansSecNr)
				return state
			else
				return {...state, ans: action.answer}
		}
		default:
			return state
	}
}
export const reportSect = (state={}, action) =>{
	switch(action.type){
		case C.PROCESS_ACT_INTRO_COMPLETED:{
			return (state.nr !== action.sectNr) ?
				state
				:
				{...state, acts: state.acts.map(act => reportSectAct(act, action))}
		}
		//case C.UPDATE_ACT_INTRO_COMPLETED:
			//return (state.nr !== action.repTracker.sectActive) ?
				//state
				//:
				//{...state, acts: state.acts.map(act => reportSectAct(act, action))}
		case C.Q_MOVE_ON:
			return (state.nr !== action.repTracker.sectActive) ?
				state
				:
				{...state, acts: state.acts.map(act => reportSectAct(act, action))}
		default:
			return state
	}
}
export const reportSectAct = (state={}, action) =>{
	switch(action.type){
		case C.PROCESS_ACT_INTRO_COMPLETED:
			return (state.nr !== action.actNr) ?
				state
				:
				{...state, intro: reportSectActIntro(state.intro, action)}
		//case C.UPDATE_ACT_INTRO_COMPLETED:
			//return (state.nr !== action.repTracker.actActive) ?
				//state
				//:
				//{...state, introStatus:"completed"}
		case C.Q_MOVE_ON:
			return (state.nr !== action.repTracker.actActive) ?
				state
				:
				{...state, qs:state.qs.map(q => reportSectActQ(q, action))}
		default:
			return state
	}
}
//TODO !!!!!!!!!!!!!!!!!!!!!!!!!! - get rid of status here- make it part of userAnswers in report
export const reportSectActIntro = (state={}, action) =>{
	switch(action.type){
		case C.PROCESS_ACT_INTRO_COMPLETED:
			return {...state, status:"completed"}
		default:
			return state
	}
}
export const reportSectActQ = (state={}, action) =>{
	switch(action.type){
		case C.Q_MOVE_ON:
			return (state.nr !== action.qNr) ?
				state
				:
				{...state, movedOn:true}
		default:
			return state
	}
}

export const reportSectActQPart= (state={}, action) =>{
	switch(action.type){
		default:
			return state
		}
}

export const reportSectActQPartSec= (state={}, action) =>{
	switch(action.type){
		default:
			return state
		}
}
*/

