import React, {Component} from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'

import { PageTemplate } from './PageTemplate'


const mapStateToProps = state => {
	return({})
}
const mapDispatchToProps = dispatch => ({
	removeUserFromStore(){
		alert("PageTemplateContainer removing user")
		dispatch(removeUser())
	}
})

//wrap all 4 sections in the same container for now.
const PageTemplateContainer = connect(
	mapStateToProps,
	mapDispatchToProps
	)(PageTemplate)

export default PageTemplateContainer

