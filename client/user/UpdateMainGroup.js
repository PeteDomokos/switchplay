import React, {Component} from 'react'
import {Redirect} from 'react-router-dom'
import PropTypes from 'prop-types'
import Card, {CardActions, CardContent} from 'material-ui/Card'
import Button from 'material-ui/Button'
import TextField from 'material-ui/TextField'
import Typography from 'material-ui/Typography'
import Icon from 'material-ui/Icon'
import Avatar from 'material-ui/Avatar'
import List, {ListItem, ListItemAvatar, ListItemSecondaryAction, ListItemText} from 'material-ui/List'
import IconButton from 'material-ui/IconButton'
import ArrowForward from 'material-ui-icons/ArrowForward'
import Person from 'material-ui-icons/Person'
//helpers
import {withStyles} from 'material-ui/styles'
import auth from './../auth/auth-helper'
import { listGroups } from '../groups/api-group.js'

const styles = theme => ({
  card: {
    maxWidth: 600,
    margin: 'auto',
    textAlign: 'center',
    marginTop: theme.spacing.unit * 5,
    paddingBottom: theme.spacing.unit * 2
  },
  title: {
    margin: theme.spacing.unit * 2,
    color: theme.palette.protectedTitle
  },
  error: {
    verticalAlign: 'middle'
  },
  textField: {
    marginLeft: theme.spacing.unit,
    marginRight: theme.spacing.unit,
    width: 300
  },
  submit: {
    margin: 'auto',
    marginBottom: theme.spacing.unit * 2
  },
  bigAvatar: {
    width: 60,
    height: 60,
    margin: 'auto'
  },
  input: {
    display: 'none'
  },
  filename:{
    marginLeft:'10px'
  }
})

class UpdateMainGroup extends Component {
  constructor({match}) {
    super()
    this.state = {
      selectingGroup:false,
      availableGroups:'',
      group:''
    }
    this.match = match
  }
  componentDidMount = () =>{
    //maybe undefined
    this.setState({group:this.props.currentGroup})
  }
  componentDidUpdate = () =>{
    //maybe undefined
    console.log("CDU group")
   // need to recall how to set state when props change s its not in  a loop
    //this.setState({group:this.props.currentGroup})
  }
  onSelectGroup = () =>{
    this.setState({selectingGroup:true})
    if(!this.state.availableGroups){
      listGroups().then((data) =>{
        if(data.error){
          console.log("group list error", data)
          this.setState({error: data.error})
        }else{
          console.log("groups: list ", data)
          this.setState({availableGroups: data})
        }
      })
    }
  }
  setGroup = (g) =>{
    this.props.onSelect(g)
    this.setState({mainGroup:g, selectingGroup:false})
  }
  render() {
    const {classes} = this.props
    const photoUrl = this.state.id
                 ? `/api/users/photo/${this.state.id}?${new Date().getTime()}`
                 : '/api/users/defaultphoto'
    if (this.state.redirectToProfile) {
      return (<Redirect to={'/user/' + this.state.id}/>)
    }
    return (
      <div>
        <Typography type="headline" component="h2" >
            Main Group: {this.state.group ? this.state.group.name : 'none selected'}
          </Typography>
          <div>
            {this.state.selectingGroup ?
              <div className='groups'>
                <div className='top-line' style={{display:'flex', justifyContent:'space-between'}}>
                  <h6 className='user-list-item'>Click to select</h6>
                  <div onClick={() => {this.setState({selectingMainGroup:false})}}>X</div>
                </div>
                {this.state.availableGroups ?
                  <List dense> 
                    {this.state.availableGroups.map(g =>
                      <ListItem button key={g} onClick={() => this.setGroup(g)}>
                        <ListItemAvatar>
                          <Avatar>
                            <Person/>
                          </Avatar>
                        </ListItemAvatar>
                        <ListItemText primary={g.name}/>
                        <ListItemSecondaryAction>
                        <IconButton>
                            <ArrowForward/>
                        </IconButton>
                        </ListItemSecondaryAction>
                      </ListItem>
                    )}
                  </List>
                  :
                  <div className='user-list-cont'>
                    <div className='user-list-item'>loading groups...</div>
                  </div>
                }
              </div>
              :
              <Button className={classes.selectBtn} onClick={this.onSelectGroup} 
                  color="primary" autoFocus="autoFocus" variant="raised">
                 Select Main Group
              </Button>
             }
          </div> 
      </div>
    )
  }
}

UpdateMainGroup.propTypes = {
  classes: PropTypes.object.isRequired
}

export default withStyles(styles)(UpdateMainGroup)
