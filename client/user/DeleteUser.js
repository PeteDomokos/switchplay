import React, {Component} from 'react'
import {Redirect, Link, withRouter} from 'react-router-dom'
import PropTypes from 'prop-types'
//material-ui
import IconButton from '@material-ui/core/IconButton'
import Button from '@material-ui/core/Button'
import { Dialog, DialogActions, DialogContent, DialogContentText, 
    DialogTitle } from '@material-ui/core'
//icons
import DeleteIcon from '@material-ui/icons/Delete'

class DeleteUser extends Component {
  render() {
    //history is passed to action creator to push after delete
    const { id, deleting, open, error, openDelete, openDialog, 
      deleteUser, closeDialog, history } = this.props

    return (<span>
      <IconButton aria-label="Delete" onClick={openDialog} color="secondary">
        <DeleteIcon/>
      </IconButton>

      <Dialog open={open}>
        <DialogTitle>{"Delete Account"}</DialogTitle>
        <DialogContent>
          <DialogContentText>
            Confirm to delete your account.
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button onClick={closeDialog} color="primary">
            Cancel
          </Button>
          <Button onClick={() => deleteUser(id, history)} color="secondary" autoFocus="autoFocus">
            Confirm
          </Button>
        </DialogActions>
      </Dialog>
    </span>)
  }
}
DeleteUser.propTypes = {
  id: PropTypes.string.isRequired
}
export default withRouter(DeleteUser)
