import React, {Component} from 'react'
import {Redirect, Link} from 'react-router-dom'
import PropTypes from 'prop-types'
//@material-ui/core
import Paper from '@material-ui/core/Paper'
import {List, ListItem, ListItemAvatar, ListItemSecondaryAction, 
    ListItemText} from '@material-ui/core'
import Avatar from '@material-ui/core/Avatar'
import IconButton from '@material-ui/core/IconButton'
import Button from '@material-ui/core/Button'
import Typography from '@material-ui/core/Typography'
import Divider from '@material-ui/core/Divider'
//icons
import Edit from '@material-ui/icons/Edit'
import Person from '@material-ui/icons/Person'
//styles
import {withStyles} from '@material-ui/core/styles'
//child components
import DeleteUserContainer from './containers/DeleteUserContainer'
//helpers
import auth from './../auth/auth-helper'
import {read} from './api-user.js'

const styles = theme => ({
  root: theme.mixins.gutters({
    width: '75vw',
    maxWidth:400,
    margin: 'auto',
    padding: theme.spacing(3),
    marginTop: theme.spacing(0),
  }),
  title: {
    margin: `${theme.spacing(3)}px 0 ${theme.spacing(2)}px`,
    color: theme.palette.protectedTitle
  }
})

class UserProfile extends Component {
  constructor({match}) {
    super()
    this.state = {
      user: {username:'', firstName:'', surname:'', email:'', _id:'', photo:''},
      redirectToSignin: false
    }
  }
  init = (userId) => {
    //check any user is authenticated
    const jwt = auth.isAuthenticated()
    read({
      id: userId
    }, {t: jwt.token}).then((data) => {
      if (data.error) {
        //but need to deal with other server errors
        this.setState({redirectToSignin: true})
      } else {
        this.setState({user: data})
      }
    })
  }
  componentDidMount = () => {
    window.scrollTo(0, 0)
    if(this.props.user)
      this.setState({user:this.props.user})
    else if(this.props.match)
      this.init(this.props.match.params.userId)
  }
  render() {
    const {classes} = this.props
    let backgroundColour = this.props.selected ? 'blue':''
    let profileStyle = {padding:'2%', backgroundColor:backgroundColour}
    
    const user = this.props.user ? this.props.user : this.state.user
    const photoUrl = user._id
              ? '/api/user/photo/' +user._id +'?'+new Date().getTime()
              : '/api/users/defaultphoto'

    if(this.state.redirectToSignin)
      return <Redirect to='/signin'/>
  
    const userIsAuthorisedToUpdate = auth.isAuthenticated() &&
      (auth.isAuthenticated().user._id == user._id || 
        auth.isAuthenticated().user.isSystemAdmin)
    return (
      <Paper onClick={this.props.onClickFunc ? this.props.onClickFunc : () => {}} 
        className={classes.root} style={profileStyle} elevation={4}>
        <List dense>
          <ListItem>

            <ListItemAvatar>
              <Avatar src={photoUrl} className={classes.bigAvatar}/>
            </ListItemAvatar>
            <ListItemText primary={user.firstName +' '+user.surname} 
                          secondary={user.email}/>
            {userIsAuthorisedToUpdate  && !this.props.shortVersion &&
              <UpdateActions userId={user._id}/>}

          </ListItem>
          {!this.props.shortVersion && 
            <AdditionalItems user={user}/> }
        </List>
      </Paper>
    )
  }
}
UserProfile.propTypes = {
  classes: PropTypes.object.isRequired
}
UserProfile.defaultProps = {
}

const UpdateActions = ({userId}) =>
  <ListItemSecondaryAction>
    <Link to={"/user/edit/" + userId}>
      <IconButton aria-label="Edit" color="primary">
        <Edit/>
      </IconButton>
    </Link>
    <DeleteUserContainer id={userId}/>
  </ListItemSecondaryAction>

const AdditionalItems = ({user}) =>
  <React.Fragment>
    <Divider/>
    <ListItem>
      {
        user.created &&
        <ListItemText primary={"Joined: " + (new Date(user.created)).toDateString()}/>
      }
    </ListItem>
  </React.Fragment>


export default withStyles(styles)(UserProfile)
