import React, {Component} from 'react'
import {Redirect, Link} from 'react-router-dom'
import PropTypes from 'prop-types'

//child components
import UserProfile from './UserProfile'
//helpers
import auth from './../auth/auth-helper'
import {read} from './api-user.js'

class User extends Component {
  constructor({match}) {
    super()
    this.state = {
      user: {username:'', firstName:'', surname:'', email:'', _id:''},
      redirectToSignin: false
    }
  }
  init = (userId) => {
    //check any user is authenticated
    const jwt = auth.isAuthenticated()
    read({
      id: userId
    }, {t: jwt.token}).then((data) => {
      if (data.error) {
        //but need to deal with other server errors
        this.setState({redirectToSignin: true})
      } else {
        this.setState({user: data})
      }
    })
  }
  componentDidMount = () => {
    window.scrollTo(0, 0)
    if(this.props.user)
      this.setState({user:this.props.user})
    else if(this.props.match)
      this.init(this.props.match.params.userId)
  }
  render() {
    const {classes} = this.props
    if(this.state.redirectToSignin)
      return <Redirect to='/signin'/>
  
    const userIsAuthorisedToUpdate = auth.isAuthenticated() &&
      (auth.isAuthenticated().user._id == this.state.user._id || 
        auth.isAuthenticated().user.isSystemAdmin)
    return (
        <UserProfile user={this.state.user}/>
    )
  }
}
User.propTypes = {
  classes: PropTypes.object.isRequired
}
User.defaultProps = {
}

export default User
