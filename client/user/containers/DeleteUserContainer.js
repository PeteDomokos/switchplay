import React, {Component} from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'

import { openDeleteUserDialog, deleteUser, closeDeleteUserDialog } from '../../actions/User'

import DeleteUser from '../DeleteUser'


const mapStateToProps = (state, ownProps) => {
	return{
		id:ownProps.id,
		deleting:state.asyncProcesses.deleting.user,
		open:state.dialogs.deleteUser,
		error:''
	}
}
const mapDispatchToProps = dispatch => ({
	openDialog(){
		console.log("openDeleteUserDialog called...")
		dispatch(openDeleteUserDialog())
	},
	deleteUser(userId, history){
		dispatch(deleteUser(userId, history))
	},
	closeDialog(){
		dispatch(closeDeleteUserDialog())
	}
})

//wrap all 4 sections in the same container for now.
const DeleteUserContainer = connect(
	mapStateToProps,
	mapDispatchToProps
	)(DeleteUser)

export default DeleteUserContainer

