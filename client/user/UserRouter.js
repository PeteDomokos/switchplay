import React, {Component} from 'react'
import { Route, Switch }from 'react-router-dom'
import PropTypes from 'prop-types'
//children
import User from './User'
import EditUserProfile from './EditUserProfile'

//todo - Route /user should redirect to "/signin" or to "/user/:userId" if signed in
const UserRouter = () =>
  <Switch>
    <Route path="/user/edit/:userId" component={EditUserProfile}/>
    <Route path="/user/:userId" component={User}/>
  </Switch>

export default UserRouter