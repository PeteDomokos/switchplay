const create = (user) => {
  console.log("client api-user create()")
  return fetch('/api/users/', {
      method: 'POST',
      headers: {
        'Accept': 'application/json'
      },
      body: user
    })
    .then((response) => {
      return response.json()
    }).catch((err) => console.log(err))
}

const list = () => {
  console.log("user list api")
  return fetch('/api/users/', {
    method: 'GET',
  }).then(response => {
    return response.json()
  }).catch((err) => console.log(err))
}

const listPlayers = () => {
  console.log("user list players api")
  return fetch('/api/users/players', {
    method: 'GET',
  }).then(response => {
    return response.json()
  }).catch((err) => console.log(err))
}
const listCoaches = () => {
  console.log("user list coaches api")
  return fetch('/api/users/coaches', {
    method: 'GET',
  }).then(response => {
    return response.json()
  }).catch((err) => console.log(err))
}

const read = (params, credentials) => {
  console.log("user read api")
  return fetch('/api/user/' + params.id, {
    method: 'GET',
    headers: {
      'Accept': 'application/json', 
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ' + credentials.t
    }
  }).then((response) => {
    return response.json()
  }).catch((err) => console.log(err))
}

const update = (params, credentials, user) => {
  console.log("user update api")
  return fetch('/api/users/' + params.id, {
    method: 'PUT',
    headers: {
      'Accept': 'application/json',
      'Authorization': 'Bearer ' + credentials.t
    },
    body: user
  }).then((response) => {
    return response.json()
  }).catch((err) => console.log(err))
}

const remove = (params, credentials) => {
  return fetch('/api/users/' + params.id, {
    method: 'DELETE',
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ' + credentials.t
    }
  }).then((response) => {
    return response.json()
  }).catch((err) => console.log(err))
}
const removeAllUsers = () => {
  return fetch('/api/users/remove-all', {
    method: 'DELETE',
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json'
    }
  }).then((response) => {
    return response.json()
  }).catch((err) => console.log(err))
}
export {
  create,
  list,
  listPlayers,
  listCoaches,
  read,
  update,
  remove,
  removeAllUsers
}
