import React, {Component} from 'react'
import PropTypes from 'prop-types'
import { Link } from 'react-router-dom'
//material-ui
import {withStyles} from 'material-ui/styles'
import Paper from 'material-ui/Paper'
import List, {ListItem, ListItemAvatar, ListItemSecondaryAction, ListItemText} from 'material-ui/List'
import Avatar from 'material-ui/Avatar'
import IconButton from 'material-ui/IconButton'
import Button from 'material-ui/Button'
import Typography from 'material-ui/Typography'
import Edit from 'material-ui-icons/Edit'
import Person from 'material-ui-icons/Person'
import Divider from 'material-ui/Divider'

const styles = theme => ({
})
const PlayerCard = ({user, clickHandler, selected, classes}) =>{
  const photoUrl = user._id
              ? '/api/user/photo/' +user._id +'?'+new Date().getTime()
              : '/api/users/defaultphoto'
  return(
    <Paper elevation={4} onClick={() => clickHandler(user)} style={{border:'solid', borderColor:'blue', width:80}}>
      <div style={{display:'flex', flexDirection:'column', alignItems:'center'}}>
        <Avatar src={photoUrl} className={classes.bigAvatar}/>
        <h5 style={{margin:5, marginBottom:0}}>{user.firstName}</h5>
        <h5>{user.surname}</h5>
      </div>
    </Paper>
  )
}

PlayerCard.propTypes = {
}
PlayerCard.defaultProps = {
}

export default withStyles(styles)(PlayerCard)
