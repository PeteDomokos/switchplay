import express from 'express'
import groupCtrl from '../controllers/group.controller'
import authCtrl from '../controllers/auth.controller'
import playerCtrl from '../controllers/player.controller'
import datapointCtrl from '../controllers/datapoint.controller'

const router = express.Router()

/*router.route('/api/data')
  .get(dataCtrl.list)
  .post(authCtrl.requireSignin,authCtrl.hasAuthorization, dataCtrl.create)*/

//router.param('datapointId', datapointCtrl.datapointByID)
//router.param('groupId', groupCtrl.groupByID)
//router.param('playerId', playerCtrl.playerByID)

export default router
