import express from 'express'
import groupCtrl
  from '../controllers/group.controller'
import { userById, addGroupToPlayer, removeGroupFromPlayer } from '../controllers/user.controller'
import authCtrl from '../controllers/auth.controller'

const router = express.Router()

router.route('/api/groups')
  .get(groupCtrl.list)
  .post(authCtrl.requireSignin, groupCtrl.create)

router.route('/api/group/players/add')
  .put(authCtrl.requireSignin, addGroupToPlayer, groupCtrl.addPlayer)

router.route('/api/group/players/remove')
  .put(authCtrl.requireSignin, removeGroupFromPlayer, groupCtrl.removePlayer)

router.route('/api/groups/by-admin-user/:userId')
  .get(groupCtrl.listUserAdminGroups)

//todo - merge this with the other update method
router.route('/api/groups/update-fields/:groupId')
  .put(authCtrl.requireSignin, authCtrl.hasAuthorization, groupCtrl.updateFields)

router.route('/api/group/photo/:groupId')
  .get(groupCtrl.photo, groupCtrl.defaultPhoto)
router.route('/api/group/defaultphoto')
  .get(groupCtrl.defaultPhoto)
  
router.route('/api/groups/:groupId')
  .get(groupCtrl.read)
  .put(authCtrl.requireSignin, authCtrl.hasAuthorization, groupCtrl.update)
  .delete(authCtrl.requireSignin, groupCtrl.remove)

router.route('/api/group/:groupId/datasets/add')
  .put(authCtrl.requireSignin, authCtrl.hasAuthorization, groupCtrl.addDataset)

router.route('/api/group/:groupId/dataset/:datasetId/datapoints/add')
  .put(authCtrl.requireSignin, authCtrl.hasAuthorization, groupCtrl.addDatapoint)

router.param('groupId', groupCtrl.groupById)
router.param('userId', userById)

export default router
