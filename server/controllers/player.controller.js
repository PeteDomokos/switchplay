import errorHandler from './../helpers/dbErrorHandler'
import User from '../models/user.model'
import Group from '../models/group.model'
import Player from '../models/player.model'

export const list = (req, res) => {
  console.log("listPlayers")
  //todo - work out why query filter not working
  User.find(/*{player:{registered:true}},*/ (err, value) =>{
    if(err){
      console.log("User find error ", errorHandler.getErrorMessage(err))
      return res.status(400).json({error: 
        errorHandler.getErrorMessage(err)
      })
  	}
    console.log("player list returned length:", value.length)
    return res.status(200).json(value)
  })
}

export const listByGroup = (req, res) => {
  console.log("listPlayersByGroup group: ", req.group.name)
  User.find(/*{player:{registered:true}},*/ (err, value) =>{
    if(err){
    	console.log("listPlayersByGroup group: ", req.group.name)
      return res.status(400).json({error: 
        errorHandler.getErrorMessage(err)
      })
  }
    console.log("player list returned length:", value.length)
    return res.status(200).json(value)
  })
}

const filterAvailablePlayers = (availPlayers, selectedPlayers) =>{
	return availPlayers.filter(availPlayer => 
		!selectedPlayers.find(p => p._id.equals(availPlayer._id)))
}
export const listEligibleByGroup = (req, res) => {
  console.log("listEligiblePlayersByGroup group: ", req.group.name)
  //note - this method wouldnt normally be called if group has parent,
  //as available players would come from parent. 
  //todo later - filter for things like age group, gender of group
 if(!req.group.parent)
 	return listEligible(req, res)
 else{
 	//find parent group
	 Group.findById(req.group.parent)
	    .populate('players', 'firstName surname')
	    .exec((err, parentGroup) => {
	    	// ||group makes it log even when error = null
	        if(err){
	          console.log("parent group error....", err)
	          return res.status('400').json({error: 
	            errorHandler.getErrorMessage(err)
	          })
	        }
		    console.log("parent group found: ", parentGroup.name)
		    return res.status(200).json(parentGroup.players)
		  })
	}
}
//param req may have group attached
export const listEligible = (req, res) => {
  console.log("listEligiblePlayers")
  //to dod - put filters int find baed on age group, gender of group etc
  User.find((err, players) =>{
    if(err){
      return res.status('400').json({error: 
        errorHandler.getErrorMessage(err)
      })
    }
    console.log("players found length:", players.length)
    return res.status(200).json(players)
  })
}
export default {
  list,
  listByGroup,
  listEligibleByGroup,
  listEligible
}

 
