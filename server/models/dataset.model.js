import mongoose from 'mongoose'
import crypto from 'crypto'
import DatapointSchema from './datapoint.model'
import DataValueFormatSchema from './datavalue-format.model'
import UserSchema from './user.model'
import GroupSchema from './group.model'

export default new mongoose.Schema({
  isStandard:Boolean,
  visibleTo:[String],
  category:{
    type: String,
    trim: true,
    required: 'Category is required'
  },
  name: {
    type: String,
    trim: true,
    required: 'Test is required'
  },
  nrParticipants: {
    type:Number,
    default:1
  },
  dataValueFormats:[DataValueFormatSchema],
  datapoints:[DatapointSchema],
  createdBy:{type:mongoose.Schema.ObjectId, ref:'User'},
  group:{type:mongoose.Schema.ObjectId, ref:'Group'}
})

//export default mongoose.model('Dataset', DatasetSchema)
