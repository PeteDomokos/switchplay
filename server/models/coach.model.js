import mongoose from 'mongoose'

//const CoachSchema = new mongoose.Schema({
export default new mongoose.Schema({
  registered:{
    type:Boolean,
    default:false
  },
  mainGroup:{
      type:mongoose.Schema.ObjectId,
      ref:'Group'
  },
  groups:[{
    type:mongoose.Schema.ObjectId,
    ref:'Group'
  }],
  desc:String,
  admin:[{type:mongoose.Schema.ObjectId, ref:'User'}]
})

//module.exports = {
//export default mongoose.model('Coach', CoachSchema)
