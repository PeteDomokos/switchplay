import mongoose from 'mongoose'
import crypto from 'crypto'
import CoachSchema from './coach.model'
import PlayerSchema from './player.model'

export default new mongoose.Schema({
  name:{
    type:String,
    required:'Name is required'
  },
  dataType:{
    type:String,
    required:'Data type is required'
  },
  unit:String
})

