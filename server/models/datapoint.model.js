import mongoose from 'mongoose'
import crypto from 'crypto'
import CoachSchema from './coach.model'
import PlayerSchema from './player.model'

export default new mongoose.Schema({
  //normally a datapoint is attached to a player
  player:{
      type:mongoose.Schema.ObjectId,
      ref:'User'
  },
  players:[{
      type:mongoose.Schema.ObjectId,
      ref:'User'
  }],
  //names of values are read from dataset
  dataValues:[Number],
  eventDate:{
    type: Date,
    default: Date.now
  },
  creationDate: {
    type: Date,
    default: Date.now
  },
  createdBy:{
    type:mongoose.Schema.ObjectId,
    ref:'User'
  }
})

/*
ADD LATER
  //a datapoint can be attached to a group instead of an individual player (eg a group test)
  group:{
      type:mongoose.Schema.ObjectId,
      ref:'Group'
  },
  //a datapoint can also be attached to a coach (eg number of hours worked)
  coach:{
      type:mongoose.Schema.ObjectId,
      ref:'Coach'
  },
*/