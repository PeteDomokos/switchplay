import { Touch, Switchplay, Dribble, Yoyo, TTest, Shuttles, Wins, Points, KeepUps }
  from '../models/player.model'
import Player from '../models/player.model'
import _ from 'lodash'
import formidable from 'formidable'
import fs from 'fs'
import errorHandler from './../helpers/dbErrorHandler'
import targetsBuilder from './../helpers/targetsBuilder'
import playersDataToAdd from './../data/playersDataToAdd.js'
import playersToAdd from './../data/playersToAdd.js'

import profileImage from './../../client/assets/images/profilepic.png'
//TODO - REFACTOR METHOD TO SEPARATE CONCERNS AND RESPONSIBILITIES
/**
*
*  
**/
const create = (req, res, next) => {
  console.log("playerCtrl.create()")
  let form = new formidable.IncomingForm()
  form.keepExtensions = true
  form.parse(req, (err, fields, files) =>{
    let playerProps = fields
    if(files.photo){
      playerProps.photo = {}
      playerProps.photo.data = fs.readFileSync(files.photo.path)
      playerProps.photo.contentType = files.photo.type
    }
    player.save((err, result) => {
      if (err) {
        console.log("player error:", err)
        return res.status(400).json({
          error: errorHandler.getErrorMessage(err)
        })
      }
      console.log("player saved:")
      res.status(200).json({
        message: "Player successfully signed up."
      })
    })
  })
}

/**
 * Load user and append to req.
 */

  //todo - change profile to player
const playerByID = (req, res, next, id) => {
  Player.findById(id).exec((err, player) => {
    if (err || !player){
      console.log("error")
      return res.status('400').json({
        error: "Player not found"
      })
    }
    req.profile = player
    next()
  })
}

/**
*
*  
**/
const read = (req, res) => {
  return res.json(req.profile)
}
/**
*
*  
**/
const readScore = (req, res) =>{
  let { startDate } = req.profile
  let { category, test, week } = req.params
  let testArr = req.profile[category][test]
  //todo - change to filter and then find teh one with the highest score
  //in case there are two entries for the same week
  let requiredTest = 
    testArr.find(test => 
      targetsBuilder.calculateWeek(test.date, startDate) == Number(week))
  return res.json(requiredTest)
}
/**
*
*  
**/
const createPlayersFromServer = (req, res, next) =>{
  console.log("ctrl.createPlayersFromServer")
  playersToAdd.forEach(playerProperties =>{
    const player = new Player(playerProperties)
    player.save((err, result) => {
      if (err) {
        console.log("player.save error", err)
      }else{
        console.log(" player.save")
      }
    })
  })
}
/**
*
*  
**/
//TODO ----writeScore which can take args from client or from a js object 
//which calls it on server
const writeDataFromServer = (req, res, next) => {
  console.log("writeDataFromServer")
  //helper method to actually write each score
  const writeScores = (playerId, testObjects) =>{
    //findPlayerById
    Player.findById(playerId).exec((err, player) => {
      if (err || !player){
        console.log("error cant find player")
      }else{
        testObjects.forEach(testObject => {
          let { category, test, data } = testObject
          let testToAdd= createTestInstance(test, data)
          //add test to player
          player[category][test].push(testToAdd)
        })
        //save to database
        player.save().then(data => {
          console.log("saved test: "+test +" to: ",player.firstName)
        })
      }
    })
  }
  //loop through the data object calling writeAScore each time
  playersDataToAdd.forEach(playerObject =>{
    console.log("calling writeScores for ", playerObject.firstName)
    writeScores(playerObject.id, playerObject.tests)
  })
  /* TRY...*/
  /*Promise.all(playersDataToAdd.map(playerObject =>{
    console.log("playerObject", playerObject)
    writeScores(playerObject.id, playerObject.tests)
  })
  .then(() => console.log("FINISHED--------------"))*/
}
/**
*
*  
**/
const listTests = (req, res) =>{
  let player = req.profile
  let { cat, test} = req.params
  console.log("playerCtrl.listTests() player:", player.firstName)
  const tests = player[cat][test]
  return res.json(tests)
}
/**
*
*  
**/
const deleteTests = (req, res, next) => {
  let player = req.profile
  let { cat, test, playerId } = req.params
  //create update object
  let update = {}
  update[cat] = player[cat]
  update[cat][test] = []
  //alternative to findOneAndUpdate, seeing as we already have player
  player.set(update)
  player.save().then(data => {
    res.json(data)
  })

}
/**
*
*  
**/
const deleteTest = (req, res, next) => {
  console.log("removeTest req------------------")
  let player = req.profile
  let { cat, test, playerId, testId } = req.params
  player[cat][test].find(test => test._id == testId).remove()
  player.save().then(data => {
    res.json(data)
  })
}
/**
*
*  
**/
const createTestInstance = (test, data) =>{
  console.log("creating new test for: "+test, data)
  switch(test){
    case 'touch':return new Touch(data)
    case 'switchplay':return new Switchplay(data)
    case 'dribble':return new Dribble(data)
    case 'yoyo':return new Yoyo(data)
    case 'tTest':return new TTest(data)
    case 'shuttles':return new Shuttles(data)
    case 'wins':return new Wins(data)
    case 'points':return new Points(data)
    case 'keepUps':return new KeepUps(data)
  }
}
/**
*
*  
**/
const list = (req, res) => {
  //get weekNr for the date
  console.log("playerCtrl.list...")
  Player.find((err, players) => {
    if (err) {
      console.log("playerCtrl.list err", err)
      return res.status(400).json({
        error: errorHandler.getErrorMessage(err)
      })
    }
    res.json(players)
  }).select('_id firstName photo games skills fitness startDate')
}
/**
*
*  
**/
const listSnapshots = (req, res) => {
  //get weekNr for the date
  console.log("playerCtrl.listSnapshots...")
  Player.find((err, players) => {
    if (err) {
      return res.status(400).json({
        error: errorHandler.getErrorMessage(err)
      })
    }
    res.json(players)
  }).select('_id firstName surname photo')
}
/**
*
*  
**/

//note: trick for bulk updating when schema is extender:
// use updateMany with query newProperty == null
const update = (req, res, next) => {
  let player = req.profile
  console.log("updating player: ", req.profile.firstName)
  player = _.extend(player, req.body)
  player.updated = Date.now()
  player.save((err) => {
    if (err) {
      return res.status(400).json({
        error: errorHandler.getErrorMessage(err)
      })
    }
    res.json(player)
  })
}
/**
*
*  
**/
const remove = (req, res, next) => {
  console.log("playerCtrl.remove")
  console.log("playerCtrl.remove removing player: ",req.profile._id )
  let player = req.profile
  player.remove((err, deletedPlayer) => {
    if (err) {
      console.log("playerCtrl.remove  -error!!!!!!!")
      return res.status(400).json({
        error: errorHandler.getErrorMessage(err)
      })
    }
    console.log("removed!!!!!!!!!!!")
    res.json(deletedPlayer)
  })
}
/**
*
*  
**/
const photo = (req, res, next) => {
  if(req.profile.photo.data){
    res.set("Content-Type", req.profile.photo.contentType)
    return res.send(req.profile.photo.data)
  }
  next()
}

const defaultPhoto = (req, res) => {
  return res.sendFile(process.cwd()+profileImage)
}

export default {
  create,
  playerByID,
  read,
  readScore,
  writeDataFromServer,
  createPlayersFromServer,
  createTestInstance,
  list,
  listSnapshots,
  remove,
  update,
  listTests,
  deleteTests,
  deleteTest,
  photo,
  defaultPhoto
}
