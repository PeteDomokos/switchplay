import User from '../models/user.model'
import Player from '../models/player.model'
import Coach from '../models/coach.model'
import _ from 'lodash'
import formidable from 'formidable'
import fs from 'fs'
import errorHandler from './../helpers/dbErrorHandler'
import playerCtrl from '../controllers/player.controller'

import profileImage from './../../client/assets/images/profilepic.png'

//TODO - REFACTOR METHOD TO SEPARATE CONCERNS AND RESPONSIBILITIES
/**
*
*  
**/
const create = (req, res, next) => {
  console.log("userCtrl.create()")
  let form = new formidable.IncomingForm()
  form.keepExtensions = true
  form.parse(req, (err, fields, files) =>{
    if(fields.playerProperties){
      console.log("adding to player properties...")
      const { playerProperties, ...userProperties} = fields
      //add name and surname into player
      //startDate is a fixed date for now, same as groupStartDate
      const allPlayerProperties = { 
          ...playerProperties, 
          firstName:userProperties.firstName, 
          surname:userProperties.surname,
          startDate:new Date("2019-07-02T16:16:42.398Z")
      }
      //add photo if it has been uploaded
      //todo - only use user photo by default if no player photo uploaded
      if(files.photo){
        allPlayerProperties.photo = {}
        allPlayerProperties.photo.data = fs.readFileSync(files.photo.path)
        allPlayerProperties.photo.contentType = files.photo.type
      }
      const player = new Player(allPlayerProperties)
      player.save((err, result) => {
        if (err) {
          console.log("userCtrl.create()...player not saved - err: ",err)
          return res.status(400).json({
            error: errorHandler.getErrorMessage(err)
          })
        }
        console.log("userCtrl.create()...saved")
        //add playerId to userProps (can use player, dont need to use result)
        let userPropsAsPlayer = {...userProperties, player:player._id}
        //check photo and add if necc
        if(files.photo){
          userPropsAsPlayer.photo = {}
          userPropsAsPlayer.photo.data = fs.readFileSync(files.photo.path)
          userPropsAsPlayer.photo.contentType = files.photo.type
        }
        //check if coach - add team lists etc
        //...todo
        //check if admin
        if(['p@p.p', 'pd@y.z', 'r@r.r', 's@s.s'].includes(fields.email))
          userPropsAsPlayer.isSystemAdmin = true

        createUser(userPropsAsPlayer, res)
      })  
    }else if(fields.coachProperties){
      //is a coach
      console.log("adding to coach properties...")
      const { coachProperties, ...userProperties} = fields
      //add name and surname into player
      const allCoachProperties = { 
          ...coachProperties, 
          firstName:userProperties.firstName, 
          surname:userProperties.surname,
      }
      //add photo if it has been uploaded
      //todo - only use user photo by default if no player photo uploaded
      if(files.photo){
        allCoachProperties.photo = {}
        allCoachProperties.photo.data = fs.readFileSync(files.photo.path)
        allCoachProperties.photo.contentType = files.photo.type
      }
      const coach = new Coach(allCoachProperties)
      coach.save((err, result) => {
        if (err) {
          console.log("coach not saved - err: ",err)
          return res.status(400).json({
            error: errorHandler.getErrorMessage(err)
          })
        }
        console.log("saved coach")
        //add playerId to userProps (can use player, dont need to use result)
        let userPropsAsCoach = {...userProperties, coach:coach._id}
        //check photo and add if necc
        if(files.photo){
          userPropsAsCoach.photo = {}
          userPropsAsCoach.photo.data = fs.readFileSync(files.photo.path)
          userPropsAsCoach.photo.contentType = files.photo.type
        }
        //check if coach - add team lists etc
        //...todo
        //check if admin
        if(['p@p.p', 'q@q.q', 'r@r.r', 's@s.s'].includes(fields.email))
          userPropsAsCoach.isSystemAdmin = true

        createUser(userPropsAsCoach, res)
      })

    }else{
      //not player or coach
      let userProps = fields
      if(['p@p.p', 'q@q.q', 'r@r.r', 's@s.s'].includes(fields.email))  
        userProps.isSystemAdmin = true
      //check photo and add if necc
      if(files.photo){
        userProps.photo = {}
        userProps.photo.data = fs.readFileSync(files.photo.path)
        userProps.photo.contentType = files.photo.type
      }
      console.log("ctrl.create() creating non-player")
      createUser(userProps, res)
    }
  })
}
/**
*
*  
**/
const createUser = (userProperties, res) =>{
  let signUpMesg = 'successfully signed up'
  if(userProperties.player)
    signUpMesg = 'player '+signUpMesg
  if(userProperties.isSystemAdmin)
    signUpMesg = 'admin '+signUpMesg
  //if(userProperties.coach)
    //signUpMesg = 'coach '+signUpMesg

  //create user
  const user = new User(userProperties)
  user.save((err, result) => {
    if (err) {
      console.log("ctrl.create() error:", err)
      return res.status(400).json({
        error: errorHandler.getErrorMessage(err)
      })
    }
    console.log("ctrl.create() saved")
    res.status(200).json({
      message: signUpMesg
    })
  })
}


/**
 * Load user and append to req.
 */
const userByID = (req, res, next, id) => {
  User.findById(id)
      .populate('player', 'name')
      .exec((err, user) => {
    if (err || !user)
      return res.status('400').json({
        error: "User not found"
      })
    req.profile = user
    next()
  })
}
/**
*
*  
**/
const read = (req, res) => {
  req.profile.hashed_password = undefined
  req.profile.salt = undefined
  return res.json(req.profile)
}

const list = (req, res) => {
  User.find((err, users) => {
      if (err) {
        return res.status(400).json({
          error: errorHandler.getErrorMessage(err)
        })
      }
      let coach = users.find(u => u.firstName == 'Deo')
      /*users.forEach(u => {
        console.log("pw")
        console.log("pw for "+u.firstName, u.password.get())
      })*/
      res.json(users)
    }).select('firstName surname groups player coach mainGroup isSystemAdmin')
      .populate('player','firstName photo')
      .populate('coach','firstName photo')
      .populate('groups', 'name')
}

const update = (req, res, next) => {
  console.log("userCtrl.update()")
  let form = new formidable.IncomingForm()
  form.keepExtensions = true
  form.parse(req, (err, fields, files) =>{
    if (err) {
      return res.status(400).json({
        error: "update unsuccessful"
      })
    }
    let user = req.profile
    let { playerProperties, ...userProperties } = fields
    //todo - make separate methodd for player update, which returns immediately if no update needed,
    //so user update not repeated 
    if(user.player && (playerProperties || files.photo || fields.firstName || fields.surname) ){
      //need to update player aswell as user
      let otherPlayerProperties ={}
      if(fields.firstName)
        otherPlayerProperties.firstName = fields.firstName
      if(fields.surname)
        otherPlayerProperties.surname = fields.surname
      let allPlayerProperties = {...playerProperties, ...otherPlayerProperties }
      Player.findById(user.player).exec((err,player) =>{
        player = _.extend(player, allPlayerProperties)
        if(files.photo){
          player.photo.data = fs.readFileSync(files.photo.path)
          player.photo.contentType = files.photo.type
        }
        player.save((err, result) => {
          if (err) {
            return res.status(400).json({
              error: errorHandler.getErrorMessage(err)
            })
          }
          console.log("player update saved. now updating user")
          user = _.extend(user, fields)
          user.updated = Date.now()
          //add photo if it has been uploaded
          if(files.photo){
            user.photo.data = fs.readFileSync(files.photo.path)
            user.photo.contentType = files.photo.type
          }
          user.save((err, result) => {
            if (err) {
              return res.status(400).json({
               error: errorHandler.getErrorMessage(err)
              })
            }
            console.log("user update saved")
            user.hashed_password = undefined
            user.salt = undefined
            res.json(user)
          })
        })
      })
    }else{
        //no need to update player
        user = _.extend(user, fields)
        user.updated = Date.now()
        //add photo if it has been uploaded
        if(files.photo){
          user.photo.data = fs.readFileSync(files.photo.path)
          user.photo.contentType = files.photo.type
        }
        user.save((err, result) => {
          if (err) {
            return res.status(400).json({
              error: errorHandler.getErrorMessage(err)
            })
          }
          console.log("user update saved. no player")
          user.hashed_password = undefined
          user.salt = undefined
          res.json(user)
        })
    }
  })
}

/* a general method for removing stuff, tht can be edited to meet needs*/
const removeAUser = (req, res) => {
  console.log("removing a user...")
  //change this id for user we wish to delete
  let idToRemove = "5db6d0e566f68e258c8b2999"
  User.findById(idToRemove).exec((err, user) => {
    if (err || !user){
      console.log("error...")
      return res.status('400').json({
        error: "User not found"
      })
    }
    user.remove((err, deletedUser) => {
      if (err) {
        console.log("removing a user...error")
        return res.status(400).json({
          error: errorHandler.getErrorMessage(err)
        })
      }
      deletedUser.hashed_password = undefined
      deletedUser.salt = undefined
      console.log("removed user")
      res.json(deletedUser)
    })
  })
}

const remove = (req, res, next) => {
  let user = req.profile
  console.log("ctrl remove...removing user: ")
  user.remove((err, deletedUser) => {
    if (err) {
      return res.status(400).json({
        error: errorHandler.getErrorMessage(err)
      })
    }
    deletedUser.hashed_password = undefined
    deletedUser.salt = undefined
    res.json(deletedUser)
  })
}
const photo = (req, res, next) => {
  if(req.profile.photo.data){
    res.set("Content-Type", req.profile.photo.contentType)
    return res.send(req.profile.photo.data)
  }
  next()
}

const defaultPhoto = (req, res) => {
  return res.sendFile(process.cwd()+profileImage)
}



export default {
  create,
  userByID,
  read,
  list,
  remove,
  removeAUser,
  update,
  photo,
  defaultPhoto
}
