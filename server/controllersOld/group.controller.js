import Group from '../models/group.model'
import User from '../models/user.model'
import _ from 'lodash'
import errorHandler from './../helpers/dbErrorHandler'
import profileImage from './../../client/assets/images/profilepic.png'

//TODO - REFACTOR METHOD TO SEPARATE CONCERNS AND RESPONSIBILITIES
/**
*
*  
**/
const create = (req, res, next) => {
  console.log("group ctrl. create")
  let { isMain, ...groupPropsFromClient } = req.body
  console.log("isMain", isMain)
  console.log("groupPropsFromClient", groupPropsFromClient)
  let groupProps = {...req.body, admin:[req.auth]}
  const group = new Group(groupProps)
  console.log("group", group)
  //get user, check they dont have a group wih same name, and store a ref to it

  //need to populate user groups with firstName
  User.findById(req.auth).populate('groups', 'name').exec((err, user) => {
    if (err || !user)
      return res.status('400').json({
        error: "User not found"
      })
    if(user.groups.filter(g => g.name == group.name).length != 0){
      return res.status('400').json({
        error: "You already have a group with that name"
      })
    }else{
      //update user
      user.groups.push(group)
      //todo - heck if this actually updates the mainGroup
      if(!user.mainGroup || group.isMain)
        user.mainGroup = group._id
      console.log("user updated:", user)

      //todo - use Promise.all for these
      //save user
      //Promise.all([user.save().exec(), group.save().exec()]).then(data =>{
        //console.log("saved!!!!data:", data)
     //})
       user.save((err, result) => {
        if (err) {
          return res.status(400).json({
            error: errorHandler.getErrorMessage(err)
          })
        }
        group.save((err, result) => {
          if (err) {
            return res.status(400).json({
              error: errorHandler.getErrorMessage(err)
            })
          }
          console.log("group created and saved")
          res.status(200).json(result)
        })
      })
    }
  })
}

/**
 * Load user and append to req.
 */
const groupByID = (req, res, next, id) => {
  Group.findById(id)
    .populate('admin', 'firstName')
    .populate('players', 'firstName surname photo')
    .exec((err, group) => {
      if (err){ // ||group makes it log even when error = null
        console.log("error", err)
        return res.status('400').json({
          error: "Group not found"
        })
      }
      req.group = group
      next()
    })
}
/**
*
*  
**/
const read = (req, res) => {
  console.log("groupCtrl.read")
  return res.json(req.group)
}
/**
*
*  
**/
//todo - make this only show the users own groups using a query
//this shouldnt list all groups, and also the otehr groups should be deleted  now anyway
const list = (req, res) => {
  //get weekNr for the date
  console.log("groupCtrl.list...")
  Group.find((err, groups) => {
    if (err) {
      return res.status(400).json({
        error: errorHandler.getErrorMessage(err)
      })
    }
    res.json(groups)
  }).select('_id name desc')
}
/**
*
*  
**/
const update = (req, res, next) => {
  let player = req.profile
  console.log("updating player: ", req.profile.firstName)
  player = _.extend(player, req.body)
  player.updated = Date.now()
  player.save((err) => {
    if (err) {
      return res.status(400).json({
        error: errorHandler.getErrorMessage(err)
      })
    }
    res.json(player)
  })
}

/**
*
*  
**/
//todo - add a hasAuthirisationToRemoveGroup method in auth, which checks teh signed n user 
//is teh group creator or systemAdmin
const remove = (req, res, next) => {
  console.log("groupCtrl.remove")
  let group = req.group
  let user = req.auth
  User.findByIdAndUpdate(req.auth._id, {$pull: {groups:group._id}}, (err, result) =>{
    if(err){
      return res.status(400).json({
        error: errorHandler.getErrorMessage(err)
      })
    }
    console.log("updated and saved user groups", result)
    //remove group from group database
    group.remove((err, deletedGroup) => {
      if (err) {
        return res.status(400).json({
          error: errorHandler.getErrorMessage(err)
        })
      }
      res.json(deletedGroup)
    })
  }) 
}
/**
*
*  
**/
//todo - check it adds playerID ref and then sends name and surname too
const addPlayer = (req, res, next) => {
  console.log("addPlayer")
  let { group } = req
  let player = req.profile
  //create update object
  let update = {}
  update.players = [...group.players, player._id]
  group.set(update)
  group.save().then(data => {
    console.log("updated and saved group", data)
    res.json(data)
  })
}
/**
*
*  
**/
const removePlayer = (req, res, next) => {
  let { group } = req
  let player = req.profile
}
/**
*
*  
**/
const photo = (req, res, next) => {
  if(req.group.photo.data){
    res.set("Content-Type", req.group.photo.contentType)
    return res.send(req.group.photo.data)
  }
  next()
}
/**
*
*  
**/
const defaultPhoto = (req, res) => {
  return res.sendFile(process.cwd()+profileImage)
}

export default {
  create,
  groupByID,
  read,
  list,
  remove,
  update,
  photo,
  defaultPhoto,
  addPlayer,
  removePlayer
}
