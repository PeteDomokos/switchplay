import Coach from '../models/coach.model'
import _ from 'lodash'
import errorHandler from './../helpers/dbErrorHandler'
import profileImage from './../../client/assets/images/profilepic.png'

const create = (req, res, next) => {
}

/**
 * Load user and append to req.
 */
const coachByID = (req, res, next, id) => {
  console.log("coachCtrl.coachByID", id)
  coach.findById(id).exec((err, coach) => {
    if (err || !coach){
      console.log("error")
      return res.status('400').json({
        error: "coach not found"
      })
    }
    req.profile = coach
    next()
  })
}

const read = (req, res) => {
  return res.json(req.profile)
}
const readScore = (req, res) =>{
  let { startDate } = req.profile
  let { category, test, week } = req.params
  let testArr = req.profile[category][test]
  //todo - change to filter and then find teh one with the highest score
  //in case there are two entries for the same week
  let requiredTest = 
    testArr.find(test => 
      targetsBuilder.calculateWeek(test.date, startDate) == Number(week))
  return res.json(requiredTest)
}
const createCoachsFromServer = (req, res, next) =>{
  console.log("ctrl.createcoachsFromServer")
  coachsToAdd.forEach(coachProperties =>{
    const coach = new coach(coachProperties)
    coach.save((err, result) => {
      if (err) {
        console.log("ctrl.createcoachsFromServer() coach.save error", err)
      }else{
        console.log("ctrl.createcoachsFromServer() coach.save result", result)
      }
    })
  })
}
//TODO ----writeScore which can take args from client or from a js object 
//which calls it on server
const writeDataFromServer = (req, res, next) => {
  console.log("writeDataFromServer")
  //helper method to actually write each score
  const writeScores = (coachId, testObjects) =>{
    console.log("writeScores coachId", coachId)
    //findcoachById
    coach.findById(coachId).exec((err, coach) => {
      if (err || !coach){
        console.log("error cant find coach")
      }else{
        testObjects.forEach(testObject => {
          let { category, test, data } = testObject
          let testToAdd= createTestInstance(test, data)
          //add test to coach
          coach[category][test].push(testToAdd)
        })
        //save to database
        coach.save().then(data => {
          console.log("saved test: "+test +" to: ",coach)
          console.log("data", data)
        })
      }
    })
  }
  //loop through the data object calling writeAScore each time
  coachsDataToAdd.forEach(coachObject =>{
    writeScores(coachObject.id, coachObject.tests)
  })
  /* TRY...*/
  /*Promise.all(coachsDataToAdd.map(coachObject =>{
    console.log("coachObject", coachObject)
    writeScores(coachObject.id, coachObject.tests)
  })
  .then(() => console.log("FINISHED--------------"))*/
}

const listTests = (req, res) =>{
  let coach = req.profile
  let { cat, test} = req.params
  console.log("coachCtrl.listTests() coach:", coach.firstName)
  const tests = coach[cat][test]
  return res.json(tests)
}
const deleteTests = (req, res, next) => {
  let coach = req.profile
  let { cat, test, coachId } = req.params
  console.log("coachCtrl.deleteTests() coach:", coach.firstName)
  //create update object
  let update = {}
  update[cat] = coach[cat]
  update[cat][test] = []
  //alternative to findOneAndUpdate, seeing as we already have coach
  coach.set(update)
  coach.save().then(data => {
    res.json(data)
  })
  //find coach and update - no need for coach to be available with this approach
  /*coach.findOneAndUpdate({_id:coachId}, update,
    {new: true}, (err, doc) =>{
      if (err) {
        return res.status(400).json({
          error: errorHandler.getErrorMessage(err)
        })
      }else{
        console.log("deleted tests..", doc)
        res.json(doc)
      }
  })*/
}
const deleteTest = (req, res, next) => {
  console.log("removeTest req------------------")
  let coach = req.profile
  let { cat, test, coachId, testId } = req.params
  coach[cat][test].find(test => test._id == testId).remove()
  coach.save().then(data => {
    res.json(data)
  })
}

const createTestInstance = (test, data) =>{
  switch(test){
    case 'touch':return new Touch(data)
    case 'switchplay':return new Switchplay(data)
    case 'dribble':return new Dribble(data)
    case 'yoyo':return new Yoyo(data)
    case 'tTest':return new TTest(data)
    case 'shuttles':return new Shuttles(data)
    case 'wins':return new Wins(data)
    case 'points':return new Points(data)
    case 'keepUps':return new KeepUps(data)
  }
}
const list = (req, res) => {
  //get weekNr for the date
  console.log("coachCtrl.list...")
  coach.find((err, coachs) => {
    if (err) {
      return res.status(400).json({
        error: errorHandler.getErrorMessage(err)
      })
    }
    res.json(coachs)
  }).select('_id firstName photo games skills fitness startDate')
}

/*
2. UI for adding a test score to the db, for admin and for a coach
  (but user can only add for themselves, unless they are the coach(later))
4. give user a key for their club-mates, and populate with their first names, surnames, photo for the buttons
5. wire up the db scores with targets data, inc:
    - if user clicks fitness, then all fitness scores are sent for the corresponding coach (assuming not coach)
    - if user clicks coachs, then all club-mates data is sent

*/


//note: trick for bulk updating when schema is extender: use updateMany with query newProperty == null
const update = (req, res, next) => {
  let coach = req.profile
  console.log("updating coach: ", req.profile.firstName)
  coach = _.extend(coach, req.body)
  coach.updated = Date.now()
  coach.save((err) => {
    if (err) {
      return res.status(400).json({
        error: errorHandler.getErrorMessage(err)
      })
    }
    res.json(coach)
  })
}

const remove = (req, res, next) => {
  console.log("coachCtrl.remove removing coach: ",req.profile._id )
  let coach = req.profile
  coach.remove((err, deletedcoach) => {
    if (err) {
      return res.status(400).json({
        error: errorHandler.getErrorMessage(err)
      })
    }
    res.json(deletedcoach)
  })
}

const photo = (req, res, next) => {
  console.log("coach photo. profile:", req.profile)
  if(req.profile.photo.data){
    res.set("Content-Type", req.profile.photo.contentType)
    return res.send(req.profile.photo.data)
  }
  next()
}

const defaultPhoto = (req, res) => {
  return res.sendFile(process.cwd()+profileImage)
}

export default {
  create,
  coachByID,
  read,
  readScore,
  writeDataFromServer,
  createCoachsFromServer,
  list,
  remove,
  update,
  listTests,
  deleteTests,
  deleteTest,
  photo,
  defaultPhoto
}
