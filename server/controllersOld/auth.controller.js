import User from '../models/user.model'
import jwt from 'jsonwebtoken'
import expressJwt from 'express-jwt'
import config from './../../config/config'

const signin = (req, res) => {
  console.log("authCtrl.signin()")
  User.findOne({
    "email": req.body.email
  }, (err, user) => {

    if (err || !user)
      return res.status('401').json({
        error: "User not found"
      })

    if (!user.authenticate(req.body.password)) {
      return res.status('401').send({
        error: "Email and password don't match."
      })
    }

    const token = jwt.sign({
      _id: user._id
    }, config.jwtSecret)

    res.cookie("t", token, {
      expire: new Date() + 9999
    })

    return res.json({
      token,
      user: user
    })

  })
}

const signout = (req, res) => {
  console.log("sign out")
  res.clearCookie("t")
  return res.status('200').json({
    message: "signed out"
  })
}

//if no user signed in, this throws an error
const requireSignin = expressJwt({
  secret: config.jwtSecret,
  userProperty: 'auth'
})
//req.profile could be a user or could be a group - but both users and groups have an admin array
const hasAuthorization = (req, res, next) => {
  console.log("authCtrl.hasAuthorization()... auth: ", req.auth)
  console.log("req.profile: ", req.profile)
  const authorized = req.profile && req.auth && 
    (req.profile._id == req.auth._id 
      || req.profile.admin.includes(req.auth.id)
      || req.auth._id == '5d13f22395e09f37f4c8ad8f') 
  console.log("authorized: ", authorized)
  if (!(authorized)) {
    return res.status('403').json({
      error: "User is not authorized"
    })
  }
  next()
}

export default {
  signin,
  signout,
  requireSignin,
  hasAuthorization,
}
