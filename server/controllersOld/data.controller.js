import { Touch, Switchplay, Dribble, Yoyo, TTest, Shuttles, Wins, Points, KeepUps }
  from '../models/player.model'
import Player from '../models/player.model'
import User from '../models/user.model'
import _ from 'lodash'
import errorHandler from './../helpers/dbErrorHandler'
import profileImage from './../../client/assets/images/profilepic.png'
import playerCtrl from '../controllers/player.controller'

/**
*
*  
**/
//todo - impl a duplicate checker
const create = (req, res, next) => {
  console.log("dataCtrl.create() testData:", req.body)
  let dataWrapper = req.body

   Player.findById(dataWrapper.playerId).exec((err, player) => {
      if (err || !player){
        console.log("error cant find player")
        return res.status('400').json({
          error: "Player not found"
        })
      }else{
          let testToAdd= playerCtrl.createTestInstance(dataWrapper.test, dataWrapper.data)
          console.log("testToAdd", testToAdd)
          //add test to player
          player[dataWrapper.cat][dataWrapper.test].push(testToAdd)
        
          //save to database
          player.save().then(data => {
            console.log("saved test: "+dataWrapper.test +" to: ",player.firstName)         
            res.json(data)
          })
      }
  })
}

/**
 * Load user and append to req.
 */
const groupByID = (req, res, next, id) => {
  Group.findById(id)
    .populate('admin', 'firstName')
    .populate('players', 'firstName surname')
    .exec((err, group) => {
      if (err){ // ||group makes it log even when error = null
        console.log("error", err)
        return res.status('400').json({
          error: "Group not found"
        })
      }
      req.group = group
      next()
    })
}
/**
*
*  
**/
const read = (req, res) => {
  console.log("groupCtrl.read")
  return res.json(req.group)
}
/**
*
*  
**/
//todo - make this only show the users own groups using a query
const list = (req, res) => {
  //get weekNr for the date
  console.log("groupCtrl.list...")
  Group.find((err, groups) => {
    if (err) {
      return res.status(400).json({
        error: errorHandler.getErrorMessage(err)
      })
    }
    res.json(groups)
  }).select('_id name desc')
}
/**
*
*  
**/
const update = (req, res, next) => {
  let player = req.profile
  console.log("updating player: ", req.profile.firstName)
  player = _.extend(player, req.body)
  player.updated = Date.now()
  player.save((err) => {
    if (err) {
      return res.status(400).json({
        error: errorHandler.getErrorMessage(err)
      })
    }
    res.json(player)
  })
}
/**
*
*  
**/
const remove = (req, res, next) => {
  console.log("playerCtrl.remove")
  let group = req.group
  group.remove((err, deletedGroup) => {
    if (err) {
      return res.status(400).json({
        error: errorHandler.getErrorMessage(err)
      })
    }
    res.json(deletedGroup)
  })
}
/**
*
*  
**/
//todo - check it adds playerID ref and then sends name and surname too
const addPlayer = (req, res, next) => {
  console.log("addPlayer")
  let { group } = req
  let player = req.profile
  //create update object
  let update = {}
  update.players = [...group.players, player._id]
  group.set(update)
  group.save().then(data => {
    console.log("updated and saved group", data)
    res.json(data)
  })
}
/**
*
*  
**/
const removePlayer = (req, res, next) => {
  let { group } = req
  let player = req.profile
}
/**
*
*  
**/
const photo = (req, res, next) => {
  if(req.group.photo.data){
    res.set("Content-Type", req.group.photo.contentType)
    return res.send(req.group.photo.data)
  }
  next()
}
/**
*
*  
**/
const defaultPhoto = (req, res) => {
  return res.sendFile(process.cwd()+profileImage)
}

export default {
  create,
  groupByID,
  read,
  list,
  remove,
  update,
  photo,
  defaultPhoto,
  addPlayer,
  removePlayer
}
