//year, month, day, hour, minute, second, millisecond
//note: month from 0 to 11, hours produces 2 less than what we put in constructor
let week0Date = new Date("2019-07-02T18:55:52.815Z") //jul
let week1Date = new Date(2019,6,9,20,24,0,0) //jul
let week3Date = new Date(2019,6,24,15,24,0,0) //jul
let week5Date = new Date(2019,7,7,15,24,0,0) //aug
let week7Date = new Date(2019,7,21,15,24,0,0) //aug

const jerome = {
  id:'5db70842bde0f61dd051adac',
  /*test:{category:'fitness', test:'tTest', 
      data:{date:Date.now, time:15.5, points:95}
    }*/
    tests:[
    //touch
    {category:'skills', test:'touch', 
      data:{date:week0Date, left:10, right:15, advanced:0, points:35}
    },
    //switchplay
    {category:'skills', test:'switchplay', 
      data:{date:week0Date, reps:5.5, points:14}
    },
    //dribble
    {category:'skills', test:'dribble', 
      data:{date:week0Date, time:7.6}
    },
    //yoyo
    {category:'fitness', test:'yoyo', 
      data:{date:week0Date, distance:1600}
    },
    //tTest
    {category:'fitness', test:'tTest', 
      data:{date:week0Date, time:15.5}
    },
    //shuttles
    {category:'fitness', test:'shuttles', 
      data:{date:week0Date, time:10.7}
    }
  ]
}

export default jerome