# Player Gains #

This app now uses a MERN stack and is currently in development. A version of the app is being used in user trials at present within football clubs. The latest version can be viewed at 
	https://playergains.herokuapp.com/ 

The app is being gradually re-implemented on the basis of the moderations and clarifications of the app requirements garnered from the initial feedback from trials currently in progress. This re-implementation is also being undertaken with a test-driven approach, and with a more robust, modular design. The server-side was recently switched from using Scala with the Play Framework to using Node/Express with MongoDB. 
This Node back-end is now being re-implemented using TDD. The latest commit is midway through this process.
On the client side, the d3.js components are in the process of being tested. However, the entire front end is also being gradually re-implemented to utilise React 16 functionality such as hooks, as well as a redux store so that, as more data charts are added, data is shared across components effectively. 
Following a second set of trials, the app will be evaluated and re-implemented for ReactNative. Hence, it is anticipated that its main use, but not it’s only use, will be as a mobile app.


## Introduction ##

Player Gains allows sports coaches and players (or athletes) to enter and track their performance, fitness and skills data over time, and to compare their data to other players. 
Users sign up (either as coaches or as players) and then create groups (eg teams or small groups of players within a team). They assign players to each group from the database of available players. They can also create subgroups from an existing group. 

Users assign the relevant performance, skills and fitness tests to each group, depending on what criteria they want to measure. They can either select these tests from a range of pre-existing standardised tests in the system, or they can create their own custom tests.

The standardised tests come with the additional benefit that players can see how they compare to the expected standards for their age and level, or how they compare to other players from outside of their group.

Users then start to enter data whenever a test is taken, or whenever data for a test is gathered from a match or a training session. 

Users can then view their data from a variety of angles, including tracking their own progress over time, or comparing their scores to other players in the group, or to the expected standards for their age and level.

## Technologies ##

This app uses a MERN stack, with data visualisation implemented using d3.js.

**React** – the front-end is built with reactjs, with a mobile-first approach. In the next iteration (following trials), the system will be implemented with ReactNative.

**Redux** – Redux is built into the system, and was originally used for storing user details. However, these are now stored in session storage. The redux store will in the next iteration handle the data for the charts. This data is currently handled by the dashboard component and passed to the charts. However, the charts will still maintain local state related to user interaction.

**D3.js** – D3 functionality is embedded within React components, hence utilising the react life-cycles. Main charts are an interactive Scatter chart (for looking at how players’ data has changed over time) and a beeswarm plot using force-directed network (for comparing players’ best scores against one another). Initially, d3.js usage was contained within a shell inside a react component. In the current cycle, D3-driven elements are now being fully integrated with react components, with state managed through the life-cycles. In the next phase, some state will be managed via the redux store.

**Node/Express** – this server set-up matches the speed requirements of the system. There is currently no usage of complex systems such as integrating data from multiple web service sources, and so there seemed little to gain from using something such as Java or Scala and the Play framework, which was considered. It is anticipated that real-time data from users mobile devices such as smart watches will be added as an option, and a Node/Express solution should provide the necessary speed, especially when coupled with d3 on the client side for processing the data. 

**MongoDB** – a no sql database solution is deemed appropriate because users have the potential to reate their own sports tests which will have their own properties. This means that freedom needs to be afforded to the system to evolve in response to the users requirements. 

**Styling and responsiveness**– external stylesheets have been used, except in some cases where elements have been borrowed from MaterialUI. Some stylesheets contain styling for all screen sizes and devices, and other sheets are specifically for certain cases. Screen sizes have been split into three options, corresponding to mobile phone, tablet and laptop/desktop. Some react components are only rendered for specific screen sizes. This is handled predominantly via classNames and css properties than via inline media queries.

## Design Methodology ##

The app has used a wizard-of-oz methodology, involving several iterations of evaluate – develop – trial, with each iteration facilitating more and more independent usage of the app by users (ie users requiring less support from observers) or inbuilt prompts.
The approach used in the technical development to date can be described as Experiment-driven development but in order to achieve the necessary robustness, this app is now being re-implemented with test-driven development. Following trials that are currently taking place, the app will be implemented for ReactNative.



