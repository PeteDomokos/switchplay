
let startDate = new Date(2019,6,2, 20,24,0,0)  // 2 july
let week1Date = new Date(2019,6,9, 20,24,0,0)  // 9 jul
let week3Date = new Date(2019,6,24, 15,24,0,0) // 24 jul
let week5Date = new Date(2019,7,7, 15,24,0,0)  // 7 aug
let week7Date = new Date(2019,7,21, 15,24,0,0) // 21 aug

//normal case
export const normalData ={
	startDate:startDate,
	player1:{
		skills:{
			touch:[
				{date:startDate, points:75, _id:'p1-to-0'},
				{date:week1Date, points:75, _id:'p1-to-1'},
				{date:week3Date, points:80, _id:'p1-to-3'},
				{date:week5Date, points:85, _id:'p1-to-5'},
				{date:week7Date, points:95, _id:'p1-to-7'}
			],
			switchplay:[
			 	{date:startDate, reps:6.5, points:22, _id:'p1-sw-0'},
			 	{date:week1Date, reps:7, points:24, _id:'p1-sw-1'},
			 	{date:week3Date, reps:7, points:26, _id:'p1-sw-3'},
			 	{date:week5Date, reps:7, points:26, _id:'p1-sw-5'},
			 	{date:week7Date, reps:7.5, points:27, _id:'p1-sw-7'}
			],
			dribble:[
				{date:startDate, time:8.8, _id:'p1-d-0'},
				{date:week1Date, time:8.5, _id:'p1-d-1'},
				{date:week3Date, time:9.2, _id:'p1-d-3'},
				{date:week5Date, time:8.1, _id:'p1-d-5'},
				{date:week7Date, time:7.7, _id:'p1-d-7'}
			]
		},
		fitness:{
			tTest:[
				{date:startDate, time:12.5, _id:'p1-t-0'},
				{date:week1Date, time:12.2, _id:'p1-t-1'},
				{date:week3Date, time:11.9, _id:'p1-t-3'},
				{date:week5Date, time:12.5, _id:'p1-t-5'},
				{date:week7Date, time:11.6, _id:'p1-t-7'}
			],
			yoyo:[
				{date:startDate, distance:1840, _id:'p1-y-0'},
				{date:week1Date, distance:1920, _id:'p1-y-1'},
				{date:week3Date, distance:1960, _id:'p1-y-3'},
				{date:week5Date, distance:2000, _id:'p1-y-5'},
				{date:week7Date, distance:2000, _id:'p1-y-7'}
			],
			shuttles:[
				{date:startDate, time:9.6, _id:'p1-sh-0'},
				{date:week1Date, time:9.5, _id:'p1-sh-1'},
				{date:week3Date, time:9.4, _id:'p1-sh-3'},
				{date:week5Date, time:9.4, _id:'p1-sh-5'},
				{date:week7Date, time:9.2, _id:'p1-sh-7'}
			]
		}
	},
	player2:{
		skills:{
			touch:[
				{date:startDate, points:65, _id:'p2-to-0'},
				{date:week1Date, points:70, _id:'p2-to-1'},
				{date:week3Date, points:60, _id:'p2-to-3'},
				{date:week5Date, points:75, _id:'p2-to-5'},
				{date:week7Date, points:75, _id:'p2-to-7'}
			],
			switchplay:[
			 	{date:startDate, reps:6.5, points:15, _id:'p2-sw-0'},
			 	{date:week1Date, reps:6.5, points:15, _id:'p2-sw-1'},
			 	{date:week3Date, reps:7, points:16, _id:'p2-sw-3'},
			 	{date:week5Date, reps:7, points:16.5, _id:'p2-sw-5'},
			 	{date:week7Date, reps:7, points:18, _id:'p2-sw-7'}
			],
			dribble:[
				{date:startDate, time:10.9, _id:'p2-d-0'},
				{date:week1Date, time:10.7, _id:'p2-d-1'},
				{date:week3Date, time:10.7, _id:'p2-d-3'},
				{date:week5Date, time:10.5, _id:'p2-d-5'},
				{date:week7Date, time:10.2, _id:'p2-d-7'}
			]
		},
		fitness:{
			tTest:[
				{date:startDate, time:13.9, _id:'p2-t-0'},
				{date:week1Date, time:13.7, _id:'p2-t-1'},
				{date:week3Date, time:13.5, _id:'p2-t-3'},
				{date:week5Date, time:13.4, _id:'p2-t-5'},
				{date:week7Date, time:13.0, _id:'p2-t-7'}
			],
			yoyo:[
				{date:startDate, distance:1600, _id:'p2-y-0'},
				{date:week1Date, distance:1660, _id:'p2-y-1'},
				{date:week3Date, distance:1700, _id:'p2-y-3'},
				{date:week5Date, distance:1680, _id:'p2-y-5'},
				{date:week7Date, distance:1720, _id:'p2-y-7'}
			],
			shuttles:[
				{date:startDate, time:11.2, _id:'p2-sh-0'},
				{date:week1Date, time:11.0, _id:'p2-sh-1'},
				{date:week3Date, time:10.7, _id:'p2-sh-3'},
				{date:week5Date, time:10.6, _id:'p2-sh-5'},
				{date:week7Date, time:10.6, _id:'p2-sh-7'}
			]
		}
	},
	player3:{
		skills:{
			touch:[
				{date:startDate, points:55, _id:'p3-to-0'},
				{date:week1Date, points:65, _id:'p3-to-1'},
				{date:week3Date, points:80, _id:'p3-to-3'},
				{date:week5Date, points:70, _id:'p3-to-5'},
				{date:week7Date, points:75, _id:'p3-to-7'}
			],
			switchplay:[
			 	{date:startDate, reps:6.5, points:22, _id:'p3-sw-0'},
			 	{date:week1Date, reps:7, points:24, _id:'p3-sw-1'},
			 	{date:week3Date, reps:7, points:26, _id:'p3-sw-3'},
			 	{date:week5Date, reps:7, points:26, _id:'p3-sw-5'},
			 	{date:week7Date, reps:8, points:27, _id:'p3-sw-7'}
			],
			dribble:[
				{date:startDate, time:8.5, _id:'p3-d-0'},
				{date:week1Date, time:8.1, _id:'p3-d-1'},
				{date:week3Date, time:9.2, _id:'p3-d-3'},
				{date:week5Date, time:8.1, _id:'p3-d-5'},
				{date:week7Date, time:7.7, _id:'p3-d-7'}
			]
		},
		fitness:{
			tTest:[
				{date:startDate, time:12.9, _id:'p3-t-0'},
				{date:week1Date, time:12.8, _id:'p3-t-1'},
				{date:week3Date, time:12.5, _id:'p3-t-3'},
				{date:week5Date, time:12.4, _id:'p3-t-5'},
				{date:week7Date, time:12.4, _id:'p3-t-7'}
			],
			yoyo:[
				{date:startDate, distance:1540, _id:'p3-y-0'},
				{date:week1Date, distance:1620, _id:'p3-y-1'},
				{date:week3Date, distance:1760, _id:'p3-y-3'},
				{date:week5Date, distance:1700, _id:'p3-y-5'},
				{date:week7Date, distance:1780, _id:'p3-y-7'}
			],
			shuttles:[
				{date:startDate, time:10.6, _id:'p3-sh-0'},
				{date:week1Date, time:10.5, _id:'p3-sh-1'},
				{date:week3Date, time:10.4, _id:'p3-sh-3'},
				{date:week5Date, time:10.4, _id:'p3-sh-5'},
				{date:week7Date, time:10.2, _id:'p3-sh-7'}
			]
		}
	}
}
